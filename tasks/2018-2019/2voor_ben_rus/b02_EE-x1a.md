# 2. Публикуем фотографии

Условия использования произведений в системе Creative Commons обозначаются следующими символами:

![](b02-cc-by.png) - при использовании произведения необходимо ссылаться на автора;

![](b02-cc-nd.png) - произведение можно использовать только в неизменённом виде;

![](b02-cc-nc.png) - произведение можно использовать только в некоммерческих целях;

![](b02-cc-sa.png) - при изменении произведения новый вариант необходимо распространять на тех же условиях, что были указаны для оригинала.

Во время путешествия бобр Аня сделала фотографии и хотела бы лучшие из них выложить в Интернет, чтобы и другие смогли насладиться красотами мира.

## Вопрос

Какую комбинацию символов Аня должна указать рядом со своими фотографиями, чтобы другие пользователи не смогли бы их продать, но смогли бы обработать и распространять результаты на тех же условиях, ссылаясь на Аню как на первонального автора?

[Raadionupud]

A. ![](b02-va.png)

B. ![](b02-vb.png)

C. ![](b02-vc.png)

D. ![](b02-vd.png)

## Ответ:

Правильный ответ: B.

## Категории

- Понимание информации
- Разное

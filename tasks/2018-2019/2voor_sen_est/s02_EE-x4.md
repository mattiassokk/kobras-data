# 2. Piltide avaldamine

Kobras Anu tegi reisil pilte ja tahab parimad neist veebi välja panna, et ka teised saaks maailma ilu nautida. Piltide avaldamisel peaks ta nende juurde märkima ka, mis tingimustel teised neid kasutada võivad.

Anule meenus, et ta oli kunagi kuulnud Creative Commons süsteemist ja ta vaatas teiste samas serveris olevate fotode andmeid.

![](s02-1.png) märkidega foto juures oli kirjas, et seda ei tohi muuta ja võib kasutada autorile viidates ainult mitteärilistel eesmärkidel.

![](s02-2.png) märkidega foto juures oli kirjas, et seda võib muuta, kuid tulemust peab jagama autorile viidates ja samasugustel tingimustel.

Lisaks on ![](s02-3.png) märki vaadates selge, et see on seotud rahaga.

## Küsimus

Millise sümbolite kombinatsiooni peaks Anu oma piltide juurde lisama, et teised kasutajad ei tohiks neid muuta ega ilma autorile viitamata kasutada?

[Märkeruudud]

A. ![](s02-a.png)

B. ![](s02-b.png)

C. ![](s02-c.png)

D. ![](s02-d.png)

## Vastus

Õige vastus on: A, D.

## Kategooriad

- Info mõistmine
- Varia

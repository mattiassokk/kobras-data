# 13. Jae või hulgi?

Andresel on väike saekaater, kus ta toodab palkmaterjali. Ta tahab jagada oma kliendid kahte gruppi: kes ostavad vähemalt 50 palki, on hulgiostjad, ülejäänud jaekliendid.

![](j13-body.png)

## Küsimus

Millise valemi peaks ta lahtrisse C2 kirjutama ja sealt teistele ridadele kopeerima, et igal real oleks õigesti näidatud "jaemüük" või "hulgimüük"?

[Raadionupud]

A. <tt>=IF(B2>50;"hulgimüük";"jaemüük")</tt>

B. <tt>=IF(B2<50;"hulgimüük";"jaemüük")</tt>

C. <tt>=IF(B2>50;"jaemüük";"hulgimüük")</tt>

D. <tt>=IF(B2<50;"jaemüük";"hulgimüük")</tt>

## Vastus

Õige vastus on: D.

## Kategooriad

- Tabelitöötlus
- Loogika

# 2. Piltide avaldamine

Kobras Anu tegi reisil pilte ja tahab parimad neist veebi välja panna, et ka teised saaks maailma ilu nautida. Piltide avaldamisel peaks ta nende juurde märkima ka, mis tingimustel teised neid kasutada võivad.

Anule meenus, et ta oli kunagi kuulnud Creative Commons süsteemist ja ta vaatas teiste samas serveris olevate fotode andmeid.

![](j02-1.png) märkidega foto juures oli kirjas, et seda ei tohi muuta ja võib kasutada autorile viidates ainult mitteärilistel eesmärkidel.

![](j02-2.png) märkidega foto juures oli kirjas, et seda võib muuta, kuid tulemust peab jagama autorile viidates ja samasugustel tingimustel.

Lisaks on ![](j02-3.png) märki vaadates selge, et see on seotud rahaga.

## Küsimus

Mis on nende näidete põhjal iga märgi tähendus Creative Commons süsteemis?

[Vastavus]

A. ![](j02-a.png) <-> 1. Teose kasutamisel peab autorile viitama. | 2. Teost tohib kasutada ainult muutmata kujul. | 3. Teost tohib kasutada ainult mitteärilistel eesmärkidel. | 4. Teose muutmisel peab uut varianti jagama samade tingimustel, mis olid originaalil.

B. ![](j02-b.png) <-> 1. Teose kasutamisel peab autorile viitama. | 2. Teost tohib kasutada ainult muutmata kujul. | 3. Teost tohib kasutada ainult mitteärilistel eesmärkidel. | 4. Teose muutmisel peab uut varianti jagama samade tingimustel, mis olid originaalil.

C. ![](j02-c.png) <-> 1. Teose kasutamisel peab autorile viitama. | 2. Teost tohib kasutada ainult muutmata kujul. | 3. Teost tohib kasutada ainult mitteärilistel eesmärkidel. | 4. Teose muutmisel peab uut varianti jagama samade tingimustel, mis olid originaalil.

D. ![](j02-d.png) <-> 1. Teose kasutamisel peab autorile viitama. | 2. Teost tohib kasutada ainult muutmata kujul. | 3. Teost tohib kasutada ainult mitteärilistel eesmärkidel. | 4. Teose muutmisel peab uut varianti jagama samade tingimustel, mis olid originaalil.


## Vastus

Õige vastus on:

A <-> 4. Teose muutmisel peab uut varianti jagama samade tingimustel, mis olid originaalil.

B <-> 2. Teost tohib kasutada ainult muutmata kujul.

C <-> 1. Teose kasutamisel peab autorile viitama.

D <-> 3. Teost tohib kasutada ainult mitteärilistel eesmärkidel.

## Kategooriad

- Info mõistmine
- Varia

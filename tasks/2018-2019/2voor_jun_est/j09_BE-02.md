# 9. Kus on mängulennuk?

Jaana ja Robin mängivad õues mängulennukiga. Üks neist seisab künka otsas, teine aga otsib lennuki pärast iga maandumist üles.

Kõrge rohu sees paistab lennuk vaid künkal seisjale, mitte aga lähedal olijale. Jaanal ja Robinil on signaalsau, millega viibates saab otsijat lennukini juhatada:

| Vasakule            | Paremale                               | Künkale lähemale                                          | Künkast kaugemale                                         |
|---------------------|----------------------------------------|-----------------------------------------------------------|-----------------------------------------------------------|
| ![](j09-disc-1.png) | ![](j09-disc-1.png)![](j09-disc-2.png) | ![](j09-disc-1.png)![](j09-disc-2.png)![](j09-disc-1.png) | ![](j09-disc-2.png)![](j09-disc-1.png)![](j09-disc-2.png) |


Kahjuks tekivad probleemid, kui märguandeid saata ilma pausideta. Näiteks signaal

![](j09-disc-1.png)![](j09-disc-1.png)![](j09-disc-2.png)![](j09-disc-1.png)![](j09-disc-1.png)

võib tähendada nii "vasakule - lähemale - vasakule" kui ka "vasakule - paremale - vasakule - vasakule".

Jaana ja Robin peavad selle probleemi lahendama.

## Küsimus

Ainult üks järgnevaist on hea kood ehk kood, mida ei saa mõista mitut moodi. Milline neist?

|          | Vasakule                                                  | Paremale                                                                     | Lähemale                                                                     | Kaugemale                                                 |
|----------|-----------------------------------------------------------|------------------------------------------------------------------------------|------------------------------------------------------------------------------|-----------------------------------------------------------|
| A        | ![](j09-disc-1.png)                                       | ![](j09-disc-1.png)![](j09-disc-2.png)                                       | ![](j09-disc-1.png)![](j09-disc-2.png)![](j09-disc-2.png)![](j09-disc-1.png) | ![](j09-disc-2.png)![](j09-disc-1.png)![](j09-disc-2.png) |
| B        | ![](j09-disc-2.png)                                       | ![](j09-disc-1.png)                                                          | ![](j09-disc-2.png)![](j09-disc-1.png)                                       | ![](j09-disc-1.png)![](j09-disc-2.png)                    |
| C        | ![](j09-disc-1.png)![](j09-disc-2.png)![](j09-disc-2.png) | ![](j09-disc-1.png)![](j09-disc-2.png)![](j09-disc-2.png)![](j09-disc-2.png) | ![](j09-disc-1.png)                                                          | ![](j09-disc-1.png)![](j09-disc-2.png)                    |
| D        | ![](j09-disc-1.png)![](j09-disc-2.png)![](j09-disc-2.png) | ![](j09-disc-2.png)![](j09-disc-1.png)                                       | ![](j09-disc-1.png)![](j09-disc-2.png)![](j09-disc-2.png)![](j09-disc-1.png) | ![](j09-disc-1.png)                                       |


[Raadionupud]

A. Kood A

B. Kood B

C. Kood C

D. Kood D

## Vastus

Õige vastus on: C. 

## Kategooriad

- Info mõistmine
- Kodeerimine

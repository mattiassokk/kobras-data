# 4. Koprakood

Selleks, et ennast kiskjate eest kaitsta, leiutasid koprad salakoodi.

Esimesele reale kirjutavad koprad kõik tähed tähestiku järjekorras. Järgmine rida algab nn. võtmesõnaga ning sellele järgnevad tähestiku ülejäänud tähed, kusjuures iga täht esineb vaid üks kord.

Kui võtmesõnaks valida "KOOBAS", siis selles on kaks O-tähte. Sellisel juhul jäetakse alles vaid esimene O ja tulemus on selline:

Tähestik

A B C D E F G H I J K L M N O P Q R S Š Z Ž T U V W Õ Ä Ö Ü X Y

Tähestik salakoodi jaoks

K O B A S C D E F G H I J L M N P Q R Š Z Ž T U V W Õ Ä Ö Ü X Y

Kodeerimisel asendatakse sõna iga täht talle vastava alumise rea tähega, näiteks sõna "EKSAM" on salajasel kujul "SHRKJ". Saades salasõnumi ja soovides sellest aru saada, tuleb seda protsessi rakendada tagurpidi.

## Küsimus

Kuidas kodeeritakse sõna "REBASED", kui võtmesõna on "LIIKUV"?

[Raadionupud]

A. PVILQUV

B. PVILQVU

C. PVILZVU

D. PVILZUV

## Vastus

Õige vastus on: B.

## Vastuse selgitus

A B C D E F G H I J K L M N O P Q R S Š Z Ž T U V W Õ Ä Ö Ü X Y

L I K U V A B C D E F G H J M N O P Q R S Š Z Ž T W Õ Ä Ö Ü X Y

## Kategooriad

- Kodeerimine

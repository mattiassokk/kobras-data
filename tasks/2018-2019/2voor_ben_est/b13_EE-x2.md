# 13. Failid ja kaustad

Mati avas arvuti failihalduris kausta, mille nimi koos täisteega on `C:\Riistvara\Sisendseadmed\Pildid`.

![](b13-body.png)

Seejärel liikus ta ühe taseme võrra ülespoole ja avas seal kausta `Kirjeldused`. Seejärel liikus ta kahe taseme võrra ülespoole.

## Küsimus

Milline kaust on Mati failihalduris nüüd avatud?

[Raadionupud]

A. `C:\`

B. `C:\Riistvara`

C. `C:\Riistvara\Sisendseadmed`

D. `C:\Riistvara\Sisendseadmed\Kirjeldused`

## Vastus

Õige vastus on: B.

## Kategooriad

- Failihaldus

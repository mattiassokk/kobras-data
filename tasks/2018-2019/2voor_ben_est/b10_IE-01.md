# 10. Jäätisemasinad

Meil on kaks jäätisemasinat, mõlemad kasutavad nelja maitset:
![](b10-body-s.png) ![](b10-body-l.png) ![](b10-body-b.png) ![](b10-body-t.png)

Esimene masin kasutab jäätise tegemisel järgmist juhendit:

0. Võta tühi vahvlitorbik.

1. Vali juhuslik maitse ja lisa kaks selle maitsega jäätisepalli.

2. Lisa üks mingi teise maitsega jäätisepall.

3. Kui soovitud kõrgus on saavutatud, lõpeta, vastasel juhul jätka sammust 1.

Teine masin valib jäätisepalle täiesti juhuslikult ilma mingite piiranguteta.

## Küsimus

Piltidel on näha nelja jäätise alumised otsad. Milline jäätistest on kindlasti pärit teisest masinast?

[Raadionupud]

A. ![](b10-llsbblt.png)

B. ![](b10-sstllbb.png)

C. ![](b10-sstttbl.png)

D. ![](b10-ttssbll.png)

## Vastus

Õige vastus on: D.

## Kategooriad

- Algoritmid

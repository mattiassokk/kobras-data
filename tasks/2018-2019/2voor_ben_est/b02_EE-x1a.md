# 2. Piltide avaldamine

Creative Commons süsteemis märgitakse teose kasutamise tingimusi järgmiste sümbolitega:

![](b02-cc-by.png) - teose kasutamisel peab autorile viitama;

![](b02-cc-nd.png) - teost tohib kasutada ainult muutmata kujul;

![](b02-cc-nc.png) - teost tohib kasutada ainult mitteärilistel eesmärkidel;

![](b02-cc-sa.png) - teose muutmisel peab uut varianti jagama samade tingimustel, mis olid originaalil.

Kobras Anu tegi reisil pilte ja tahab parimad neist veebi välja panna, et ka teised saaks maailma ilu nautida.

## Küsimus

Millise sümbolite kombinatsiooni peaks Anu oma piltide juurde lisama, et teised kasutajad ei tohiks neid müüa, aga võiks neid töödelda ja tulemusi samadel tingimustel edasi jagada, viidates seejuures Anule kui esialgsele autorile?

[Raadionupud]

A. ![](b02-va.png)

B. ![](b02-vb.png)

C. ![](b02-vc.png)

D. ![](b02-vd.png)

## Vastus

Õige vastus on: B.

## Kategooriad

- Info mõistmine
- Varia

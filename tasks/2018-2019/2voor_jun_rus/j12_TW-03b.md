# 12. Бобрешки

Бобрешки - это деревянные куклы в форме бобров, которые можно вложить друг в друга. Каждую куклу можно по середине раскрыть и внутрь вложить какую-нибудь другую бобрешку, диаметр и высота которой будет меньше, чем у внешней куклы.

![](j12-body-fit.png) 

![](j12-body-nofit.png) 

У Эммы имеется несколько таких кукол, и она хочет их выставить на полку лучшим образом.

На приведённом рисунке видны все бобрёшки Эммы.  

![](j12-question.png) 

## Вопрос

Каково наименьшее возможное количество собранных кукол она может разместить на полке?

[Täisarv]

## Ответ

Правильный ответ: 2.

## Категории

- Логика
- Понимание информации

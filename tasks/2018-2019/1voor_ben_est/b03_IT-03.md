# 3. Kurikad

Koprad Ann, Ben, Cat ja Dan mängivad kurikatega.

Õpetaja andis neile ülesande panna seitse kurikat ritta nii, et

* vasakult kolmas kurikas oleks punane;

* iga punase kurika parempoolne naaber oleks ka punane.

Kopralapsed tegid sellised read:

![](b03-body.png)

## Küsimus

Kes neist **ei täitnud** õpetaja antud ülesannet?

[Raadionupud]

A. Ann

B. Ben

C. Cat

D. Dan

## Vastus

Õige vastus on: D.

## Kategooriad

- Info mõistmine

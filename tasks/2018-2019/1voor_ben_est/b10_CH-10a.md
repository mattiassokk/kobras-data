# 10. Bruno riided

Kopraema paneb Bruno riided lauale virna.

|  Särk                |  Alussärk                 |  Püksid              |  Aluspüksid               |  Traksid              |  Sokid               |  Kingad              |
|----------------------|---------------------------|----------------------|---------------------------|-----------------------|----------------------|----------------------|
|  ![](b10-shirt.png)  |  ![](b10-undershirt.png)  |  ![](b10-pants.png)  |  ![](b10-underpants.png)  |  ![](b10-braces.png)  |  ![](b10-socks.png)  |  ![](b10-shoes.png)  |


Bruno paneb riideid selga alati selles järjekorras, nagu nad laual on, alustades kõige pealmisest.

Brunole meeldivad kõik tema riided, aga ta ei taha kunagi kanda trakse särgi all.

## Küsimus

Milline järgmistest kuhjadest on Bruno jaoks õiges järjekorras?

[Raadionupud]

A. ![](b10-answer-a.png)
B. ![](b10-answer-b.png)
C. ![](b10-answer-c.png)
D. ![](b10-answer-d.png)

## Vastus

Õige vastus on: B.

## Kategooriad

- Info mõistmine

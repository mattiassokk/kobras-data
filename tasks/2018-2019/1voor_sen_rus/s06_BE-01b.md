# 6. Переворачивание карт

На столе выложен ряд игральных карт. Каждая карта может быть открытой (лицом наверх) ![](s06-face-up.png) или закрытой (рубашкой наверх) ![](s06-face-down.png) .

Во время своего хода игрок смотрит карты справа налево:

* Если данная карта закрыта, игрок открывает эту карту (переворачивает лицом наверх) и заканчивает свой ход.

* Если данная карта открыта, игрок закрывает эту карту (переворачивает рубашкой наверх) и переходит к следующей карте.

* Если карт больше нет, игрок заканчивает свой ход.

Например, если игрок начинает свой ход при таком раскладе карт

![](s06-face-down.png) ![](s06-face-down.png) ![](s06-face-down.png) ![](s06-face-up.png) ![](s06-face-down.png) ![](s06-face-up.png) ![](s06-face-up.png) ,

то сначала игрок переворачивает первую карту с правой стороны, затем вторую карту с правой стороны и третью; затем игрок заканчивает свой ход, так как третья карта перевернута лицом наверх:

![](s06-face-down.png) ![](s06-face-down.png) ![](s06-face-down.png) ![](s06-face-up.png) ![](s06-face-up.png) ![](s06-face-down.png) ![](s06-face-down.png) .

## Вопрос

Если игрок начинает игру, в которой 7 карт закрыты (рубашкой наверх):

![](s06-face-down.png) ![](s06-face-down.png) ![](s06-face-down.png) ![](s06-face-down.png) ![](s06-face-down.png) ![](s06-face-down.png) ![](s06-face-down.png) ,

то сколько ходов должен сделать игрок, чтобы все карты были бы открыты (лицом наверх)
![](s06-face-up.png) ![](s06-face-up.png) ![](s06-face-up.png) ![](s06-face-up.png) ![](s06-face-up.png) ![](s06-face-up.png) ![](s06-face-up.png) ?

[Raadionupud]

A. Меньше 10 ходов

B. Как минимум 10, но меньше 100 ходов

C. Как минимум 100, но меньше 1000 ходов

D. Как минимум 1000 ходов

E. В игре никогда не наступит такая ситуация, в которой все 7 карт были бы открыты

## Ответ

Правильный ответ: C.

## Категории

- Дискретная математика

# 7. Palju tööd

Töökojas on robot, mis oskab teha mitmesuguseid töid. Erinevate tööde tegemiseks kulub robotil erinev arv tunde.

Iga tunni alguses otsustab robot selle tunni kasutamise üle järgmiste reeglite järgi:

* Kui on tulnud uus töö, alustab ta kohe selle tegemist.

* Vastasel korral jätkab ta selle tööga, mis on kõige kauem ootel seisnud.

Näiteks, kui töökotta saabuvad järgmised tööd:

1. 7-tunnine kell 8,

2. 3-tunnine kell 10,

3. 5-tunnine kell 12,

siis jagab robot oma aega nende vahel järgmiselt:

![](s07-body.png)

Seega lõpetab robot esimese töö kell 22, teise kell 17 ja kolmanda kell 23.

## Küsimus

Ühel päeval saabusid töökotta järgmised tööd:

1. 5-tunnine kell 8,

2. 3-tunnine kell 11,

3. 5-tunnine kell 14,

4. 2-tunnine kell 17.

Mis kell (24h) lõpetab robot töö number 4?

[Täisarv [1, 24]]

## Vastus

Õige vastus on: 20.

## Kategooriad

- Algoritmid
- Info mõistmine

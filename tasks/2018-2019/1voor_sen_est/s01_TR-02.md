# 1. eKool

Juku sai meili, mille saatjaks on märgitud "eKooli administraator". Meilis on kirjas, et Juku peab kohe saatma sellele vastuseks oma eKooli kasutajanime ja parooli, muidu tema konto suletakse ja kõik ta hinded kustutatakse. Juku kaalus järgmisi käitumisvariante:

1. Saata oma kasutajanimi ja parool, nagu nõutud.

2. Saata oma pinginaabri kasutajanimi ja mingi juhuslik sõna parooliks.

3. Kustutada meil sellele vastamata.

4. Saata oma kasutajanimi ja parool oma pinginaabrile.

5. Teatada meilist õpetajale ja vanematele.

## Küsimus

Millised neist tegevustest oleks Juku poolt mõistlikud?

[Raadionupud]

A. 1 ja 5

B. 2 ja 4

C. 2 ja 5

D. 3 ja 5

## Vastus

Õige vastus on: D.

## Kategooriad

- Turvalisus
- Varia

# 10. Кодовый замок

На двери дома Тиши имеется кодовый замок, четыре кнопки которого находятся внутри дома и еще четыре - снаружи дома. Чтобы открыть дверь, надо находящиеся снаружи кнопки повернуть так, чтобы они совпали с положением кнопок внутри дома. Исходное положение кнопок с обеих сторон следующее:

![](b10-body-1.png)

Каждую кнопку можно повернуть в любое положение. Поворот каждой кнопки можно описать с помощью буквы и числа. Буква `П` означает, что кнопку повернули по часовой стрелке (по направлению движения стрелки часов); буква `Л` означает, что кнопку повернули против часовой стрелки (в направлении противоположном движению стрелки часов). Число означает, на сколько градусов повернули кнопку.

Например, поворот кнопки ![](b10-body-2.png) в положение ![](b10-body-3.png) описывает `П90`.

Выходя сегодня утром из дома, Тиша повернул расположенные внутри дома кнопки следующим образом:

![](b10-body-4.png)

## Вопрос

Какое сообщение Тиша должен послать другим членам семьи, чтобы они смогли открыть дверь снаружи?

[Raadionupud]

A. `П90Л90П90Л90`

B. `Л90Л90П90П90`

C. `Л90П90Л90П90`

D. `П90П90Л90Л90`

## Ответ

Правильный ответ: B.

## Объяснение ответа

TODO

## Это информатика!

TODO

## Категории

- Геометрия
- Кодирование

# 11. Учет продуктов

В конце каждого месяца семьи бобров записывают, сколько веточек ив они съели за текущий месяц.

В таблице приведены данные по четырем семьям:

![](b11-body.png)

## Вопрос

Какой из представленных графиков описывает потребление веток семьей Бобровых?

[Raadionupud]

A. ![](b11-variant-a.png)
B. ![](b11-variant-b.png)

C. ![](b11-variant-c.png)
D. ![](b11-variant-d.png)

## Ответ

Правильный ответ: C.

## Объяснение ответа

График A не может быть правильным ответом, потому что график показывает равное потребление веток в октябре и ноябре, а Бобровы съели в октябре больше веток, чем в ноябре. (На самом деле, это график потребления веток семьей Герасимовых.)

График B не может быть правильным ответом, потому что график показывает большее потребление веток в сентябре, чем в ноябре, а Бобровы съели в сентябре меньше веток, чем в ноябре. (На самом деле, это график потребления веток семьей Васильевых.)

График D не может быть правильным ответом, потому что график показывает увеличивающееся с каждым месяцем потребление веток, а Бобровы съели в ноябре меньше веток, чем в октябре. (На самом деле, это график потребления веток семьей Антоновых.)

Форма графика C соответствует потреблению веток семьей Бобровых: в сентябре было самое маленькое потребление веток, в ноябре чуть больше, в октябре еще больше, а в декабре потребление было самым большим.

## Это информатика!

TODO

## Категории

- Понимание информации
- Обработка таблицы

# 12. Salapärane klahv

Kati trükib arvutisse oma referaadi teksti. Tema kass astub korraks klaviatuurile, aga pealtnäha ei juhtu midagi, ja Kati trükib teksti edasi.

Natukese aja pärast märkab Kati, et tal on tekstis üks sõna vahele jäänud. Ta paneb kursori õigesse kohta ja hakkab trükkima vahelejäänud sõna. Oma üllatuseks märkab ta, et uut sõna trükkides kustub selle järel olev sõna tähthaaval ära.

## Küsimus

Millisele klahvile Kati kass astus?

[Raadionupud]

A. Delete (Del) klahvile

B. Escape (Esc) klahvile

C. Insert (Ins) klahvile

D. Tab klahvile

## Vastus

Õige vastus on: C (Insert klahvile).

## Vastuse selgitus

TODO

## See on informaatika!

TODO

## Kategooriad

- Tekstitöötlus

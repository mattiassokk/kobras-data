# 11. Toidustatistika

Koprapered panevad iga kuu lõpus kirja, mitu pajuoksa nad selle kuu jooksul ära söönud on.

Järgnevas tabelis on nelja pere andmed:

![](b11-body.png)

## Küsimus

Milline järgnevatest graafikutest kujutab perekond Beebikute tarbimist?

[Raadionupud]

A. ![](b11-variant-a.png)
B. ![](b11-variant-b.png)

C. ![](b11-variant-c.png)
D. ![](b11-variant-d.png)

## Vastus

Õige vastus on: C.

## Vastuse selgitus

Graafik A ei saa olla õige vastus, sest graafikul on oktoobri ja novembri tarbimised võrdsed, aga Beebikud sõid oktoobris rohkem kui novembris. (Tegelikult on see Deedikute tarbimise graafik.)

Graafik B ei saa olla õige vastus, sest graafikul on septembri tarbimine suurem kui novembri oma, aga Beebikud sõid septembris vähem kui novembris. (Tegelikult on see Ceedikute tarbimise graafik.)

Graafik D ei saa olla õige vastus, sest graafikul on iga kuu tarbimine eelmise kuu omast suurem, aga Beebikud sõid novembris vähem kui oktoobris. (Tegelikult on see Aavikute tarbimise graafik.)

Graafiku C kuju vastab Beebikute tarbimisele: septembris oli tarbimine kõige väiksem, novembris natuke suurem, oktoobris veel suurem ja detsembris kõige suurem.

## See on informaatika!

TODO

## Kategooriad

- Tabelitöötlus
- Info mõistmine


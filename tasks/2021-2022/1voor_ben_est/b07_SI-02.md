# 7. Puult puule

Jack ei ole kobras. Ta on hoopis pärdik, kes elab pargis.

Iga puu otsast võib Jack edasi hüpata teisele puule, mis on kas ühe või kahe ruudu kaugusel samas reas või veerus, või ühe ruudu kaugusel diagonaalis:

![](b07-body-1.png)

Täna mängib Jack mängu, kus ta hüppab ühelt puult teisele ilma maad puutumata. Ta võib alustada ükskõik milliselt puult pargis ja tahab käia võimalikult paljude puude otsas.

Puud paiknevad pargis järgmiselt:

![](b07-body-2.png)

## Küsimus

Mis on kõige suurem arv puid, lähtepuu kaasa arvatud, mille otsa Jack võib jõuda ilma vahepeal kordagi maad puutumata?

[Täisarv]

## Vastus

Õige vastus on: 8.

## Vastuse selgitus

Jack saab ilma maad puutumata käia kõigi 8 alloleval kaardil sinisega märgitud puu otsas.

![](b07-expl-1.png)

Puud pargis jagunevad kuude gruppi, mis on kaardil märgitud erinevate värvidega. Ükskõik millise puu otsast alustades võib Jack jõuda kõigi sama värvi puude otsa, kuid ei pääse ilma maad puutumata ühegi teist värvi puu otsa. Sinine grupp on kõige suurem, selles on 8 puud.

Kuidas selliseid gruppe leida? Vali üks puu ja värvi see mingi värviga, näiteks kollaseks. Siis värvi kollaseks kõik puud, mille otsa saab hüpata esimeselt kollaselt puult. Siis värvi kõik need, mis on veel värvimata, aga mille otsa saab mõnelt kollaselt puult. Jätka seda seni, kuni on veel värvimata puid, mille otsa saab mõnelt kollaselt puult. Nii oled leidnud esimese grupi. Kui on veel värvimata puid, vali üks neist, värvi see mingi järgmise värviga ja korda seda protsessi.

## See on informaatika!

Arvutiteaduse seisukohalt on meil tegemist _graafi_ nimelise andmestruktuuriga. Meie ülesandes on puud selle graafi _tipud_ ja kaks puud on omavahel ühendatud _servaga_, kui Jack saab ühelt puult teisele hüpata. Alloleval joonisel on graafi servad märgitud mustade joontega.

![](b07-expl-2.png)

Kui kahe puu vahel on sellistest servadest koosnev tee, kuuluvad nad ühte gruppi ja eelpool kirjeldatud gruppide leidmise protsess värvib nad ühte värvi. Graafiteooria keeles nimetatakse selliseid gruppe _sidususkomponentideks_. Kaks põhilist meetodit, mida nende leidmiseks kasutada, on _laiuti otsing_, mida me eelpool juba selgitasime, ja _sügavuti otsing_.

### Veebiviited

* Graaf: <https://en.wikipedia.org/wiki/Graph_(discrete_mathematics)>
* Sidususkomponent: <https://en.wikipedia.org/wiki/Component_(graph_theory)>
* Laiuti otsing: <https://en.wikipedia.org/wiki/Breadth-first_search>
* Sügavuti otsing: <https://en.wikipedia.org/wiki/Depth-first_search>

## Kategooriad

- Info mõistmine
- Graafid

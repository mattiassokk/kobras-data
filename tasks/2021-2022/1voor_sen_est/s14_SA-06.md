# 14. Piltide pakkimine

Rastergraafikas esitatakse pilte väikestest ühevärvilistest ruutudest (pikselitest) koosnevate mosaiikidena. Must-valges pildis võib iga piksel olla kas must või valge, mida tavaliselt tähistatakse vastavalt arvudega 0 ja 1.

Selleks, et vähendada piltide salvestamiseks vajalikku andmemahtu, kasutatakse erinevaid pakkimismeetodeid. Allolev joonis illustreerib üht lihtsat meetodit must-valgete piltide pakkimiseks.

![](s14-body.png)

Iga rea esimene arv näitab järjestikuste valgete pikselite arvu (kui esimene piksel on must, siis algab rida arvuga 0) ja edasi on vaheldumisi mustade ja valgete pikselite arvud.

## Küsimus

Millist pilti kodeerivad järgmised andmed?

```
15
5, 3, 7
4, 1, 3, 1, 6
3, 1, 5, 1, 5
2, 1, 7, 1, 4
1, 1, 4, 1, 4, 1, 3
1, 1, 3, 3, 3, 1, 3
1, 1, 4, 1, 4, 1, 3
2, 1, 7, 1, 4
3, 1, 5, 1, 5
4, 1, 3, 1, 1, 1, 4
5, 3, 3, 1, 3
12, 1, 2
13, 1, 1
15
```

[Raadionupud]

A. ![](s14-variant-a.png)
B. ![](s14-variant-b.png)

C. ![](s14-variant-c.png)
D. ![](s14-variant-d.png)

## Vastus

Õige vastus on: B.

## Vastuse selgitus

TODO

## See on informaatika!

TODO

## Kategooriad

- Kodeerimine
- Info mõistmine

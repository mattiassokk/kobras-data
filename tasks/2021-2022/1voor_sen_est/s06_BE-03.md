# 6. Kaelakee kirjeldus

Jean armastab kaelakeid meisterdada. Ta tahab oma keede kirjeldusi ka sõpradega jagada ja kasutab selleks omaloodud keelt.

Iga helmest märgib selle kujule vastav täht `S` (_star_, viisnurk), `T` (_triangle_, kolmnurk), `R` (_rectangle_, nelinurk), `L` (_line_, kriips). Lihtsalt helmeste järjest üles kirjutamise asemel kasutab Jean kavalamat skeemi:

* kui kees on järjest mitu ühesugust helmest, võib kirjutada helmeste arvu ja selle järele helme tähise;
* kui kees on korduv lõik, võib kirjutada kordumiste arvu ja selle järele sulgudesse lõigu kirjelduse.

Näiteks kee

![](s06-body-1.png)

üks võimalik kirjeldus on `S3(TR)3SL`. Selle kirjelduse pikkus on 9 märki.

## Küsimus

Mitu märki on alloleva kee minimaalse pikkusega kirjelduses (loetakse nii tähti, numbreid kui ka sulge)?

![](s06-body-2.png)

[Raadionupud]

A. 12 märki

B. 13 märki

C. 14 märki

D. 15 märki

## Vastus

Õige vastus on: B (13 märki).

## Vastuse selgitus

TODO

## See on informaatika!

TODO

## Kategooriad

- Info mõistmine

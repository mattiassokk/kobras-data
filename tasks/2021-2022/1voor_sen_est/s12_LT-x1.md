# 12. Burrows-Wheeleri teisendus

Vaatleme järgmist algoritmi antud sõna teisendamiseks:

1. lisada sõna lõppu marker `*`;
2. teha loend saadud teksti kõigist võimalikest tsüklilistest nihetest;
3. sorteerida loend tähestikuliselt (lugedes markeri `*` tähestiku lõpus olevaks);
4. lugeda tulemuseks tekstide viimased märgid sorteeritud loendis.

Teksti tsükliliseks nihkeks nimetatakse teksti, mis saadakse nii, et võetakse teksti algusest mõned märgid ja pannakse nad (järjekorda muutmata) teksti lõppu.

Kui näiteks rakendame seda algoritmi sõnale `BANANA`, saame tulemuseks `BNN*AAA`:

Markeriga tekst | Tsüklilised nihked | Sorteeritud loend | Lõplik tulemus
----------------|--------------------|-------------------|---------------
`BANANA*`       | `BANANA*`          | `ANANA*B`         | `BNN*AAA`
|               | `ANANA*B`          | `ANA*BAN`         |
|               | `NANA*BA`          | `A*BANAN`         |
|               | `ANA*BAN`          | `BANANA*`         |
|               | `NA*BANA`          | `NANA*BA`         |
|               | `A*BANAN`          | `NA*BANA`         |
|               | `*BANANA`          | `*BANANA`         |

## Küsimus

Milline on tulemus, kui rakendada seda algoritmi sõnale `BEBRAS`?

Sisesta vastuseks vajalikud märgid ilma tühikuteta!

[Tekstikast]

## Vastus

Õige vastus on: `R*EBBAS`.

## Vastuse selgitus

TODO

Markeriga tekst | Tsüklilised nihked | Sorteeritud loend | Lõplik tulemus
----------------|--------------------|-------------------|---------------
`BEBRAS*`       | `BEBRAS*`          | `AS*BEBR`         | `R*EBBAS`
|               | `EBRAS*B`          | `BEBRAS*`         |
|               | `BRAS*BE`          | `BRAS*BE`         |
|               | `RAS*BEB`          | `EBRAS*B`         |
|               | `AS*BEBR`          | `RAS*BEB`         |
|               | `S*BEBRA`          | `S*BEBRA`         |
|               | `*BEBRAS`          | `*BEBRAS`         |

## See on informaatika!

TODO

## Kategooriad

- Tekstitöötlus
- Algoritmid

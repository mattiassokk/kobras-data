# 5. Raudtee remont

Kopramaa raudteel vajab 12-kilomeetrine lõik remonti.

![](s05-body.png)

Koprad on aga laisavõitu ja ei viitsi suuri töid teha. Kui mõnel kopral palutakse raudteed remontida, käitub ta järgmiselt:

- Kui remonti vajav lõik ei ole üle 1 kilomeetri, teeb ta töö ise ära.

- Kui lõik on pikem, leiab ta kaks sõpra ja palub kummalgi remontida poole lõigust (ja ise ei tee midagi).

Ja kuna koprad on laisad, ei lase keegi ennast kaubelda rohkem kui ühte teelõiku remontima.

## Küsimus

Mitu kobrast lõpuks raudteed remondib, kui linnapea palub Arnoldil 12-kilomeetrise lõigu korda teha?

[Täisarv]

## Vastus

Õige vastus on: 16.

## Vastuse selgitus

TODO

## See on informaatika!

TODO

## Kategooriad

- Diskreetne matemaatika

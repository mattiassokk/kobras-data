# 15. Lauluvõistlus

Kopraküla lauluvõistluse finaalis osales kuus lauljat:

![](s15-body.png)

Lauljaid oli hindamas neli eksperdist kohtunikku, kes igaüks andsid hindeid oma kriteeriumide järgi:

 &nbsp; | Nancy | Michael | Grace | James
--------|-------|---------|-------|------ 
 Anna   | 80    | 8       | 60    | 0
 Betty  | 90    | 10      | 80    | 50
 Carrie | 85    | 7       | 90    | 100
 Dennis | 100   | 9       | 100   | 30
 Elin   | 95    | 6       | 70    | 10
 Frank  | 75    | 5       | 50    | 20

Võistluse korraldajad panid iga võistleja kohta kirja, mitmendal kohal see võistleja iga kohtuniku pingereas oli, liitsid iga võistleja jaoks kokku need neli kohanumbrit ja kuulutasid üldvõitjaks selle, kellel nii saadud summa oli kõige väiksem.

## Küsimus

Kes võistluse võitis?

[Raadionupud]

A. Anna

B. Betty

C. Carrie

D. Dennis

E. Elin

F. Frank

## Vastus

Õige vastus on: D (Dennis).

## Vastuse selgitus

TODO

## See on informaatika!

TODO

## Kategooriad

- Varia
- Info mõistmine

# 10. Rosett

Programm `diamond` joonistab rombi, nagu alloleval joonisel näidatud.

![](s10-body-1.png)

Vaatame nüüd programmi `rosette`, mis peaks joonistama alloleval joonisel näidatud roseti.

![](s10-body-2.png)

## Küsimus

Millised arvud peavad programmis `rosette` olema `X` ja `Y` asemel, et soovitud pilt saada?

[Raadionupud]

A. `X` = 12, `Y` = 30

B. `X` = 15, `Y` = 24

C. `X` = 15, `Y` = 36

D. `X` = 12, `Y` = 20

## Vastus

Õige vastus on: B (`X` = 15, `Y` = 24).

## Vastuse selgitus

TODO

## See on informaatika!

TODO

## Kategooriad

- Geomeetria
- Algoritmid

# 11. Счета за коммунальные услуги

Для поддержания бобровой плотины в порядке город взимает плату в размере 2 бобриков с каждого бобра, а за очистку воды - по 3 бобрика с каждой семьи.

![](s11-body.png)

## Вопрос

Какую формулу должен записать бухгалтер в ячейку C2, чтобы при ее копировании в ячейки от C3 до C5 рассчитывались правильные суммы для каждой семьи?

[Raadionupud]

A. `=B2*C8+C7`

B. `=B2*C7+C8`

C. `=B2*$C$7+$C$8`

D. `=B2*$C$8+$C$7`

## Ответ

Правильный ответ: C.

## Vastuse selgitus

TODO

## See on informaatika!

TODO

## Категории

- Обработка таблицы

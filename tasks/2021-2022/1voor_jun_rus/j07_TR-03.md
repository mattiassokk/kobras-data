# 7. Марсианский код

Для передачи своих сообщений марсиане зашифровывают каждое слово с помощью пары чисел.

Первое число в паре указывает на сумму значений имеющихся в слове букв. Каждой букве соответствует приведенное ниже значение:

A | Б | M | Н  | O  | Р   | С   | T   | У
--|---|---|----|----|-----|-----|-----|----
1 | 2 | 4 | 10 | 50 | 180 | 300 | 650 | 960

Второе число в паре указывает на индексы расположенных в алфавитном порядке букв.

Например, слово `МАРС` зашифровывается следующим образом:

- суммой значений букв `M`, `A`, `Р` и `С` будет 4+1+180+300=485;
- если мы расположим буквы в слове `MAРС` по алфавиту, то получим `AMРС`; таким образом, индексом для буквы `A` будет 1, индексом для буквы `M` будет 2, индексом для буквы `Р` будет 3 и индексом для буквы `С` будет 4; в результате последовательностью индексов будет 2134;
- в итоге кодом для слова `MAРС` будет 485;2134.

## Вопрос

Какой код будет в этой системе для слова `САТУРН`?

[Raadionupud]

A. 1440;415632

B. 1440;718964

C. 2101;415632

D. 2101;718964

## Ответ

Правильный ответ: C (2101;415632).

## Объяснение ответа

TODO

## Это информатика!

TODO

## Категории

- Понимание информации
- Кодирование

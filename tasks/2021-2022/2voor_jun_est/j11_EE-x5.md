# 11. Üüriarved

Kopralinn kogub tammi korrashoiuks 2 bebrikut kopra kohta ja veepuhastuseks 3 bebrikut pere kohta.

![](j11-body.png)

## Küsimus

Millise valemi peab raamatupidaja kirjutama tabeli lahtrisse C2, et selle kopeerimisel lahtritesse C3 kuni C5 arvutataks kõigile peredele õiged arve summad?

[Raadionupud]

A. `=B2*C8+C7`

B. `=B2*C7+C8`

C. `=B2*$C$7+$C$8`

D. `=B2*$C$8+$C$7`

## Vastus

Õige vastus on: C.

## Vastuse selgitus

TODO

## See on informaatika!

TODO

## Kategooriad

- Tabelitöötlus

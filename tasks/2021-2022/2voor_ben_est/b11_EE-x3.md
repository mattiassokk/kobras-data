# 11. Tabelarvutus

![](b11-body.png)

## Küsimus

Milline väärtus tekib nende valemite järgi arvutades töölehe lahtrisse D4?

[Täisarv]

## Vastus

Õige vastus on: 7.

## Vastuse selgitus

TODO

## See on informaatika!

TODO

## Kategooriad

- Tabelitöötlus

# 7. Kopraturniir

Kobras Kalju vaatas tenniseturniiri, kus iga mängu võitja pääses edasi järgmisse vooru. Ta kasutas võistlejate tähistamiseks kaarte numbritega ühest kaheksani ja märkis tahvlile iga mängu kohale selle võitja numbri.

Siis tuli tema noorem vend Tarmo, kes võttis kõik võitjate kaardid tahvlilt ja pani need tahvli alla numbrite järjekorda. Kalju ei saanud siiski pahaseks, sest ta saab kõik tulemused taastada.

![](b07-1.png)

## Küsimus

Kes võitis turniiri?

[Raadionupud]

A. Võistleja 1

B. Võistleja 2

C. Võistleja 4

D. Võistleja 8

## Vastus

Õige vastus on: C.


## Vastuse selgitus

Esimese vooru võitjad peavad olema need, kelle numbrid on kaartide hulgas. Kui need kaardid tabelisse ära panna, peavad teise vooru võitjad olema need, kelle numbrid on allesjäänud kaartide hulgas. Nii jätkates saamegi taastada kõigi mängude tulemused:

![](b07-2.png)

## Kategooriad

- Graafid
- Info mõistmine

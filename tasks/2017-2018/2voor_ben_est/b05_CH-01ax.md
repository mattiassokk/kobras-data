# 5. Labürint

Martini robot seisab labürindi sissepääsu juures (must nool, noole ots näitab roboti suunda) ja peab jõudma väljapääsuni (punane ring).

Martin saab robotit juhtida järgmiste käskudega:

| Käsk           | Seletus                                     |
| -------------- | ------------------------------------------- |
| ![](b05-1.png) | Astu üks samm edasi ja pööra 90° vasakule   |
| ![](b05-2.png) | Astu üks samm edasi ja pööra 90° paremale   |
| ![](b05-3.png) | Astu kaks sammu edasi ja pööra 90° paremale |

Robot suudab meelde jätta vaid neli käsku, kuid pääseb siiski labürindist välja, kui kordab õigeid käske kaks korda.

![](b05-4.png) ![](b05-5.png)

## Küsimus

Millise käskude jada peab Martin robotile andma, et see jõuaks väljapääsuni?

[Raadionupud]

A. ![](b05-a.png)
B. ![](b05-b.png)
C. ![](b05-c.png)
D. ![](b05-d.png)

## Vastus

Õige vastus on: D.


## Vastuse selgitus

Seda käsujada kaks korda täites liigub robot nii:

![](b05-6.png)

## Kategooriad

- Algoritmid

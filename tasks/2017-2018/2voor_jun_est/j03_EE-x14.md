# 3. NFC

NFC (*Near Field Commmunication*, lähiväljaside) on uus populaarsust koguv tehnoloogia juhtmevabaks andmevahetuseks üksteisest mõne sentimeetri kaugusel olevate seadmete vahel.

## Küsimus

Mille abil NFC-side toimub?

[Raadionupud]

A. Ultraheli

B. Raadiolainete

C. Infrapunakiirguse

D. Ultraviolettkiirguse

## Vastus

Õige vastus on: B.

## Kategooriad

- Varia

# 6. Koprasport

Koprad võistlevad 100 meetri jooksus väljalangemistega turniiril, kus iga duelli võitja pääseb edasi järgmisse ringi.

Kaarel ennustab jooksude võitjaid järgmiste reeglite järgi:

1. Kui kahel jooksjal on sama pikkus, võidab kergem.
2. Kui kahel jooksjal on sama kaal, võidab pikem.
3. Kui kahel jooksjal on erinevad nii pikkus kui kaal, siis:
    a) Pikem jooksja võidab, kui tema pikkuse ja kaalu erinevus on suurem kui vastase pikkuse ja kaalu erinevus.
    b) Kergem jooksja võidab, kui tema pikkuse ja tema vastase pikkuse erinevus on alla 5 cm.
    c) Kui tingimused a) ja b) on mõlemad tõesed või mõlemad väärad, võidab väiksema stardinumbriga jooksja.

Tänavu osalevad turniiril kaheksa sportlast, kelle pikkused ja kaalud on järgmised:

1. 72 cm, 60 kg
2. 80 cm, 70 kg
3. 90 cm, 70 kg
4. 65 cm, 60 kg
5. 90 cm, 73 kg
6. 75 cm, 62 kg
7. 78 cm, 65 kg
8. 85 cm, 65 kg

Jooksjad loositi esimeseks ringiks järgmistesse paaridesse:

![](j06-graphics.png)

## Küsimus

Millise stardinumbriga sportlane Kaarli ennustuste kohaselt turniiri võitma peaks?

[Täisarv]

## Vastus

Õige vastus on: 3.

## Vastuse selgitus

Kaarli ennustusreeglite kohaselt peaks turniiritabel tulema selline:

![](j06-explanation.png)

## Kategooriad

- Graafid
- Info mõistmine

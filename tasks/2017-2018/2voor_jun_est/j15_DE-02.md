# 15. Naerata mulle!

Koprad lõid programmi, mis suudab fotodelt leida naeratavaid nägusid. Programm teeb selleks kaks sammu:

1. Eeltöötlus: pilt teisendatakse emotikonilaadseks mudeliks, mis koosneb kahest täpist ja joonest, need kujutavad silmade ja suu asukohta.

![](j15_samm_1.png)

2. Naeratuse tuvastamine: saadud mudelit võrreldakse mustriga, mis koosneb punastest joontest ja neljast rohelisest täpist.

![](j15_samm_2.png)

Pilt tunnistatakse naeratavaks näoks sel juhul, kui kõiki rohelisi punkte puudutab mõni leitud kujund, kuid ükski neist ei puuduta punaseid jooni.

Naeratav nägu:

![](j15_tulemus_pos.png)

Mittenaeratav nägu:

![](j15_tulemus_neg.png)

## Küsimus

Mitu naeratavat nägu tuvastatakse järgmisel pildil, mis on juba eeltöötluse läbinud?

![](j15_kysimus.png)

[Täisarv]

## Vastus

Õige vastus on: 4.

## Vastuse selgitus

![](j15_vastus.png)

## Kategooriad

- Varia

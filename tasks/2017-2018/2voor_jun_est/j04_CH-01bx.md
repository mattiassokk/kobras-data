# 4. Labürint

Martini robot seisab labürindi sissepääsu juures (must nool, noole ots näitab roboti suunda) ja peab jõudma väljapääsuni (punane ring).

Martin saab robotit juhtida järgmiste käskudega:

* Astu samm edasi: ![](j04_fwd.png)

* Astu samm edasi ja pööra 90° vasakule: ![](j04_fwd_left.png)

* Astu samm edasi ja pööra 90° paremale: ![](j04_fwd_right.png)

Robot suudab meelde jätta vaid kuni kaheksa käsku, kuid võib sama käskude jada korrata mitu korda.

![](j04_body1.png) ![](j04_body2.png)

## Küsimus

Millise käskude jada peab Martin robotile andma, et see jõuaks väljapääsuni?

[Raadionupud]

A. ![](j04_choice_a.png)
B. ![](j04_choice_b.png)
C. ![](j04_choice_c.png)
D. ![](j04_choice_d.png)

## Vastus

Õige vastus on: D.

## Vastuse selgitus

Seda käsujada kolm korda täites liigub robot nii:

![](j04_solution.png)

## Kategooriad

- Algoritmid

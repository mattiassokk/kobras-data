# 14. Stiilsed tekstid

Kobras Anna tahab oma sõpradele saata ilusa kutse uue rakendusega, mis lubab tal moodustada disainitud teksti.

* `*` rasvase kirja jaoks; näiteks `*kobras*` tulemus on <b>kobras</b>
* `/` kaldkirja jaoks; näiteks `/kobras/` tulemus on <i>kobras</i>
* `_` allajoonimise jaoks; näiteks `_kobras_` tulemus on <u>kobras</u>
* `$` värviga esile tõstmise jaoks; näiteks `$kobras$` tulemus on <span style="background-color: #FFFF00">kobras</span>

Stiile saab ka kombineerida, näiteks:

* `_/kobras/_` tulemus on <u><i>kobras</i></u>
* `/_kobras_/` tulemus on samuti <i><u>kobras</u></i>
* `$_kobras_$` tulemus on <span style="background-color: #FFFF00"><u>kobras</u></span>

Rakenduses saab ka sisestada pilte, kirjutades pildi nime `:` märkide vahele, näiteks:

* `:sünnipäev:` tulemus on ![](j14-1.png)
* `:saar:` tulemus on ![](j14-2.png)
* `:maja:` tulemus on ![](j14-3.png)

## Küsimus

Mida peab Anna sisestama, et saada tulemuseks järgnev sõnum:

<b>Sa</b> oled kutsutud <u>super</u> ![](j14-1.png) peole <b><u>minu</u></b> ![](j14-3.png), mis on ![](j14-2.png) peal. Seal on <span style="background-color: #FFFF00"><i>suurepärane muusika ja söök</i></span>. Näeme <u>seal!</u>

[Raadionupud]

A. `*Sa* oled kutsutud _super_ sünnipäev peole */minu/* :maja:, mis on :saar: peal. Seal on $/suurepärane muusika ja söök/$. Näeme /seal!/`

B. `*Sa* oled kutsutud _super_ :sünnipäev: peole *_minu_* :maja:, mis on :saar: peal. Seal on $/suurepärane muusika ja söök/$. Näeme _seal!_`

C. `*Sa* oled kutsutud _super_ :sünnipäev: peole */minu/* :maja:, mis on saar peal. Seal on $*suurepärane muusika ja söök*$. Näeme _seal!_`

D. `*Sa* oled kutsutud /super/ :sünnipäev: peole *_minu_* :maja:, mis on :saar: peal. Seal on $suurepärane muusika ja söök$. Näeme /seal!/`

## Vastus

Õige vastus on: B.

## Vastuse selgitus

Variandis A on `sünnipäev` ilma kooloniteta ja seda ei asendata pildiga. Variandis C on `*/minu/*` allajoonimise asemel kaldkirjas. Variandis D on `/super/` allajoonimise asemel kaldkirjas.

//MÄRKUS: tundub, et see sama küsimus üks ühele oli 2021-2022, 2.voor, jun/sen rühmas. Vt. seal toodud kommentaarid //

## Kategooriad

- Tekstitöötlus
- Info mõistmine

# 14. Tammiehitus

Koprad tahavad ehitada uue tammi. Neil on kaks tingimust:

1. Nad tahavad valminud tammi tugevust katsetada aasta kõige sajusemal kuul.
2. Tamm tuleks ehitada üks või kaks kuud enne katsetamist, ja selleks oleks hea valida võimalikult kuiv aeg.

Tammi ehitus võtab kobrastel täpselt ühe kuu.

Koprametsa sademete hulk kuude kaupa on järgmine:

![](b14-0.png)

## Küsimus

Millal peaks koprad tammi ehitama?

[Raadionupud]

A. Jaanuaris

B. Aprillis

C. Septembris

D. Detsembris

## Vastus

Õige vastus on: D.

## Kategooriad

- Info mõistmine

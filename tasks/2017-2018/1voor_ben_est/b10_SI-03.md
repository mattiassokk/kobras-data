# 10. Maasikajaht

Neli kobrast alustavad erinevatest kohtadest ujumist ja ujuvad alati nooltega näidatud suunas.

![](b10-0.png)

## Küsimus

Mitu kobrast jõuab maasikani?

[Raadionupud]

A. 1 kobras

B. 2 kobrast

C. 3 kobrast

D. 4 kobrast

## Vastus

Õige vastus on: B.

## Kategooriad

- Info mõistmine

# 1. Mõistatuslik aparaat

Vaatame pilti aastast 1971:

![](b01.png)

## Küsimus

Millega on tegemist?

[Raadionupud]

A. Maailma esimene elektrooniline kassaaparaat

B. Maailma esimene duubel-CD-mängija diskoritele

C. Maailma esimene sülearvuti

D. Maailma esimene digitaalne autoraadio

## Vastus

Õige vastus on: C.

## Kategooriad

- Varia

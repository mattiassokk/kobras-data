# 4. Robot

Kobras ostis kasutatud roboti ja tahab seda programmeerida teda kodustel koristustöödel aitama.

Robotil on kaks ratast ning kumbagi ratast juhib oma mootor.

Kui kobras annab robotile käsu edasi sõita, pöörlevad mõlemad rattad edasisuunas, kuid parempoolne natuke kiiremini kui vasakpoolne.

## Küsimus

Kuidas robot liigub?

[Raadionupud]

A. Liigub edasisuunas, kuid kaldub paremale

B. Liigub edasisuunas, kuid kaldub vasakule

C. Liigub otse edasi

D. Pöörleb kohapeal

## Vastus

Õige vastus on: B.

## Kategooriad

- Varia
- Info mõistmine

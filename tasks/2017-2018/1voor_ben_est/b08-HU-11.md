# 8. Koolileht

Koolilehe toimetuses on 10 õpilast, kes igal reedel tegelevad oma artiklite kirjutamise või toimetamisega. Allolevas tabelis on märgitud, mis kellaaegadel iga õpilane selleks tööks arvutit vajab.

![](b08-0.png)

Kõik arvutid toimetuses on ühesugused. Iga õpilane kasutab arvutit terve märgitud tunni jooksul ja kaks õpilast samal ajal ühte arvutit kasutada ei saa.

## Küsimus

Mitu arvutit on toimetuses minimaalselt vaja, et kõik õpilased oma tööd tehtud saaks?

[Raadionupud]

A. 4

B. 5

C. 6

D. 10

## Vastus

Õige vastus on: B.

## Kategooriad

- Info mõistmine
- Graafid
- Diskreetne matemaatika

# 2. Loterii

Sulle tuleb aadressilt `magic@loterii.com` meil, mis lubab 1000 €, kui saadad kirja edasi kümnele oma sõbrale ja teatad sellest aadressil `magic@loterii.com`.

## Küsimus

Milline on õige käitumisviis?

[Raadionupud]

A. Sa edastad meili oma sõpradele, kirjutades 10 aadressi komadega eraldatuna To: väljale ning lisad `magic@loterii.com` CC: väljale, et oma 1000 € kätte saada

B. Sa edastad meili oma sõpradele, saates selleks 10 eraldi meili, ning lõpuks vastad aadressile `magic@loterii.com`, et soovid oma 1000 € kätte saada

C. Sa vastad aadressile `magic@loterii.com` ja palud endale tulevikus rämpsposti mitte saata

D. Sa märgid selle meili rämpspostiks, et edaspidi aadressilt `magic@loterii.com` tulevaid kirju Sulle enam ei näidataks

## Vastus

Õige vastus on: D.

## Kategooriad

- Varia
- Turvalisus

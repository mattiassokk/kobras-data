# 3. Operatsioonisüsteem

## Küsimus

Millised järgnevatest tegevustest **ei ole** tarkvarapiraatlus?

[Märkeruudud]

A. Sa ostad ilma operatsioonisüsteemita arvuti ning jätad selle kapi otsa seisma, ilma et teda kunagi käivitaksid

B. Sa ostad ilma operatsioonisüsteemita arvuti, laenad sõbralt Windowsi DVD ja paigaldad oma uude arvutisse Windowsi

C. Sa ostad ilma operatsioonisüsteemita arvuti, laenad sõpradelt Windowsi ja Ubuntu Linuxi DVD-d ning paigaldad oma uude arvutisse mõlemad süsteemid

D. Sa ostad ilma operatsioonisüsteemita arvuti, laenad sõbralt Ubuntu Linuxi DVD ning paigaldad oma uude arvutisse Ubuntu Linuxi

## Vastus

Õige vastus on: A, D.

## Kategooriad

- Varia

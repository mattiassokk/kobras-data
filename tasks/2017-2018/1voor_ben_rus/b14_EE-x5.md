# 14. Строительство дамбы

Бобры хотят построить новую дамбу. Но они должны учесть два условия:

1. Они хотят испытать новую дамбу на прочность в тот месяц года, когда выпадает больше всего осадков.
2. Дамба должна быть построена за месяц или два до начала проверки, и для этого по мере возможности хорошо было бы выбрать сухое время.

На строительство дамбы требуется ровно один месяц.

Ниже указано количество осадков, выпадающее в лесу в разные месяцы года:

![](b14-0.png)

## Вопрос

Когда бобры должны приступить к строительству дамбы?

[Raadionupud]

A. В январе

B. В апреле

C. В сентябре

D. В декабре

## Ответ

Верный ответ: D.

## Категории

- Понимание информации

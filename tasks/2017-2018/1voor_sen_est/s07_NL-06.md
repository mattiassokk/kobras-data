# 7. Kulude vähendamine

Kopramaal on kaheksa raudteejaama ja viis raudteeliini. Joonisel on iga liin näidatud eraldi värviga. Igast peatusest on võimalik ükskõik millisesse teise peatusesse sõita kõige rohkem ühe ümberistumisega. Näiteks on võimalik saada peatusest B peatusesse H, kui sõita peatusest B lilla liiniga peatusesse F, istuda seal ümber oranžile liinile ja sõita sellega peatusesse H.

Raudteefirma soovib kulude kokkuhoiuks osa liine sulgeda. Pärast liinide sulgemist peab endiselt olema võimalik ükskõik millise kahe peatuse vahel reisimine maksimaalselt ühe ümberistumisega.

![](s07-a.png)

## Küsimus

Mis on suurim arv liine, mille firma võib kinni panna?

[Täisarv]

## Vastus

Õige vastus on 2.

## Kategooriad

- Graafid

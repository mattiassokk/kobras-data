# 2. Raamatukogu

Kui raamatukogu lugeja laenutab raamatu, seotakse raamatukogu andmebaasis tema isikuandmed laenutatud raamatu andmetega. Selline seos peab olema volitamata ligipääsu eest kaitstud.

## Küsimus

Millistel järgnevatest juhtudest ei taga raamatukogu piisaval määral isikuandmete kaitset?

[Märkeruudud]

A. Kui volitamata isik saab teada raamatukogu lugejate arvu.

B. Kui volitamata isik saab teada raamatute arvu, mida võib hetkel laenutada.

C. Kui volitamata isik saab teada, kui tihti mingit kindlat raamatut laenutatakse.

D. Kui volitamata isik saab mõne raamatu kohta teada seda laenutanud lugejate nimed.

E. Kui volitamata isik saab mõne lugeja kohta teada tema laenutatud raamatute andmed.

## Vastus

Õige vastus on D, E.


## Vastuse selgitus

Ainult juhtudel D ja E lekivad lugejate isikustatud laenutusandmed. Juhtudel B ja C lekivad ainult raamatute andmed, mis pole isikuandmete kaitse objekt. Juhul A lekib üldine satistika kogu lugejaskonna kohta, mis ei sisalda ühegi lugeja isikuandmeid.

## See on informaatika!

Isikuandmete kaitse on oluline, aga sama oluline on ka, et isikuandmete kaitset ei kasutataks ettekäändena igasugusest infojagamisest keeldumiseks.

## Kategooriad

- Varia
- Turvalisus

# 5. Värvi jälgimise robot

Mikk on ehitanud roboti, mis loeb värvilisi ruute, vahetab nende värve ja liigub ruutude vahel. Robot järgib ette antud reegleid, näiteks:

![](s05-a.svg) Kui näed punast ruutu, vaheta selle värv roheliseks ja liigu üks ruut paremale

![](s05-b.svg) Kui näed punast ruutu, vaheta selle värv roheliseks ja liigu üks ruut vasakule

Roboti algasend on kõige vasakpoolsemal ruudul. Robot tuvastab ruudu värvi, seejärel leiab reegli, mis algab tuvastatud värviga, siis vahetab ruudu värvi vastavalt reeglile, pärast mida liigub vastavalt reeglile. Robot kordab sama protseduuri uues asukohas. Kui robot ei suuda ruudule vastavat reeglit tuvastada või astub ruutudest väljapoole, lõpetab ta töö.

Robotile anti ruutude jada

![](s05-c.svg)

ja reeglid

![](s05-d.svg) ![](s05-e.svg) ![](s05-f.svg) ![](s05-g.svg)

## Küsimus

Kuidas näevad ruudud välja pärast roboti töö lõppu?

[Raadionupud]

A. ![](s05-h.svg)

B. ![](s05-i.svg)

C. ![](s05-j.svg)

D. ![](s05-k.svg)

## Vastus

Õige vastus on A.

## Kategooriad

- Algoritmid

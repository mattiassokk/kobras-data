# 9. Hüppajad

Peeter ja Heili mängivad koos lihtsat videomängu. Mängu eesmärgiks on juhtida finišisse kobras, kes liigub platvormidel hüpates. Platvormid on kahel tasemel: alumisel (kust kobras alati ka alustab) ja ülemisel. Aeg, mis kulub platvormi ületamiseks, on kirjas vastava platvormi all.

Näiteks:

* Kobras on punktis A 3 sekundit pärast starti;
* Kobras on punktis B 5 sekundit pärast starti;
* Kobras on punktis C 10 sekundit pärast starti;
* Kobras on finišis 15 sekundit pärast starti.

![](j09-1.png)

Kobras saab hüpata nii alumiselt platvormilt ülemisele kui vastupidi.

Peeter and Heili alustavad mängimist kumbki oma puldiga täpselt samal ajal, kuid neil on erinevad mänguplaanid:

![](j09-2.png)

## Küsimus

Mitmendal sekundil asuvad nii Peeter kui Heili ülemisel tasemel?

[Raadionupud]

A. 2. sekundil

B. 4. sekundil

C. 6. sekundil

D. 8. sekundil

## Vastus

Õige vastus on B (4. sekundil).


## Vastuse selgitus

Me võime leida vastuse sel viisil, et kirjutame üles numbreid "0" ja "1", kus "0" tähendab üht sekundit olemist alumisel tasemel ja "1" üht sekundit olemist ülemisel tasemel. Sel juhul näeb Peetri mängu kirjeldus välja "0001111001111000" ja Heili oma "0011001100110011". Selleks, et leida, millal mõlemad on ülemisel tasemel, peame otsima, millal on mõlema jada puhul kohakuti numbrid "1". Võime seda teha näiteks loogilise operatsiooniga AND:

<pre>
0001111001111000
0011001100110011
----------------
0001001000110000
</pre>

## See on informaatika!

Üks põhilistest informaatika probleemidest on info esitamine. Selles ülesandes võime olulise detailina märkida üles selle, millal mängija on ülemisel tasemel. Kui see on tehtud, saame kasutada operaatorit AND, et leida, millisel momendil on ülemise taseme tähistamiseks mõeldud "1" mõlemal mängijal korraga.

## Kategooriad

- Diskreetne matemaatika
- Loogika
- Info mõistmine

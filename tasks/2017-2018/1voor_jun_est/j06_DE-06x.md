# 6. Honomakato töökindel arvutivõrk

Honomakato saarestik koosneb viiest saarest: Ho, No, Ma, Ka ja To. Suurim saar Ho on ühendatud internetiga suure kaabli kaudu, väiksemad kaablid ühendavad saari Ho ja No, Ho ja Ka, Ka ja Ma ning Ka ja To. Nende kaablite abil on kõik väiksemad saared ühendatud Hoga ning seega ka internetiga.

![](j06-1.png)

Honomakato elanikud soovivad, et nende arvutivõrk oleks töökindel: kui suvaline üks väike kaabel katkeb, peaks kõigil saartel siiski võrguühendus säilima.

## Küsimus

Mitu väikest kaablit tuleb minimaalselt lisada, et Honomakato saarestiku arvutivõrk saaks töökindlaks?

[Raadionupud]

A. 0

B. 1

C. 2

D. 3

## Vastus

Õige vastus on C.


## Vastuse selgitus

Ükskõik milline järgnevatest paaridest teeb arvutivõrgu töökindlaks:

1. Ho-Ma ja No-To: Ho-Ma kindlustaks saari Ma ja Ka ning No-To saari No ja To katkestuste eest.
2. Ho-To ja No-Ma: Ho-To kindlustaks saari To ja Ka ning No-Ma saari Ma, No ja Ka katkestuste eest.
3. No-To ja No-Ma: No-To kindlustaks saari No, To ja Ka ning No-Ma saari Ma, No ja Ka katkestuste eest.
4. No-To ja Ma-To: No-To kindlustaks saari No, To ja Ka ning Ma-To saari Ma ja To katkestuste eest.
5. No-Ka ja Ma-To: No-Ka kindlustaks saari No ja Ka ning Ma-To saari Ma ja To katkestuste eest.
6. No-Ma ja Ma-To: No-Ma kindlustaks saari Ma, No ja Ka ning Ma-To saari Ma ja To katkestuste eest.

![](j06-2.png)

Samas on lihtne näha, et kindlasti ei saa hakkama vähem kui kahe uue kaabliga:

* kindlasti ei ole töökindlat ühendust saarel, kuhu jõuab ainult üks kaabel;
* saarestikus on kolm saart, kuhu jõuab ainult üks kaabel;
* iga uus kaabel suurendab kahe saare ühenduste arvu.

## See on informaatika!

Näitena toodud Honomakato saarestiku internetiühendus pole ainuüksi fragment arvutivõrgust, vaid selline struktuur on omane kogu internetile: kõik arvutid, telefonid, televiisorid, külmikud jms on tänapäeval ühendatud sarnasel moel. 1960. aastatel oli interneti loomisel üheks tähtsamaks põhimõtteks saavutada töökindlus: ühenduse katkemine kahe punkti vahel ei tohtinud häirida kogu võrgu tööd. Ka teiste võrgustike (teedevõrgud, tootmisahelad jne) jaoks on oluline, et kogu võrgustiku töökindlus ei sõltuks ühest ühendusest või seadmest.

Arvutiteadlased kasutavad graafiteooriat, et analüüsida erinevaid võrgustikke. Graaf on võrk, mis koosneb tippudest ja ühendustest (servadest, kaartest) nende vahel. Graafi nimetatakse sidusaks, kui iga suvalise kahe tipu vahel saab liikuda ühe või mitme serva kaudu. Kui graafis leidub serv, mille eemaldamisel muutuks graaf mittesidusaks, nimetatakse sellist serva sillaks. Töökindlates võrgustikes sildadest hoidutakse, õnneks on olemas erinevaid algoritme sildade tuvastamiseks.

## Kategooriad

- Graafid

# 12. Superkangelane

Superkangelane vaatab Kopralinna. Tal on vaja igast punktist jõe kaldal näha linna risti üle jõe jäävat punkti (vastavalt hallidele joontele).

![](j12-0.png)

* Kahjuks on jõe ja linna vahel 16 erineva pikkusega seina.
* Õnneks on superkangelasel võime näha läbi seina.
* Kahjuks suudab superkangelane näha ainult läbi ühe seina.
* Õnneks on superkangelane piisavalt tugev, et seinu lõhkuda.
* Kahjuks väsitab seina lõhkumine superkangelast.

## Küsimus

Kui mitu seina peab superkangelane minimaalselt ära lõhkuma, et kogu linna näha?

[Raadionupud]

A. 9

B. 10

C. 11

D. 12

## Vastus

Õige vastus on A (9).


## Vastuse selgitus

Selle asemel, et leida vähimat seinte arvu, mida superkangelane peab ära lõhkuma, keskendume hoopis suurimale arvule, mitu seina võib alles jääda, sisuliselt on ülesanne sama. Allpool näeme, et alles võib jääda maksimaalselt 7 seina. Kuna kokku on seinu 16, peab superkangelane lõhkuma ära 16-7=9 seina.

Keskendume seitsmele hallile seinale järgmisel joonisel.

![](j12-1.png)

Ükski paar neist hallidest seintest "ei kattu" omavahel, seega igast punktist vaadates jääb superkangelase ja linna vahele kõige rohkem üks sein. Nii saame öelda, et superkangelane võib alles jätta vähemalt 7 seina. Nüüd peame näitama, et superkangelane ei saa alles jätta rohkem kui 7 seina.

Seinu on nelja erineva pikkusega, nimetame neid pikkusi vastavalt nende omavahelistele proportsioonidele 1, 2, 3 ja 4.

![](j12-2.png)

Näeme, et superkangelane võib vasakpoolse seina A asemel jätta alles ühe kahest 2 ühiku pikkusega seinast B või C, seega on olemas alternatiivne variant seitsmest säilitatavast seinast pikkustega 2, 1, 2, 1, 1, 2 ja 1. Superkangelane ei saa lahendada ülesannet paremini, kuna ainult viis seina on pikkusega 1 ning kaks neist "kattuvad".

Miks me valisime seina pikkusega 3? Me tegime seda, lähtudes valitud lahenduskäigust, mitte konkreetsest ülesandest. Me alustasime seinade lõhkumist vasakult ja valisime alati järgmise mittekattuva seina, mis lõpeb võimalikult vasakul.

## See on informaatika!

Üldine lahendus on nn. ahne algoritm. See tähendab, et igal sammul tehakse selline valik, mis tundub olevat parim just sellel momendil, "lokaalselt". See ei vaata "suuremat pilti" ega arvesta kogu informatsiooniga.

Konkreetne ülesanne on näide ajastamise probleemist, mida informaatikud soovivad lahendada võimalikult efektiivse algoritmi abil. Kujutame vasakult paremale liikumist ette kui ajalist muutust ja seinu kui alamülesandeid, mida on vaja lahendada fikseeritud algus- ja lõpuajaga. Piirang, et superkangelane saab näha vaid läbi ühe seina, vastab sellele, et korraga saab sooritada vaid ühe alamülesande. Meie ülesande juures püüab superkangelane lahendada nii palju ülesandeid kui võimalik!

## Kategooriad

- Algoritmid

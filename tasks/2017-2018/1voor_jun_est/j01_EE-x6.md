# 1. Heli ja video

XIX sajandil elasid mitmed silmapaistvad infotehnoloogiaga tegelenud teadlased:

| ![](j01-1.jpg) | ![](j01-2.jpg) | ![](j01-3.jpg) | ![](j01-4.jpg) |
|-|-|-|-|
| Thomas Alva Edison | Vennad Lumière’id | Joseph Nicéphore Niépce | Valdemar Poulsen |

Nende saavutused võimaldavad salvestada ja kasutada heli, pilti ja videot:

* Thomas Alva Edison: esimene mehaaniline seade heli salvestuseks ja taasesituseks,
* Vennad Lumière’id: esimene film,
* Joseph Nicéphore Niépce: esimene foto,
* Valdemar Poulsen: esimene magnetiline helisalvestusseade.

## Küsimus

Milline on õige kronoloogiline järjekord?

[Raadionupud]

A. Niépce (1822), Lumière'id (1877), Edison (1888), Poulsen (1896)

B. Niépce (1822), Edison (1877), Lumière'id (1888), Poulsen (1896)

C. Lumière'id (1822), Edison (1877), Niépce (1888), Poulsen (1896)

D. Niépce (1822), Poulsen (1877), Lumière'id (1888), Edison (1896)

## Vastus

Õige on B.

## Vastuse selgitus

Õige vastuse saab leida ilmselt valede variantide välistamisega.

## Kategooriad

- Varia

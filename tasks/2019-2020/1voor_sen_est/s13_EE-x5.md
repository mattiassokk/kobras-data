# 13. Esiletõstetud tekst

Miial on tekstilõik

> Bebras is an international initiative to promote informatics and computational thinking among school students of all ages.
> Participants are usually supervised by teachers who may integrate the Bebras challenge into their teaching activities.
> You can find Bebras tasks from previous years on websites and in the Bebras brochures issued by many countries.
> The second week of November is declared World-Wide Bebras week for solving new tasks.
> Estonia also participates in the Bebras challenge.

Ta tahab selles lõigus kõik sõna "Bebras" esinemised kindlal viisil esile tõsta.

Selleks kasutab ta oma tekstitöötlusprogrammi funktsiooni "otsi ja asenda":

![](s13-main.png)

## Küsimus

Millise tulemuse Miia saab?

A.

> <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> is an international initiative to promote informatics and computational thinking among school students of all ages.
> Participants are usually supervised by teachers who may integrate the <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> challenge into their teaching activities.
> You can find <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> tasks from previous years on websites and in the <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> brochures issued by many countries.
> The second week of November is declared World-Wide <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> week for solving new tasks.
> Estonia also participates in the <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> challenge.

B.

> <span style="font-style: italic; color: green">Bebras</span> is an international initiative to promote informatics and computational thinking among school students of all ages.
> Participants are usually supervised by teachers who may integrate the <span style="font-style: italic; color: green">Bebras</span> challenge into their teaching activities.
> You can find <span style="font-style: italic; color: green">Bebras</span> tasks from previous years on websites and in the <span style="font-style: italic; color: green">Bebras</span> brochures issued by many countries.
> The second week of November is declared World-Wide <span style="font-style: italic; color: green">Bebras</span> week for solving new tasks.
> Estonia also participates in the <span style="font-style: italic; color: green">Bebras</span> challenge.

C.

> <span style="font-weight: bold; color: green">Bebras</span> is an international initiative to promote informatics and computational thinking among school students of all ages.
> Participants are usually supervised by teachers who may integrate the <span style="font-weight: bold; color: green">Bebras</span> challenge into their teaching activities.
> You can find <span style="font-weight: bold; color: green">Bebras</span> tasks from previous years on websites and in the <span style="font-weight: bold; color: green">Bebras</span> brochures issued by many countries.
> The second week of November is declared World-Wide <span style="font-weight: bold; color: green">Bebras</span> week for solving new tasks.
> Estonia also participates in the <span style="font-weight: bold; color: green">Bebras</span> challenge.

D.

> <span style="font-weight: bold; font-style: italic">Bebras</span> is an international initiative to promote informatics and computational thinking among school students of all ages.
> Participants are usually supervised by teachers who may integrate the <span style="font-weight: bold; font-style: italic">Bebras</span> challenge into their teaching activities.
> You can find <span style="font-weight: bold; font-style: italic">Bebras</span> tasks from previous years on websites and in the <span style="font-weight: bold; font-style: italic">Bebras</span> brochures issued by many countries.
> The second week of November is declared World-Wide <span style="font-weight: bold; font-style: italic">Bebras</span> week for solving new tasks.
> Estonia also participates in the <span style="font-weight: bold; font-style: italic">Bebras</span> challenge.

[Raadionupud]

A. Tulemuse A

B. Tulemuse B

C. Tulemuse C

D. Tulemuse D


## Vastus

Õige vastus on: D.

## Kategooriad

- Tekstitöötlus

# 1. Apple Pay

Käesoleval aastal jõudis ka Eestisse mobiilimakseid võimaldav teenus Apple Pay.

## Küsimus

Millal sai esimest korda maailmas seda teenust kasutada?

[Raadionupud]

A. 2015. a

B. 2016. a

C. 2017. a

D. 2018. a

## Vastus

Õige vastus on: A (2015. a).

## Kategooriad

- Varia

# 7. Kolmikhäda

Kopraemal on neli mänguasja. Ta tahab panna need nelja kasti, mis on
tähistatud tähtedega W, X, Y ja Z. Igasse kasti saab panna maksimaalselt
ühe mänguasja.

![](s07-main.png)

Kopraema soovib, et oleksid täidetud **kõik** järgmised tingimused:

1. Vähemalt üks järgnevatest väidetest on tõene: kastis X on mänguasi **või** kastis Y pole
mänguasja **või** kastis Z pole mänguasja.
2. Vähemalt üks järgnevatest väidetest on tõene: kastis W on mänguasi **või** kastis X on
mänguasi **või** kastis Z pole mänguasja.
3. Vähemalt üks järgnevatest väidetest on tõene: kastis X pole mänguasja **või** kastis Y pole
mänguasja **või** kastis Z on mänguasi.
4. Vähemalt üks järgnevatest väidetest on tõene: kastis W pole mänguasja **või** kastis X pole
mänguasja **või** kastis Y pole mänguasja.
5. Vähemalt üks järgnevatest väidetest on tõene: kastis X pole mänguasja **või** kastis Y on
mänguasi **või** kastis Z pole mänguasja.

## Küsimus

Mis on maksimaalne arv mänguasju, mida ta saab panna kastidesse nii, et
kõik need tingimused oleksid täidetud?

[Raadionupud]

A. 1

B. 2

C. 3

D. 4

## Vastus

Õige vastus on: C (3).

## Kategooriad

- Loogika
- Diskreetne matemaatika

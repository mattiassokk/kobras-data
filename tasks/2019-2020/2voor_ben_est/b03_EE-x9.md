# 3. Pildistamine koolis

Üha enam küsimusi tekitab koolis pildistamine ja filmimine. Millal võib seda teha ja millal on vaja küsida luba?

## Küsimus

Millistel juhtudel on koolis tehtud pildi kasutamiseks vaja pildil olevate kaaslaste luba?

Märgi kõik õiged vastused!

[Märkeruudud]

A. pildistamine endale mälestuseks

B. pildi avaldamine sotsiaalmeedias

C. pildi avaldamine ajakirjanduses

D. pildistamine tõendiks õiguskaitseorganitele

## Vastus

Õige vastus on: B, C.

## Kategooriad

- Turvalisus
- Varia

# 6. Teisendused

Üheksa lahtriga mängulaual on algseisus kuues vasakpoolses lahtris kolm punast ja kolm rohelist kaarti:

![](b06-initial.svg)

Teisenduse rakendamine tähendab teatavate kaartide tõstmist ühest lahtrist teise.
Lubatud on kaht liiki teisendusi (kus kaarte loendame vasakult paremale):

**1. teisendus:**
4. kaart tõsta 6. kaardist paremal olevasse tühja lahtrisse;
seejärel tõsta 1. kaart lahtrisse, kus enne oli 4. kaart.

**2. teisendus:**
5. kaart tõsta 6. kaardist paremal olevasse tühja lahtrisse;
seejärel tõsta 3. kaart lahtrisse, kus enne oli 5. kaart, ja 1. kaart lahtrisse, kus enne oli 3. kaart.

Teisendusi võib rakendada üksteise järel jadana.
Näiteks rakendades algseisule jada "1. teisendus; 2. teisendus", on tulemus selline:

pärast 1. teisendust:

![](b06-after1.svg)

ja pärast 2. teisendust:

![](b06-after12.svg)

## Küsimus

Millise teisenduste jada rakendamine viib algseisu järgnevaks lõppseisuks?

![](b06-question.svg)

[Raadionupud]

A. 1. teisendus; 2. teisendus; 1. teisendus

B. 2. teisendus; 1. teisendus; 1. teisendus

C. 1. teisendus; 1. teisendus; 2. teisendus

D. 1. teisendus; 1. teisendus; 1. teisendus

## Vastus

Õige vastus on: C.

## Kategooriad

- Kombinatoorika

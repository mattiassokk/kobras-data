# 1. WWW juubel

2019. aastal oli meile kõigile tuttaval veebil (WWW, World Wide Web) juubel.

## Küsimus

Kui vanaks sai veeb?

[Raadionupud]

A. 15 aastat

B. 20 aastat

C. 25 aastat

D. 30 aastat

## Vastus

Õige vastus on: D (30 aastat).

## Kategooriad

- Varia

# 9. Kosmosereis

Kosmonaudid saavad reisida planeetide vahel, kasutades rakette (![](b09-rocket.png)) või kosmoselaevu (![](b09-starship.png)):

![](b09-main.png)

Kui kosmonaut asub Veenusel (![](b09-venus.png)) ja soovib jõuda Saturnile (![](b09-saturn.png)), võib ta kõigepealt kasutada raketti, et lennata Jupiterile (![](b09-jupiter.png)), seejärel jätkata teekonda kosmoselaevaga Neptunile (![](b09-neptun.png)) ning siis uuesti kosmoselaevaga sihtkohta. Sama teekonna märgib kosmonaut lühemalt üles sellisel viisil:

![](b09-rocket.png) ![](b09-starship.png) ![](b09-starship.png)

Kosmonaut Toomas on planeedil Neptun (![](b09-neptun.png)) ja soovib jõuda tagasi koju ehk planeedile Maa (![](b09-earth.png)). Kosmoseagentuur saatis talle mõned reisijuhised.

## Küsimus

Milline järgnevatest juhistest **ei vii** Toomast tagasi Maale?

[Raadionupud]

A. ![](b09-starship.png) ![](b09-starship.png) ![](b09-rocket.png)

B. ![](b09-rocket.png) ![](b09-starship.png) ![](b09-rocket.png) ![](b09-starship.png)

C. ![](b09-rocket.png) ![](b09-starship.png) ![](b09-starship.png) ![](b09-starship.png) ![](b09-rocket.png)

D. ![](b09-rocket.png) ![](b09-rocket.png) ![](b09-starship.png)

## Vastus

Õige vastus on: B.

## Kategooriad

- Graafid
- Info mõistmine

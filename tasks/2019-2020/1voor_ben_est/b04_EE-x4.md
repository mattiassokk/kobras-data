# 4. Starship

Ahti Heinla, üks Skype'i loojaid, asutas 2014. aastal uue firma nimega Starship.

## Küsimus

Mida see firma toodab?

[Raadionupud]

A. Robotgiide

B. Robotkullereid

C. Hotelliteenindusroboteid

D. Kosmoselaevu (satelliite)

## Vastus

Õige vastus on: B (robotkullereid).

## Kategooriad

- Varia

# 2. Veebiprotokollid

![](b02-main.png)

Veebi lehitsedes võib kasutada kas HTTP või HTTPS protokolli.

## Küsimus

Mis on nende protokollide peamine erinevus?

[Raadionupud]

A. HTTPS ühendused on kiiremad: S tuleb sõnast "speedy" (inglise keeles "kiire").

B. HTTPS ühendused on paremad videote vaatamiseks: S tuleb sõnad "streaming" (inglise keeles "voogedastus").

C. HTTPS ühendused on turvalised: keegi ei saa pealt kuulata kliendi ja serveri vahel liikuvat salajast infot, näiteks paroole ja pangakaartide andmeid.

D. HTTPS ühendust kasutavad serverid on usaldusväärsed: võib olla kindel, et neis avaldatud info on tõene ja müügiks pakutavad kaubad kvaliteetsed.

## Vastus

Õige vastus on: C (HTTPS ühendused on konfidentsiaalsed).

## Kategooriad

- Turvalisus
- Varia

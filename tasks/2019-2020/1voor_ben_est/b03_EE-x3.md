# 3. Populaarne arvutimäng

![](b03-tetris.jpg)

Kuni selle aasta alguseni hoidis maailma müüduima arvutimängu tiitlit 1984. aastal Aleksei Pažitnovi loodud Tetris.

## Küsimus

Milline mäng tänavu esikoha üle võttis?

[Raadionupud]

A. Minecraft

B. Pokemon

C. Super Mario

D. Angry Birds

## Vastus

Õige vastus on: A (Minecraft).

## Kategooriad

- Varia

# 1. Kobras

"Kobras" on üldhariduskoolide õpilastele mõeldud informaatikaviktoriin, mida korraldatakse paljudes riikides üle kogu maailma. Käesoleval aastal on sel viktoriinil juubel.

## Küsimus 1

Mis aastal sündis "Kobras"?

[Raadionupud]

A. 1999

B. 2004

C. 2009

D. 2014


## Küsimus 2

Kus sündis "Kobras"?

[Raadionupud]

E. USA

F. Poola

G. Leedu

H. Hiina

## Vastus 1

Õige vastus on: B (2004).

## Vastus 2

Õige vastus on: G (Leedu).

## Kategooriad

- Varia

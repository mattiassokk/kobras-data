# 15. Kvartaliaruanne

Elonil on oma firma müügiandmed kuude kaupa:

![](j15-main.png)

## Küsimus 1

Milline oli tema firma kolmanda kvartali summaarne müük?

[Täisarv]

## Küsimus 2

Milline kuu keskmine müük kolmandas kvartalis?

[Täisarv]


## Vastus 1

Õige vastus on: 15000


## Vastus 2

Õige vastus on: 5000

## Kategooriad

- Info mõistmine
- Tabelitöötlus

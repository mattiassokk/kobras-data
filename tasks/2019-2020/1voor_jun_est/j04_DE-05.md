# 4. Lõks

Isesõitev paat sõidab jõe lõunakaldalt põhjakaldale, möödudes jões olevatest saartest. 

Paat navigeerib digitaalse ruutudeks jagatud kaardi abil, liikudes ruudust ruutu. Paat valib liikumiseks ainult neid ruute, mis on täiesti vabad ehk mille ükski osa pole kaetud saarega. 

![](j04-map.png)

Navigeerimine toimub selliste instruktsioonide põhjal:

![](j04-flowchart.png)

Piraadid lisasid jõkke tehissaare, mille abil saab paadi lõksu püüda: kui paat jõuab saare lähedale, võib ta peatuda enne põhjakaldale jõudmist.  

## Küsimus

Milline neist saartest võib paadi lõksu püüda?

[Raadionupud]

A. ![](j04-a.png)
B. ![](j04-b.png)
C. ![](j04-c.png)
D. ![](j04-d.png)

## Vastus

Õige vastus on: A. 

![](j04-solution.png)

## Kategooriad

- Algoritmid

# 7. Varutee

Varuteele pargitud rongi vagunid tuleb enne rongi väljumist järjestada nende numbrite järjekorras nii, et kõige vasakpoolsem vagun oleks number 1.

Varuteel on manööverdamiseks kaks tupikut, kuhu vedur saab vagunid ajutiselt lükata. Iga vaguni tupikusse lükkamisel saab valida, kumba tupikusse ta läheb. Kui kõik vagunid on tupikute vahel jagatud, haagitakse nad rongiks nii, et rongi alguses on kõik ühes ja lõpus kõik teises tupikus olnud vagunid. Kogu seda protseduuri loeme üheks operatsiooniks.

Alltoodud näites on meil 4 vagunit ning nende õigesse järjekorda haakimiseks vajame kaht operatsiooni (seda pole võimalik teha vaid ühe operatsiooniga):

![](s07-1.png)

<p/>

![](s07-2.png)

## Küsimus

Kui vagunid on järjekorras 2 – 8 – 3 – 1 – 5 – 7 – 6 – 4, siis milline on vähim operatsioonide arv, et need ülaltoodud süsteemi järgi haakida järjekorda 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8?

![](s07-q.png)

[Täisarv]

## Vastus

Õige vastus on: 3.

## Kategooriad

- Algoritmid

# 2. Протоколы передачи данных

![](b02-main.png)

Изучая информацию в Интернете, можно использовать протокол HTTP или HTTPS.

## Вопрос

В чем главное отличие между этими протоколами?

[Raadionupud]

A. HTTPS соединения быстрее: буква S указывает на слово "speedy" (с английского языка это переводится как "быстрый").

B. HTTPS соединения лучше подходят для просмотра видео: буква S указывает на слово "streaming" (с английского языка это переводится как "потоковая передача данных").

C. HTTPS соединения безопасны: никто не сможет прослушать зашифрованную информацию, которая движется между клиентом и сервером (например, пароли или данные банковской карточки).

D. Серверы, используемые для HTTPS соединений, надежны: можно быть уверенным в том, что опубликованная на них информация является достоверной, а выставленные на продажу товары качественными.

## Ответ

Правильный ответ: C (HTTPS соединения являются конфиденциальными).

## Категории

- Безопасность
- Разное

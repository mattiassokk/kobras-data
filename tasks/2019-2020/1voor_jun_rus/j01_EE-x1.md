# 1. Бобр

"Бобр" - это викторина по информатике для учащихся общеобразовательных школ, которая проводится во многих странах мира. В этом году викторина отмечает юбилей.

В каком году и в какой стране впервые прошла викторина "Бобр"?


## Вопрос 1

[Raadionupud]

A. 1999

B. 2004

C. 2009

D. 2014


## Вопрос 2

[Raadionupud]

E. США

F. Польша

G. Литва

H. Китай

## Ответ 1

Правильный ответ: B (2004).

## Ответ 2

Правильный ответ: G (Литва).

## Категории

- Разное

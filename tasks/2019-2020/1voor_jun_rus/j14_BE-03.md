# 14. Ивы и тополя

<div style="float: right">
  <p><img src="j14-willow.jpg" /><br />Ива</p>
  <p><img src="j14-poplar.jpg" /><br />Тополь</p>
</div>

Бобр Таня хочет посадить у себя в саду некоторые деревья, чтобы жаркими летними денечками наслаждаться тенью. Она выбирает между ивами и тополями, но хочет знать, что из них растет быстрее. Она ищет информацию в поисковике, где для выполнения сложных запросов можно использовать два вида операций:

| Операция | Значение |
| ----------- | --------------------------------------------------------------- |
| … # … | Условие складывается из расположенных с обеих сторон от знака # частей, и показываются только страницы, которые удовлетворяют обеим частям. |
| [ … ] | Приводит условие в обратное состояние: показывает только те страницы, которые не удовлетворяют расположенному в скобках условию. |

Некоторые примеры:

| Запрос | Значение |
| ------ | ----------------------------------- |
| ива | Страницы об ивах |
| [ ива ] | Страницы, которые не содержат информацию об ивах |
| ива # тополь | Страницы, которые содержат информацию как об ивах, так и о тополях |
| [ [ тополь ] ] | Страницы, которые не являются страницами, которые не касаются тополей (= страницы о тополях) |
| [ ива # тополь ] | Страницы, которые одновременно не содержат информацию как об ивах, так и о тополях |
| ива # [ тополь ] | Страницы, которые содержат информацию об ивах, но не о тополях |

## Вопрос

Какое из следующих запросов найдет такие и только такие страницы, которые содержат информацию об ивах или тополях, или же как об ивах, так и о тополях?

[Raadionupud]

A. [ива] # [тополь]

B. [[ива] # тополь] # [ива # [тополь]]

C. [[ива] # [тополь]]

D. [[ива] # [тополь]] # [ива # тополь]

## Ответ

Правильный ответ: C.

## Категории

- Понимание информации
- Обработка текста

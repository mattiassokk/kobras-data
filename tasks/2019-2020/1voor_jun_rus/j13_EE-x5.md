# 13. Выделенный текст

Маше прислали следующий текст

> Bebras is an international initiative to promote informatics and computational thinking among school students of all ages.
> Participants are usually supervised by teachers who may integrate the Bebras challenge into their teaching activities.
> You can find Bebras tasks from previous years on websites and in the Bebras brochures issued by many countries.
> The second week of November is declared World-Wide Bebras week for solving new tasks.
> Estonia also participates in the Bebras challenge.

Все имеющиеся в тексте слова "Bebras" она хочет выделить определенным образом.

Для этого она использует функцию "найти и заменить", которая имеется в ее текстовом редакторе:

![](j13-main.png)

## Вопрос

Какой результат получила Маша?

A.

> <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> is an international initiative to promote informatics and computational thinking among school students of all ages.
> Participants are usually supervised by teachers who may integrate the <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> challenge into their teaching activities.
> You can find <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> tasks from previous years on websites and in the <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> brochures issued by many countries.
> The second week of November is declared World-Wide <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> week for solving new tasks.
> Estonia also participates in the <span style="font-weight: bold; font-style: italic; color: green">Bebras</span> challenge.

B.

> <span style="font-style: italic; color: green">Bebras</span> is an international initiative to promote informatics and computational thinking among school students of all ages.
> Participants are usually supervised by teachers who may integrate the <span style="font-style: italic; color: green">Bebras</span> challenge into their teaching activities.
> You can find <span style="font-style: italic; color: green">Bebras</span> tasks from previous years on websites and in the <span style="font-style: italic; color: green">Bebras</span> brochures issued by many countries.
> The second week of November is declared World-Wide <span style="font-style: italic; color: green">Bebras</span> week for solving new tasks.
> Estonia also participates in the <span style="font-style: italic; color: green">Bebras</span> challenge.

C.

> <span style="font-weight: bold; color: green">Bebras</span> is an international initiative to promote informatics and computational thinking among school students of all ages.
> Participants are usually supervised by teachers who may integrate the <span style="font-weight: bold; color: green">Bebras</span> challenge into their teaching activities.
> You can find <span style="font-weight: bold; color: green">Bebras</span> tasks from previous years on websites and in the <span style="font-weight: bold; color: green">Bebras</span> brochures issued by many countries.
> The second week of November is declared World-Wide <span style="font-weight: bold; color: green">Bebras</span> week for solving new tasks.
> Estonia also participates in the <span style="font-weight: bold; color: green">Bebras</span> challenge.

D.

> <span style="font-weight: bold; font-style: italic">Bebras</span> is an international initiative to promote informatics and computational thinking among school students of all ages.
> Participants are usually supervised by teachers who may integrate the <span style="font-weight: bold; font-style: italic">Bebras</span> challenge into their teaching activities.
> You can find <span style="font-weight: bold; font-style: italic">Bebras</span> tasks from previous years on websites and in the <span style="font-weight: bold; font-style: italic">Bebras</span> brochures issued by many countries.
> The second week of November is declared World-Wide <span style="font-weight: bold; font-style: italic">Bebras</span> week for solving new tasks.
> Estonia also participates in the <span style="font-weight: bold; font-style: italic">Bebras</span> challenge.

[Raadionupud]

A. результат А

B. результат B

C. результат C

D. результат D


## Ответ

Правильный ответ: A.

## Категории

- Обработка текста

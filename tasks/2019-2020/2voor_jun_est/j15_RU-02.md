# 15. Video pakkimine

Video on kujutiste, nn kaadrite järjend, kus iga kaader erineb veidi
eelmisest. Video salvestamiseks on kõige lihtsam viis salvestada iga
kaadri kõik pikslid. Efektiivsem viis on aga salvestada täielikult 
ainult esimene kaader ja seejärel salvestada ainult need pikslid, mis
muutuvad üleminekul ühelt kaadrilt järgmisele.

![](j15-frames.png)

Ülaltoodud joonisel
liigub tume ruut mõõtmetega 10x10 mööda heledat välja mõõtmetega 20x20
alumisest vasakust nurgast ülemisse paremasse nurka, nihkudes igas
kaadris ühe piksli võrra horisontaalselt ja vertikaalselt. See võtab
11 kaadrit. Kui salvestame selle video lihtsal viisil, siis kulub
selleks (20x20)x11 = 4400 pikslit.

## Küsimus

Mitu pikslit kulub, kui salvestada see video ülalkirjeldatud
efektiivsemal viisil?

[Täisarv]

## Vastus

Õige vastus on: 780.

## Kategooriad

- Varia

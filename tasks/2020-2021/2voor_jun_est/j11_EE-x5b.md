# 11. Kodukohvik

Bruno oli koolivaheajal kaks nädalat emal abiks nende kodukohvikus.

Mõlema esmaspäeva hommikul sai ta ema käest 40€ väärtuses münte ja pani need kohviku kassasse vahetusrahaks.

Iga tööpäeva õhtul märkis ta üles kassas oleva raha kogusumma ja sai sellise graafiku:

![](j11-tekst.png)

Mõlema reede õhtul võttis ta kassast kogu raha välja ja andis emale.

## Küsimus

Mitu eurot oli kohviku suurim päevane sissetulek?

[Täisarv]

## Vastus

Õige vastus on 40€.

## Vastuse selgitus

Kohviku käibest parema ülevaate saamiseks toome graafikul välja eraldi kassajäägi hommikul ja õhtul (seejuures on oluline mitte unustada esmaspäeva hommikul algseisuks sisse pandud 40€):

![](j11-selgitus1.png)

Veel selgem on asi, kui jätame graafikult välja hommikuse jäägi ja keskendume päeval lisandunud summale:

![](j11-selgitus2.png)

Nüüd on ilmne, et kõige suurema summa, 40€, teenis kohvik teise nädala esmaspäeval.

## See on informaatika

See ülesanne illustreerib, kuidas andmete esitusviis võib nende põhjal mõnele küsimusele vastamise teha kas lihtsamaks või keerulisemaks. Kui graafiku aluseks olevad andmed on arvutustabelis või andmebaasis, saab esituse parandamiseks vajaliku teisenduse lasta ära teha arvutil. Mida rohkem on andmeid, seda suurem on sellisest automaatsest parandamisest saadav kasu.

## Kategooriad

- Info mõistmine

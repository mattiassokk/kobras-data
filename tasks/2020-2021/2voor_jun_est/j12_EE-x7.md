# 12. Sõnade otsing tekstis

Sõnade otsimiseks tekstist võib kasutada regulaaravaldisi (_regular expressions_). Need võivad sisaldada metamärke, mis määravad keerukamaid otsingureegleid.

![](j12-tekst.png)

Mõned metamärkide näited:

* `[:space:]` – tühikud, näiteks `b[:space:]e` leiab teksti `b e`,

* `?` – erisümbol, mis määrab, et selle ees olev märk võib tekstis esineda või puududa, näiteks `be?b` leiab nii `beb` kui ka `bb`.

Järgnevas tekstis esineb sõna "bebras" viis korda (paksus kirjas esile toodud), iga kord erinevalt kirjutatuna:

> There are two extant species of **b-e-b-r-a-s**; the North American **b e b r a s** and the Eurasian **bebras**. The American **\_bebras\_** was coined by Carl Linnaeus in 1758 who also classified the species name fiber. However, the two **bebas** were not conclusively shown to be separate species until the 1970s with chromosomal evidence.

## Küsimus

Millise regulaaravaldise peaks otsinguaknasse sisestama, et leida tekstist sõna "bebras" kõik viis varianti?

[Raadionupud]

A. `_?b-?e-?b-?r?-?a-?s_?`

B. `_?b-?[:space:]?e[:space:]?-?b[:space:]?-?r?[:space:]?-?a[:space:]?-?s_?`

C. `_?b[:space:]?e[:space:]?b[:space:]?r?[:space:]?a[:space:]?s_?`

D. `_?b-?[:space:]?e[:space:]?-?b[:space:]?-?[:space:]?-?a[:space:]?-?s_?`

## Vastus

Õige vastus on B. `_?b-?[:space:]?e[:space:]?-?b[:space:]?-?r?[:space:]?-?a[:space:]?-?s_?`.

## Vastuse selgitus

Vaatleme iga avaldist eraldi.

Avaldis A ei leia varianti "b e b r a s", kus tähtede vahel on tühikud:

![](j12-Aselgitus.png)

Avaldis B leiab kõik viis varianti:

![](j12-Bselgitus.png)

Avaldis C ei leia varianti, kus tähtede vahel on sidekriipsud:

![](j12-Cselgitus.png)

Avaldis D leiab vaid selle variandi, kus täht "r" puudub:

![](j12-Dselgitus.png)

## See on informaatika

Regulaaravaldisi kasutatakse programmeerimises väga sageli, näiteks osaliselt kokku langevate tekstide, märgihulkade, sõnade otsimisel, tekstiridade valideerimisel, samuti sümbolite või sõnade asendamiel. Neid saab kasutada ka andmete valideermisel, näiteks kui paroolile seatakse minimaalse tähemärkide arvu või suur- ja väiketähtede, numbrite või erimärkide olemasolu nõuded.

## Kategooriad

- Tekstitöötlus
- Info mõistmine

# 15. Веб-страница школы

Митя хочет опубликовать на сайте школы диаграммы результатов опроса учеников. Графический редактор позволяет сохранять изображения в различных форматах.

## Вопрос

Какой из следующих форматов следует использовать для диаграмм?

[Raadionupud]

A. Windows Bitmap (*.bmp)

B. Encapsulated PostScript (*.eps)

C. Joint Picture Experts Group image format (*.jpg)

D. Portable Network Graphics (*.png)

## Ответ

Правильный ответ: D (PNG).

## Объяснение ответа

Для публикации диаграммы в Интернете из предложенных вариантов лучше всего подходит формат PNG. Это формат растровой графики, где каждая картинка представлена в виде маленьких разноцветных квадратиков. PNG формат основывается на сжатии без потерь. Это означает, что при восстановлении получим точно такие же первоначальные данные. Данный формат очень подходит для представления "искусственных" рисунков, состоящих из больших участков с однородным цветом или повторяющимся узором, или имеющих контрастные линии. Именно так и выглядят диаграммы. Также PNG формат поддерживается всеми современными браузерами, которые доступны в операционных системах для настольных компьютеров, планшетов и телефонов.

JPEG также является растровым форматом, но сжатие происходит с потерями. JPEG был создан для того, чтобы представлять "естественные" рисунки, как, например, фотографии, где много различных тонов и плавных переходов между ними. Если для сохранения рисунков с контрастными чёткими линиями использовать этот формат, то применяемый алгоритм сжатия приведёт к довольно большим искажениям (это видно на приведённом ниже сравнении). По этой причине этот формат не очень подходит для "искусственных" рисунков (графиков, чертежей, скриншотов).

BMP является растровым форматом без потерь, как и PNG, но основывается на более простых алгоритмах сжатия и поэтому сохранённые в этом формате файлы имеют больший размер, чем при сохранении точно такого же рисунка в формате PNG. Например, размер файла с одной примерно 600х350 пиксельной столбчатой диаграммой в формате PNG будет около 8 KB, а в формате BMP более 600 KB. Стоит учитывать, что BMP является форматом, специфичным для Windows, и поэтому не обязательно будет поддержан в браузерах других операционных систем.

EPS отличается от всех вышеописанных форматов, потому что это формат векторной графики. Это означает, что имеющиеся в EPS-файле рисунки описываются не с помощью мозаики из пикселей квадратной формы, а с помощью объектов, состоящих из геометрических фигур (линий, прямоугольников, кругов и др.). Благодаря этому рисунки в векторном формате можно увеличивать и уменьшать без изменения качества. В приведённой ниже сравнительной таблице видно, что при увеличении представленных в формате PNG и BMP рисунков они становятся квадратоподобными, а в случае JPEG формата рисунок становится нечётким. В то же время рисунок в векторном формате остаётся чётким. Недостатком EPS формата является то, что он исторически был очень сложным форматом, применявшимся в типографии, и сейчас не поддерживается браузерами (для просмотра рисунков в этом формате необходима отдельная программа).

Scalable Vector Graphics (*.svg) является современным форматом векторной графики и предназначен для использования в Интернете. У этого формата в общем схожие с EPS форматом позитивные свойства и, если программное обеспечения для создания диаграмм поддерживает этот формат, предпочтительнее использовать именно его. Однако стоит иметь ввиду, что это довольно новый и находящийся ещё в процессе развития файловый формат, поэтому в случае некоторых сложных рисунков может получиться так, что все браузеры не смогут их достоверно отобразить.

PNG/BMP (увеличение) | JPEG (увеличение) | SVG/EPS (увеличение)
---|---|---
![](j15png-zoom.png) | ![](j15jpg-zoom.png) | ![](j15svg-zoom.png)

## Это информатика!

Если одни и те же данные можно представить в разных форматах, как это, например, и есть в случае графики, то при выборе правильного формата надо знать его позитивные и негативные стороны.

## Категории

- Разное

# 15. Kooli veebileht

Mati tahab avaldada kooli veebilehel õpilaste küsitluse tulemuste tulpdiagrammid. Kujundustarkvara võimaldab salvestada pilte mitmes vormingus.

## Küsimus

Millist allolevatest vormingutest peaks diagrammide jaoks kasutama?

[Raadionupud]

A. Windows Bitmap (*.bmp)
B. Encapsulated PostScript (*.eps)
C. Joint Picture Experts Group image format (*.jpg)
D. Portable Network Graphics (*.png)

## Vastus

Õige vastus on: D (PNG).

## Vastuse selgitus

Pakutud variantidest on diagrammide veebis esitamiseks sobivaim vorming PNG. See on rastergraafika vorming, mis tähendab, et iga pilt esitatakse väikeste erivärviliste ruudukestena. PNG vorming kasutab kadudeta pakkimist, mis tähendab, et lahtipakkimisel saame tagasi täpselt esialgsed andmed. See vorming on sobiv suurte ühtlast värvi või korduva mustriga pindade ja kontrastsete joontega "kunstlike" jooniste esitamiseks, nagu diagrammid ongi. Ka on PNG vorming toetatud kõigis tänapäevastes veebilehitsejates nii arvuti- kui ka tahvli- ja telefoniopsüsteemides.

JPEG on samuti rastervorming, kuid kasutab kadudega pakkimist. JPEG on loodud, et esitada "naturaalseid" pilte, näiteks fotosid, kus on palju erinevaid toone ja sujuvaid üleminekuid nende vahel. Kontrastsete teravate joontega piltidel põhjustab selles vormingus kasutatav pakkimisalgoritm üsna suuri moonutusi, nagu näha allolevas võrdluses. Sellepärast ei ole see vorming kuigi sobiv "kunstlike" piltide (graafikud, joonised, erkaanitõmmised) jaoks.

BMP on kadudeta rastervoming, nagu ka PNG, kuid kasutab palju lihtsamat pakkimisalgoritmi, mistõttu on selles vormingus failid palju suuremad kui saaksime sama pilti PNG vormingus salvestades. Näiteks ühe umbes 600x350 pikseli suuruse tulpdiagrammi faili maht PNG vormingus on umbes 8 KB, aga BMP vormingus üle 600 KB. Lisaks on BMP Windowsi-spetsiifiline vorming ja ei tarvitse kõigi opsüsteemide veebilehitsejates toetatud olla.

EPS on kõigist eelnevatest erinevat vektorgraafika vorming. See tähendab, et EPS-failis kirjeldatakse pilti mitte ruudukujulistest pikselitest mosaiigina, vaid joontest, ristkülikutest, ringidest jmt geomeetrilistest objektidest koosnevana. Tänu sellele saab vektorvormingus pilte suurendada ja vähendada ilma, et nende kvaliteet sellest muutuks. Allolevas võrdlustabelis on näha, et PNG ja BMP vormingus pildid muutuvad suurenamisel ruuduliseks ning JPEG vormingus pilt läheb uduseks, aga vektorvormingus pilt jääb endiselt teravaks. EPS vormingu puudus on, et see on ajalooliselt trükitööstuses kasutusel olnud väga keeruline vorming ja ei ole veebilehitsejates toetatud (selles vormingus piltide vaatamiseks on vaja lisaprogrammi).

Tänapäevane ja ka veebis kasutamiseks mõeldud vektorgraafika vorming on Scalable Vector Graphics (*.svg). Sellel on üldiselt EPS vorminguga sarnased positiivsed omadused ja kui diagrammide koostamise tarkvara seda vormingut toetab, on see enamasti parim valik. Siiski tasub silmas pidada, et see on üsna uus ja veel arenev failivorming ning mõnede keerulisemate jooniste puhul võib juhtuda, et kõik veebilehitsejad ei oska neid päris õigesti näidata.

PNG/BMP (suurendus) | JPEG (suurendus) | SVG/EPS (suurendus)
---|---|---
![](j15png-zoom.png) | ![](j15jpg-zoom.png) | ![](j15svg-zoom.png)

## See on informaatika

Kui samu andmeid on võimalik esitada mitmes erinevas vormingus, nagu see näiteks graafika puhul on, siis peab õige vormingu valimiseks teadma iga vormingu tugevaid ja nõrku külgi.

## Kategooriad

- Varia

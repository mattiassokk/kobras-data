# 11. Veergude tähised

Tabelarvutussüsteemis tähistatakse esimest 26 veergu ladina (täpsemalt küll inglise) tähestiku tähtedega:

![](j11-a.png)

Järgmist 26 veergu tähistatakse 2-täheliste kombinatsioonidega AA kuni AZ:

![](j11-aa.png)

Nende järel järgmist 26 kombinatsioonidega BA kuni BZ:

![](j11-ba.png)

Nii jätkub see, kuni kahetähelised kombinatsioonid ZZ järel otsa saavad. Siis alustatakse kolmetähelistega AAA kuni AAZ:

![](j11-aaa.png)

## Küsimus

Milline on 2020. veeru tähis?

[Raadionupud]

A. BXR

B. BYR

C. CXR

D. CYR

## Vastus

Õige vastus on: B (BYR).

## Vastuse selgitus

Üks võimalus vastuse leidmiseks on avada tabelarvutusprogramm, panna kursor töölehe esimesse veergu, vajutada 2019 korda klahvi "nool paremale" ja vaadata siis vastava veeru tähis. See on muidugi tüütu ja lisaks on üsna suur risk, et vahepeal võib loendamine segi minna.

Teine võimalus on arvutada igale pakutud variandile vastav veerunumber. Selleks paneme tähele, et

- ühetäheliste tähistega on 26 veergu, ehk veerud 1 kuni 26;

- kahetähelistes tähistes on võimalikke esitähti 26 ja iga esitähe järele teiseks täheks ka 26 võimalikku varianti; seega on kahetähelisi tähiseid kokku 26*26=676; seega on kahetäheliste tähistega veerud 27 kuni 702;

- A-tähega algavates kolmetähelistes tähistes on teise ja kolmanda tähena kõikvõimalikud kahetähelised paarid, mida eelmise punkti põhjal on 676; seega on A-tähega algavate kolmetäheliste tähistega veerud 703 kuni 1378;

- B-tähega algavate hulgas on BXR ees veel 23*26+17=615 veergu: kõigepealt sellised, kus teine täht on A kuni W (23 võimalust) ja iga teise tähe jaoks käivad kolmandana läbi A kuni Z (26 võimalust), ning siis veel sellised, mille alguses on BX ja kolmanda tähena esinevad A kuni Q (17 võimalust); seega on BXR ees veel veerud 1379 kuni 1993 ja BXR enda number on 1994;

- eelmise punktiga sarnaselt aruvutades saame, et BYR ees on veel 24*26+17=641 B-tähega algavat kolmetähelist tähist ja seega BYR number ongi otsitav 2020;

- ülesande lahendamiseks pole see enam vajalik, aga samamoodi saame leida ka, et CXR veeru number on 2670 ja CYR oma 2696.

## See on informaatika!

Arvutisüsteemides on alatasa vaja mitmesuguste objektide tähiseid erinevate esituste vahel teisendada. Kõike tavalisem näide on arvude teisendamine inimestele harjumuspärase kümnendsüsteemi ja arvutis sisemiselt kasutatava kahendsüsteemi vahel.

Näiteks kui me kirjutame 10-süsteemis 345, siis tähendab see avaldist

    3*10^2 + 4*10^1 + 5*10^0 = 300+40+5 = 345.

Sama arvu esitus 2-süsteemis oleks 101011001, mis tähendab avaldist

    1*2^8 + 0*2^7 + 1*2^6 + 0*2^5 + 1*2^4 + 1*2^3 + 0*2^3 + 0*2^1 + 1*2^0 = 256+64+16+8+1 = 345.

Tabeliveergude tähistes on kasutusel 26 "numbrit" A kuni Z, ja nii on meil tegemist 26-süsteemiga, kus iga järgmise järgu kaal on eelmise omast 26 korda suurem. Aga erinevalt klassikalisest positsioonilisest süsteemist, kus 26 numbri väärtused peaks olema 0 kuni 25, on tabeliveergude tähistamisel nende väärtusteks 1 kuni 26.

Seda tausta teades saaksime vastuse selgitusena toodud sammukaupa arutelu asemel arvutada otse (kasutades tähtede väärtuste spikrina esimest ülesande tekstis olevat joonist):

    BXR = 2*26^2 + 24*26^1 + 18*26^0 = 1352 + 624 + 18 = 1994,
    BYR = 2*26^2 + 25*26^1 + 18*26^0 = 1352 + 650 + 18 = 2020,
    CXR = 3*26^2 + 24*26^1 + 18*26^0 = 2028 + 624 + 18 = 2670,
    CYR = 3*26^2 + 25*26^1 + 18*26^0 = 2028 + 650 + 18 = 2696.

## Kategooriad

- Kodeerimine
- Info mõistmine

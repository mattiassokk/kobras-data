# 13. Sisukord

Bioloogiatunniks teeb Kaarel referaati lindudest ja loomadest. Referaadi sisukorda ei tee ta käsitsi, vaid laseb selle genereerida tekstitöötlusprogrammil.

Selleks märgib ta imetajate ja lindude peatükkide pealkirjad laadiga Pealkiri 1 (ingl *Heading 1*). Töös vaadeldud imetajate (rebane, hunt, jänes, kobras) jaotiste pealkirjad märgib ta laadiga Pealkiri 2 (ingl *Heading 2*). Kobraste jaotises on kaks alamjaotist, mis on ka sisukorras kajastatud.

![](b13-body.png)

## Küsimus

Täna lisas Kaarel referaati jäneste jaotisse alamjaotised valgejänesest, halljänesest ja ameerika jänesest.

Kuidas peaks nende pealkirjad vormistama, et nad sisukorras õigesti kajastuksid?

[Raadionupud]

A. Need peaks märkima laadiga Pealkiri 1

B. Need peaks märkima laadiga Pealkiri 2

C. Need peaks märkima laadiga Pealkiri 3

D. Need peaks märkima laadiga Pealkiri 4

## Vastus

Õige vastus on: C.

## Vastuse selgitus

Number pealkirja laadis vastab üksuse tasemele dokumendi struktuuris. Peatükid kui kõrgeima taseme üksused on number 1, nende sees olevad jaotised number 2 ja nende sees olevad alamjaotised number 3. Kui alamjaotiste sees oleks omakorda pealkirjastatud lõike, oleks nende taseme number 4.

## See on informaatika!

Arvutid on tänapäeval veel üsna nõrgad vabas vormis andmete töötlemisel ja soovitud tulemuse saamiseks peavad kasutajad sageli ise lisama oma andmetele märkusi selle kohta, kuidas neid töötlema peaks. Kuigi see võib alguses tunduda tarbetu lisatööna, on see pikemas plaanis aja ja vaeva kokkuhoid.

Näiteks kui pealkirjadele on laadid märgitud, saab nende kõigi kujundust korraga muuta, andes käsu ühe või teise laadiga pealkirju erinevalt vormistada. Kui pealkirjadel laadimärke küljes ei oleks, peaks dokumendi autor iga pealkirja kujundust eraldi muutma. Esiteks on see rohkem tööd ja teiseks kaasneb sellega ka risk, et mõni pealkiri jääb vahele ja pärast on dokument ebaühtlase stiiliga.

## Kategooriad

- Info mõistmine
- Tekstitöötlus

# 12. Karlsson

Astridi väikevend luges suvel raamatut "Karlsson katuselt" ja kirjutas sellest emakeeletunniks kokkuvõtte.

Viimasel hetkel märkas Astrid, et väikevend on paljudes kohtades kirjutanud "Karlsson" asemel "Karlson". Seejuures esineb Karlssoni nimi väikevenna töös mitmes erinevas käändes ning on mõnes kohas kirjutatud õigesti ja mõnes kohas valesti.

## Küsimus

Kuidas saab Astrid aidata väikevennal vea kiiresti ja vähese vaevaga parandada?

[Raadionupud]

A. Laseb tekstitöötlusprogrammil asendada teksti "so" kõik esinemised tekstiga "sso"

B. Laseb tekstitöötlusprogrammil asendada teksti "Karlson" kõik esinemised tekstiga "Karlsson"

C. Seda ei saa teha funktsiooni "otsi ja asenda" ühe kasutusega, tuleb otsida ja asendada iga käänet eraldi

D. Eesti keele käänete tõttu ei saa seda üldse "otsi ja asenda" funktsiooniga parandada, tuleb kogu tekst hoolikalt üle lugeda

## Vastus

Õige vastus on: B.

## Vastuse selgitus

B on õige vastus, sest sõna "Karlson" käänamisel selle tüvi ei muutu. Teksti "Karlson" asendamisel tekstiga "Karlsson" asendatakse õigesti ka "Karlsoni" → "Karlssoni", "Karlsonit" → "Karlssonit" ja nii edasi ka kõik ülejäänud käänded.

A ei ole õige, sest nii tehes muutuks juba õigesti kirjutatud "Karlsson" vigaseks variandiks "Karlssson", samuti näiteks "Majasokk" → "Majassokk".

C oleks õige vastus, kui oleks vaja teha parandus mingis sõnas, mille tüvi käänamisel muutub, näiteks kui sõnas "õde" erinevates käänetes ("õde", "õe", "õele", ...) oleks "õ" asemel kogemata "ö" kirjutatud. Karlssoni nimi käänamisel nii palju ei muutu ja sellepärast saab seda lihtsamalt parandada.

D oleks õige vastus, kui meie valesti kirjutatud sõna võiks mõnes teises kontekstis olla ka õige sõna või selle osa. Näiteks kui tekstis esineb ka sõna "öeldis", siis lihtne asendus "öe" → "õe" rikuks selle ära. Paljudes tekstitöötlusprogrammides on selliste olukordade lahendamiseks "otsi ja asenda" fuktsioonil võimalik määrata, et asendatakse ainult terveid sõnu. Aga siiski on võimalik, et ka sellest lahendusest ei piisa ja tulebki kõik kahtlased kohad ükshaaval üle vaadata.

## See on informaatika

Arvutid on küll kiired ja täpsed käsutäitjad, aga käsu andmisel peab alati meeles pidama, et arvuti teeb täpselt seda, mida kästud, isegi kui see ei ole see, mida kasutaja tahtis. Suur osa informaatikast ja eriti programmeerimisest seisnebki just selles, et välja mõelda, kuidas oma soove arvuti jaoks piisavalt täpselt sõnastada.

## Kategooriad

- Tekstitöötlus

# 11. Linnad

Kaarel teeb geograafiatunniks referaati ja tahab sinna lisada joonise Eesti linnade rahvaarvude kohta. Ta kaalub joonisele mitut kujundust, aga tegi ühe variandi juures vea.

## Küsimus

Milline järgmistest joonistest **EI ESITA** ülejäänutega samu andmeid?

[Raadionupud]

A. ![](b11-a.png)
B. ![](b11-b.png)

C. ![](b11-c.png)
D. ![](b11-d.png)

## Vastus

Õige vastus on: D.

## Vastuse selgitus

Kõigil teistel joonistel on Tartu rahvaarv Narva omast suurem (mis on ka faktiliselt õige), aga joonisel D on see vastupidi.

## See on informaatika

Sama informatsiooni on sageli võimalik kujutada mitmel erineval moel. Aga isegi kui andmetes endis vigu ei ole, on mõned esitused paremini aru saadavad kui teised. Näiteks rahvaarvu järgi järjestatud jooniselt A on kohe näha, et Narva on Eesti suuruselt kolmas linn, samas nimede järgi järjestatud jooniselt B on selle väljalugemine märksa tülikam.

Muidugi, kui joonise eesmärk ongi näidata mingi suuruse muutumist ajas, nagu allpool on tehtud Tartu rahvaarvuga erinevatel aastatel, on just tulpade järjestamine nende kõrguse järgi vähem ülevaatlik.

![](b11-x1.png) ![](b11-x2.png)

Näidetes kasutatud andmed pärinevad [Wikipediast](https://et.wikipedia.org/wiki/Eesti_linnad#Eesti_linnade_loend).


## Kategooriad

- Info mõistmine
- Tabelitöötlus

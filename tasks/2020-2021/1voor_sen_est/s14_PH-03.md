# 14. Kingitus

Kopraatias kasutatakse arvude märkimiseks ainult nulle ja ühtesid. Kaks Kopraatia last otsisid oma emale sünnipäevakingitust. Selleks oli neil kahe peale raha 11000101 kobrut. Lapsed leidsid kolm võimalikku kingitust: salli hinnaga 1110110 kobrut, kaelakee hinnaga 11001000 kobrut ja kella hinnaga 10011101 kobrut.

![](s14-body.png)

## Küsimus

Milline on kõige kallim kingitus, mille lapsed saavad olemasoleva raha eest osta?

[Raadionupud]

A. Sall

B. Kaelakee

C. Kell

## Vastus

Õige vastus on: C (kell).

## Vastuse selgitus

Antud kingitustest maksab kaelakee rohkem, kui on lastel raha: 11001000 > 11000101. Ülejäänud kahe asja jaoks on neil piisavalt raha: 1110110 < 11000101 ja 10011101 < 11000101. Seega saavad nad osta kas salli või kella. Neist kahest on kallim kell: 1110110 < 10011101.

## See on informaatika!

Ülesanne on seotud kahendsüsteemiga. Kahendsüsteem kasutab erinevalt meie jaoks harjumuspärasest kümnendsüsteemist arvude märkimiseks vaid kaht numbrit: 0 ja 1. Kasutusel on ka teised arvusüsteemid, näiteks kaheksandsüsteem (numbrid 0-7) ja kuueteistkümnendsüsteem (numbrid 0-9 ning neile lisaks tähed A, B, C, D, E ja F).

Arvutid kasutavad kahendsüsteemi selle lihtsuse tõttu. Me võime mõelda numbrist 0 kui signaali puudumisest (lüliti on olekus "väljas") ja numbrist 1 kui signaali olemasolust (lüliti on olekus "sees"). Sel viisil saame lülitite erinevate olekute järgnevusena esitada suvalisi arve. Ka muud andmed (näiteks tähed) teisendatakse kahendarvudeks ja nii võib jada 01101000 tähistada kas arvu 104 või tähte "h".

## Kategooriad

- Kodeerimine

# 9. Kolm töölist

Anna, Bruno ja Carmen on tehase ainukesed kolm töölist. Nad lähevad tööle vastavalt kindlaksmääratud nõuetele. Igal esmaspäeval peab tööle minema täpselt kaks töölist, teistel päevadel aga vastavalt järgmistele reeglitele: 

1. Kui Anna läheb tööle, siis peab Bruno jääma järgmisel päeval koju.
2. Kui Bruno läheb tööle, siis läheb tööle ka Carmen, kuid Carmen jääb sel juhul järgmisel päeval koju.
3. Kui Carmen jääb ühel päeval koju, siis jääb Anna koju järgmisel päeval.

## Küsimus

Tootmise optimeerimiseks võib erinevatel päevadel olla vaja erinevat arvu töölisi. Milline järgnevatest nädalaplaanidest on eelkirjeldatud reeglite järgimisel võimalik?


A. 

| | Esmaspäev | Teisipäev | Kolmapäev | Neljapäev | Reede |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Töölisi | 2 | 3 | 1 | 1 | 2 |

B. 

| | Esmaspäev | Teisipäev | Kolmapäev | Neljapäev | Reede |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Töölisi | 2 | 1 | 3 | 2 | 1 |

C. 

| | Esmaspäev | Teisipäev | Kolmapäev | Neljapäev | Reede |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Töölisi | 2 | 1 | 3 | 1 | 0 |

D. 

| | Esmaspäev | Teisipäev | Kolmapäev | Neljapäev | Reede |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Töölisi | 2 | 1 | 0 | 3 | 2 |

[Raadionupud]

A. Nädalaplaan A

B. Nädalaplaan B

C. Nädalaplaan C

D. Nädalaplaan D


## Vastus

Õige vastus on: C.

## Vastuse selgitus

Tähistame töölisi lühiduse mõttes tähtedega: olgu Anna A, Bruno B ja Carmen C.

Alustame sellest, et teeme selgeks, millised kaks töölist saavad töötada esmaspäeval. Paarid võiksid olla A-B, A-C ja B-C. Võime välistada paari A-B, kuna 2. reegli kohaselt läheb koos B-ga tööle ka C, aga esmaspäeval ei tohtinud tööl olla kolme inimest. Seega alustame kahe variandiga: A-C ja B-C.

Variant 1:

| | Esmaspäev | Teisipäev | Kolmapäev | Neljapäev | Reede |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Tööl | A-C | ? | ? | ? | ? |
| Kodus | B | B | ? | ? | ? |

Variant 2:

| | Esmaspäev | Teisipäev | Kolmapäev | Neljapäev | Reede |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Tööl | B-C | ? | ? | ? | ? |
| Kodus | A | C | A | ? | ? |

Esimese variandi puhul ei tohi vastavalt 1. reeglile teisipäeval töötada B, 2. variandi puhul ei tohi vastavalt 2. reeglile teisipäeval töötada C. Nii saame vastusevariantidest välistada A. 

Teise variandi puhul näeme ka reeglit 3: A jääb koju peale C kojujäämist. Seega kui on mingi päev, kus keegi tööle ei lähe, siis sellele järgmisel päeval ei saa kõik 3 uuesti tööl olla, kuna A peab jääma koju. Nii välistame vastusevariandi D. 

Ajagraafik B vastab reeglitele ainult kuni kolmapäevani: võime alustada variandist 1 (esmaspäeval töötavad A ja C), C võib töötada teisipäeval ja kõik võivad tööle minna kolmapäeval: 

| | Esmaspäev | Teisipäev | Kolmapäev | Neljapäev | Reede |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Tööl | A-C | C | A-B-C | ? | ? |
| Kodus | B | A-B | - | B-C | A |

Kui aga kõik töötavad kolmapäeval, siis peavad nii B kui C jääma neljapäeval koju (vastavalt reeglid 1 ja 2), ainult A saab sel juhul neljapäeval töötada. Seega peame välistama ka vastusevariandi B. 

Ainuke sobiv variant on C, järgnev tabel vastab kõigile reeglitele: 

| | Esmaspäev | Teisipäev | Kolmapäev | Neljapäev | Reede |
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Tööl | A-C | C | A-B-C | A | - |
| Kodus | B | A-B | - | B-C | A-B-C |

## See on informaatika!

Selle ülesande puhul võib rakendada erinevaid strateegiaid. Toodud seletuses lõime me hüpoteesi (näiteks "A ja B töötavad esmaspäeval") ja kontrollime sammhaaval, kas see on võimalik toodud reeglite põhjal (kas see rahuldab meie tingimusi). Kui jah, läheme ühe sammu edasi ja kontrollime tulemust jne. Kui leiame mingil momendil vastuolu, astume sammu tagasi, loome uue hüpoteesi ja proovime seda samal moel kontrollida. 

Inimese jaoks on raske pidevalt jälgida, et hüpoteeside tegemine oleks süstemaatiline ja et võimalik lahendus kindlasti leitakse. Heaks näiteks on sudoku lahendamine: mida raskem sudoku, seda rohkem peame looma (ja seda tõenäolisemalt ka kummutama) hüpoteese. Arvutiprogrammid suudavad olla seevastu väga süstemaatilised ning seetõttu on loodud palju algoritme analoogsete ülesannete lahendamiseks.

## Kategooriad

- Loogika
- Info mõistmine

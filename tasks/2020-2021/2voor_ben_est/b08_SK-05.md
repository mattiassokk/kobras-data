# 8. Kus nad elavad?

Koprad Alar, Siim ja Erik seisavad kollasel ruudul oma kooli ees, näoga
näidatud suunas.

Teel koju teeb igaüks neist teatavates kohtades pöörded.

Alar teeb kaks vasakpööret.

Siim teeb ühe vasakpöörde ja selle järel ühe parempöörde.

Erik teeb ühe vasakpöörde.

Iga kobras elab erinevas majas. Koprad liiguvad ainult horisontaalselt
või vertikaalselt, aga mitte diagonaalselt. Koprad ei saa astuda
hallidele ruutudele.

![](b08-tekst.png)

## Küsimus

Millises majas iga kobras elab?

[Raadionupud]

A. Alar majas 4, Siim majas 2, Erik majas 5

B. Alar majas 3, Siim majas 2, Erik majas 1

C. Alar majas 4, Siim majas 2, Erik majas 1

D. Alar majas 3, Siim majas 2, Erik majas 4

## Vastus

Õige vastus on D.

## Vastuse selgitus

Vastused A ja C on valed, sest majani 4 jõudmiseks tuleb kas pöörata üks
kord vasakule või pöörata üks kord vasakule ja teine kord paremale või
pöörata rohkem kui kaks korda. Seepärast ei saa Alar elada majas 4.

Vastus B on vale, sest majani 1 jõudmiseks tuleb pöörata kaks korda
vasakule ja seejärel üks kord paremale. Seetõttu ei saa Erik elada majas
1.

Õige vastuseni jõudmiseks võib panna tähele, et maja 3 on ainuke maja,
kuhu Alar jõuda saab. Siim saab jõuda majani 2 või 4, aga ta ei saa
elada samas majas nagu Erik. Erik saab jõuda ainult majani 4.

## See on informaatika

Kujutada ette mingi tegevuse mõju pole tavaliselt liiga raske: kui on
teada, et kobras on liikunud ruudustikul vasakule, vasakule ja paremale,
siis on lihtne järgida tema liikumisi, eeldusel, et "vasakule" ja
"paremale" on selgelt defineeritud tegevused, näiteks liikumine ühe
ruudu võrra ruudustikus kopra hetkesuunaga määratud suunas.
Ülesandes aga esitatakse raskem probleem, mis on informaatikas siiski
üsna tavaline: antud on mõned tulemused või vihjed ning vaja on kindlaks teha,
mis juhtus minevikus. Mitmel juhul on meil teada andmed (või isegi
palju andmeid) ning me püüame aru saada, mis liiki protsessis need tekkinud on.
Üldiselt võib samade andmetega kokku sobida mitu protsessi: näiteks
on teada, et Siim pööras vasakule ja paremale, aga selle tingimusega on
kooskõlas kaks erinevat maja (2 ja 4).

### Veebilehed

* <https://en.wikipedia.org/wiki/Grid_plan>

## Kategooriad

- Info mõistmine

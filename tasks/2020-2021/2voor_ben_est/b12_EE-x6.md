# 12. Sõnade otsing tekstis

Järgnevas tekstis esineb sõna "beaver" viis korda:

![](b12-tekst.png)

Sõnade otsimisel ja asendamisel tekstitöötlusprogrammis võib lisaks sõnadele endile arvestada ka nende vormingut.

## Küsimus

Mitu sõna "beaver" esinemist leitakse järgmise käsuga?

![](b12-küsimus.png)

[Raadionupud]

A. 0

B. 3

C. 4

D. 5

## Vastus

Õige vastus on C. 4.

## Vastuse selgitus

Kuna otsingutingimuseks on valitud lisaks sõnale "beaver" ka vorming "Italic" ehk kaldkiri, leitakse 4 esinemist:

![](b12-selgitus.png)

## See on informaatika

Tekstitöötlusprogrammi võimaluste tundmine ja nende kasutamine oma töö hõlbustamiseks on loomulikult informaatika!

## Kategooriad

- Tekstitöötlus

from sys import argv

scale = 30
margin = 10
colors = ['white', 'black']


rows = int(argv[1])
cols = int(argv[2])
counts = [int(c) for c in argv[3].split(', ')]
doreset = argv[4] == 'yes'

print(f'<svg width="{cols * scale + 2 * margin}" height="{rows * scale + 2 * margin}">')

x, y = 0, 0
color = 0
for c in counts:
	for i in range(c):
		assert x < cols
		assert y < rows
		print(f'  <rect x="{x * scale + margin}" y="{y * scale + margin}" width="{scale}" height="{scale}" stroke="gray" stroke-width="1" fill="{colors[color]}" />')
		x += 1
	color = 1 - color
	if x == cols:
		y += 1
		x = 0
		if doreset:
			color = 0
assert x == 0
assert y == rows

print(f'</svg>')

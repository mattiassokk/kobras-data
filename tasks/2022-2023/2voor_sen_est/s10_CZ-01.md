# 10. Pusle

Meil on kuus pusletükki, kõik täpselt sama kujuga:

![](s10-body.png)

Kõigi nende tükkide väljaulatuvad osad sobivad ideaalselt teiste tükkide sisselõigetega.

Järgime algoritmi:

1. Võta ükskõik milline tükk.
2. Võta veel üks tükk ja ühenda see eelmisega kokku.
3. Korda sammu 2, kuni sul on kõigist kuuest tükist kujund.
4. Värvi see nii, et detailide vahelised piirid muutuksid nähtamatuks.

## Küsimus

Milline järgmistest kujunditest **ei saa olla** loodud seda algoritmi järgides?

[Raadionupud]

A. ![](s10-option-a.png)

B. ![](s10-option-b.png)

C. ![](s10-option-c.png)

D. ![](s10-option-d.png)

## Vastus

Õige vastus on: D.

## Vastuse selgitus

![](s10-solution.png)

## See on informaatika!

Algoritmi loomisel peaksime alati huvi tundma, millises olekus see lõpeb. Kui me algoritmist tõesti aru saame, suudame sageli ennustada lõppseisundit ilma kõiki samme läbimata. Kui leiame mõne märgi või atribuudi, mis muutub algoritmi täitmisel erinevates lahendustes erinevalt, saame seda kasutada õige lahenduse leidmiseks.

## Kategooriad

- Algoritmid
- Geomeetria

# 13. Kobraste andmebaas

Koprakülas elab tosin perekonda. Arvutiteadlane Jeffrey koostas külaelanike kohta andmebaasi.

![](s13-body.png)

Ta salvestas iga külaelaniku andmed järgmise 16-bitise jadana (kus bitid on nummerdatud 0..15 **paremalt vasakule**):

| Bitid | Tähendus |
| ----- | -------- |
| b<sub>15</sub>..b<sub>12</sub> | neli bitti perekonna järjekorranumbriks |
| b<sub>11</sub> | üks bitt soo märkimiseks: 0 = emane; 1 = isane |
| b<sub>10</sub>..b<sub>4</sub> | seitse bitti kehakaaluks (täisarv) |
| b<sub>3</sub>..b<sub>2</sub> | kaks bitti ameti tähistamiseks: 00 = pesaehitaja; 01 = tammiehitaja; 10 = toiduvaruja; 11 = õpetaja |
| b<sub>1</sub>..b<sub>0</sub> | kaks bitti lemmiktoidu tähistamiseks: 00 = puukoor; 01 = vetikad; 10 = kõrrelised; 11 = laugulised |

Näiteks bitijada 0100 0 0100101 10 01 (kus tühikud on loetavuse parandamiseks) tähistab kobrast, kes kuulub perekonda number 4, on emane, kelle kaal on 37 ja kes töötab toiduvarujana ning armastab vetikaid.

Jeffrey esitab andmebaasi päringuid loogiliste avaldistena, kus bittide väärtusi tõlgendatakse järgmiselt: 0 = väär; 1 = tõene.

## Küsimus

Millised koprad vastavad järgmisele avaldisele?

b<sub>11</sub> JA MITTE(b<sub>10</sub>) JA b<sub>9</sub> JA b<sub>7</sub> JA MITTE(b<sub>3</sub> JA b<sub>2</sub>)

[Raadionupud]

A. Emased, kelle kaal on vähemalt 16 ja kes töötavad toiduvarujatena

B. Isased, kelle kaal on vähemalt 64 ja kes töötavad pesa- või tammiehitajatena

C. Isased, kelle kaal on 40 kuni 63 ja kes töötavad pesa- või tammiehitajate või toiduvarujatena

D. Isased, kelle kaal kuni 39 ja kes töötavad tammiehitajatena

## Vastus

Õige vastus on: C (isased, kelle kaal on 40 kuni 63 ja kes töötavad pesa- või tammiehitajate või toiduvarujatena).

## Vastuse selgitus

Õige vastus on C, sest

- b<sub>11</sub> ehk b<sub>11</sub> = 1 tähendab "isane";
- MITTE(b<sub>10</sub>) ehk b<sub>10</sub> = 0 tähendab "kaal on alla 64";
- b<sub>9</sub> JA b<sub>7</sub> ehk b<sub>9</sub> = 1 ja b<sub>7</sub> = 1 tähendab "kaal on vähemalt 32+8=40";
- MITTE(b<sub>3</sub> JA b<sub>2</sub>) välistab ainult juhu b<sub>3</sub> = 1 ja b<sub>2</sub> = 1, mis tähendaks "õpetaja", ja seega kõik teised ametid vastavad tingimusele.

Variant A ei saa olla õige, sest räägib emastest kobrastest, aga b<sub>11</sub> = 1 tähendab "isane". Sellele variandile vastav avaldis oleks MITTE(b<sub>11</sub>) JA b<sub>8</sub> JA b<sub>3</sub> JA MITTE(b<sub>2</sub>).

Variant B ei saa olla õige, sest räägib kaalust vähemalt 64, aga b<sub>10</sub> = 0 tähendab "kaal on alla 64". Sellele variandile vastav avaldis oleks b<sub>11</sub> JA b<sub>10</sub> JA MITTE(b<sub>3</sub>).

Variant C ei saa olla õige, sest räägib kaalust kuni 39, aga b<sub>9</sub> = 1 ja b<sub>7</sub> = 1 tähendab "kaal on vähemalt 32+8=40". Sellele variandile vastav avaldis oleks päris keeruline: b<sub>11</sub> JA MITTE(b<sub>10</sub>) JA MITTE(b<sub>9</sub> JA b<sub>8</sub>) JA MITTE(b<sub>9</sub> JA b<sub>7</sub>) JA MITTE(b<sub>3</sub>) JA b<sub>2</sub>.

## See on informaatika!

Arvuti mälus kodeeritakse kõik andmed bittidena ja elektroonika tasemel toimuvad kõik operatsioonid, sealhulgas ka arvude võrdlemised, bitiloogika tehetena. Kasutajate eest on see keerukus tavaliselt varjatud, sest sõltuvalt operatsiooni liigist ja protsessori tüübist teeb kasutajale nähtavate operatsioonide teisendamise bitiloogika teheteks ära kas rakendustarkvara, süsteemitarkvara või protsessor ise.

Programmi- ja päringukeeltes on lisaks JA (ingl _AND_) ja MITTE (ingl _NOT_) tehetele tavaliselt võimalik kasutada veel VÕI (ingl _OR_) ja sageli ka "välistav VÕI" (ingl _exclusive OR_, _XOR_) tehteid. Need lisatehted teevad mõnede loogiliste avaldiste kirjutamise mugavamaks, aga pole rangelt võttes vajalikud, sest kõik avaldised saab kirja panna ka ainult JA ja MITTE kombinatsioonidena.

Enamgi veel, tegelikult pole vaja isegi kahte erinevat tehet. On võimalik näidata, et kõik loogilised avaldised saab kirja panna ainult NING-EI (ingl _NOT-AND_, _NAND_) tehte abil, kus x NING-EI y = MITTE(x JA y). Selle kohta öeldakse, et NING-EI on universaalne tehe. Samamoodi on universaalne tehe ka VÕI-EI (ingl _NOT-OR_, _NOR_), kus x VÕI-EI y = MITTE(x VÕI y). Paljud elektroonikaskeemid koosnevadki ainult NING-EI elementidest, sest neid on pooljuhtidest väga lihtne moodustada.

Kopraküla teadlase Jeffrey nimi viitab USA arvutiteadlasele Jeffrey Ullmanile, kes on muude arvutiteaduse valdkondade kõrval andnud olulise panuse ka andmebaasisüsteemide alasesse uurimistöösse.

## Kategooriad

- Varia
- Loogika
- Kodeerimine

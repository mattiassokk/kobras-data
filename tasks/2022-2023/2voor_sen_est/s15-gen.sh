#!/bin/bash

python3 s15-gen.py 6 5 "1, 3, 1, 0, 1, 3, 1, 0, 1, 4, 0, 1, 2, 2, 0, 1, 3, 1, 1, 3, 1" yes >s15-body.svg

python3 s15-gen.py 6 5 "0, 4, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1" yes >s15-option-a.svg
python3 s15-gen.py 6 5 "1, 4, 4, 1, 1, 3, 1, 0, 1, 4, 0, 1, 3, 1, 1, 3, 1" yes >s15-option-b.svg
python3 s15-gen.py 6 5 "1, 4, 0, 1, 4, 1, 3, 1, 4, 1, 0, 1, 3, 1, 1, 3, 1" no >s15-option-c1.svg
python3 s15-gen.py 6 5 "1, 4, 0, 1, 4, 0, 1, 3, 1, 4, 1, 0, 1, 3, 1, 1, 3, 1" yes >s15-option-c2.svg
python3 s15-gen.py 6 5 "1, 4, 0, 1, 4, 1, 3, 1, 4, 1, 0, 1, 3, 1, 1, 3, 1" yes >s15-option-d.svg

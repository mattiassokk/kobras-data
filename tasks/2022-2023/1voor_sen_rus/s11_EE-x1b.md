# 11. Оформление текста

В приведённом тексте пробелы отмечены символом '`·`', а переходы на новые абзацы символом '`¶`'. Неотмеченные переходы на новую строку автоматически добавляются текстовым процессором.

    Логика·—·это·наука·о·правилах·,структурах·и·формах·мышления.

    Слово·«логика»·первоначально·происходит·от·древнегреческого·прилагательного·
    λογική·(·logikē),·которое·является·женской·формой·λογικός·(logikos;·«относящийся·
    к·речи·;·относящийся·к·мышлению»).·Слово·использовалось·либо·как·отдельное·
    существительное,либо·во·фразе·λογικὴ·τέχνη(logikē·technē)·в·смысле·«искусство·
    мышления».

## Вопрос

Сколько существует пар слов, между которыми при оформлении текста пробелы были использованы неправильно?

(Если в одной паре допущено несколько ошибок, то считай это за одну ошибку.)

[Täisarv]

## Ответ

Правильный ответ: 5.

## Объяснение ответа

Первая ошибка — это '**`правилах·,структурах`**', где пробел должен быть после запятой, а не перед.

Вторая ошибка — это '**`λογική·(·logikē)`**', где пробела не должно быть после скобки.

Третья ошибка — это '**`речи·;·относящийся`**', где пробела не должно быть перед точкой с запятой.

Четвертая ошибка — это '**`существительное,либо`**', где пробел должен быть после запятой.

Пятая ошибка — это '**`τέχνη(logikē`**', где пробел должен быть перед скобкой.

## Это информатика!

Помимо того, что неправильное использование знаков препинания является ошибкой правила правописания, оно также может привести к неудобствам, связанным с автоматическим переносом строки во время обработки текстов на компьютере. Если не поставить пробел после запятой или точки, то при необходимости текстовой редактор не сделает в этом месте разрыва строки; это может повлиять на выравнивание текста. Если поставить пробел перед запятой или точкой, то при автоматическом переходе на новую строку текстовой редактор может перенести знак препинания в начало следующей строки, а это выглядит некрасиво.

По тем же причинам пробелы ставятся перед открывающей скобкой и открывающими кавычками, а также после закрывающей скобки и закрывающими кавычками, но не после открывающей скобки и открывающих кавычек или перед закрывающей скобкой и закрывающими кавычками.

С точки зрения правописания мы можем обсудить, имеется ли в случае '**`правилах·,структурах`**' одна ошибка (пробел и запятая в неправильном порядке) или две ошибки (лишний пробел перед запятой, отсутствие необходимого пробела после запятой). Чтобы было ясно и понятно, то вместо количества ошибок в задании спрашивается количество пар слов, где что-то не так. Такая же точность требуется во многих ситуациях, когда разные стороны (будь то люди или компьютеры) должны понимать правило одинаково — например, когда оценивание за диктант на экзамене по эстонскому языку основывается на количестве ошибок.

## Категории

- Обработка текста

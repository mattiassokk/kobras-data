# 15. Ahelkood

Ahelkood on kokkuhoidlik viis must-valgel pildil olevate kontuuride (piirjoonte) esitamiseks. Põhimõte on valida üks must piksel ja salvestada igal sammul suund liikumiseks järgmise musta pikslini kontuuri päripäeva (kellaosuti liikumise suunas) läbimisel.

Suunad on tähistatud numbritega selliselt:

![](j15-directions.png)

Näiteks kui valime alloleval pildil esimeseks sinise raamiga tähistatud piksli, saame edasi liikuda punaste nooltega näidatud suundades ja tulemuseks on ahelkood 1, 0, 0, 7, 7, 5, 5, 4, 3, 3, 2.

![](j15-example.png)

Vaatame nüüd sellist pilti:

![](j15-question.png)

## Küsimus

Milline järgmistest **ei ole** selle pildi ahelkood?

[Raadionupud]

A. 1, 1, 1, 0, 7, 7, 7, 6, 5, 5, 5, 4, 3, 3, 3, 2

B. 7, 6, 5, 5, 5, 5, 4, 3, 3, 2, 1, 1, 1, 1, 0, 7

C. 5, 5, 5, 4, 3, 3, 3, 2, 1, 1, 1, 0, 7, 7, 7, 6

D. 3, 3, 2, 1, 1, 1, 0, 7, 7, 7, 6, 5, 5, 5, 4, 3

## Vastus

Õige vastus on: B.

## Vastuse selgitus

Järgnevad joonised illustreerivad vastusevariantides olevate ahelkoodide saamist:

A. ![](j15-solution-a.png)
B. ![](j15-solution-b.png)

C. ![](j15-solution-c.png)
D. ![](j15-solution-d.png)

## See on informaatika!

Ahelkood on üks meetod objektide esitamiseks nende kontuuride abil. Ahelkood on määratud stardipiksli määramisega ja suunavektorite jadaga vastavalt sellele, kas igal sammul suundutakse kontuuri järgmise musta pikslini vasakul, paremal, üleval, all või diagonaalis. Ahelkoodi eeliseks on vajaliku info säilitamine ja samas andmemahu väga suur kokkuhoid. Esimesena esitles ahelkoodi ideed Freeman 1961. aastal ning seepärast on see tuntud ka kui Freemani ahelkood (ingl _Freeman Chain Code_, FCC).

Ahelkoodi üks kasutusala on numbrituvastus. Esiteks võimaldab see kood objekte kompaktselt esitada, teiseks on ahelkoodide võrdlemine lihtsam kui piltide võrdlemine. Lisaks sellele on ahelkoodist võimalik välja lugeda ka kujutatava objekti üksikuid omadusi. Ahelkoodid esindavad kadudeta tihendamist ja säilitavad kogu info, mis on vajalik kiireks ja efektiivseks mustrite analüüsiks.

## Kategooriad

- Info mõistmine
- Geomeetria
- Kodeerimine
- Varia

import sys

scale = 50
margin = 0.2 * scale

dirs = [(+1, 0), (+1, -1), (0, -1), (-1, -1), (-1, 0), (-1, +1), (0, +1), (+1, +1)]
chain = list(map(int, sys.argv[1].split(', ')))

minx, miny = 0, 0
maxx, maxy = 0, 0
x, y = 0, 0
for d in chain:
	dx, dy = dirs[d]
	x, y = x + dx, y + dy
	minx, miny = min(minx, x), min(miny, y)
	maxx, maxy = max(maxx, x), max(maxy, y)
sizex, sizey = maxx - minx + 1, maxy - miny + 1

print(f'<svg width="{sizex * scale + 2 * margin}" height="{sizey * scale + 2 * margin}">')
print(f'  <defs>')
print(f'    <marker id="arrow" markerWidth="10" markerHeight="10" refX="9" refY="3" orient="auto" markerUnits="strokeWidth">')
print(f'      <path d="M0,0 L0,6 L9,3 z" fill="#f00" />')
print(f'    </marker>')
print(f'  </defs>')

print(f'')
for x in range(minx, maxx + 2):
	print(f'  <line x1="{(x - minx) * scale + margin}" y1="{margin}" x2="{(x - minx) * scale + margin}" y2="{sizey * scale + margin}" stroke="#444" stroke-width="1" />')
for y in range(miny, maxy + 2):
	print(f'  <line x1="{margin}" y1="{(y - miny) * scale + margin}" x2="{sizex * scale + margin}" y2="{(y - miny) * scale + margin}" stroke="#444" stroke-width="1" />')

print(f'')
x, y = 0, 0
for d in chain:
	print(f'  <rect x="{(x - minx) * scale + margin}" y="{(y - miny) * scale + margin}" width="{scale}" height="{scale}" stroke="#444" stroke-width="1" fill="#000" />')
	dx, dy = dirs[d]
	x, y = x + dx, y + dy
assert (x, y) == (0, 0)

if sys.argv[2] == 'x':
	print(f'')
	print(f'  <rect x="{(x - minx) * scale + margin}" y="{(y - miny) * scale + margin}" width="{scale}" height="{scale}" stroke="#00f" stroke-width="3" fill="#000" />')

if sys.argv[2] == 'x':
	print(f'')
	x1, y1 = 0, 0
	for d in chain:
		dx, dy = dirs[d]
		x2, y2 = x1 + dx, y1 + dy
		print(f'  <line x1="{(x1 - minx + 0.5) * scale + margin}" y1="{(y1 - miny + 0.5) * scale + margin}" x2="{(x2 - minx + 0.5) * scale + margin}" y2="{(y2 - miny + 0.5) * scale + margin}" stroke="#f00" stroke-width="2" marker-end="url(#arrow)" />')
		xt, yt = x1 + + 0.5 * dx + 0.25 * dy, y1 + 0.5 * dy - 0.25 * dx
		print(f'  <text x="{(xt - minx + 0.5) * scale + margin}" y="{(yt - miny + 0.5) * scale + margin}" fill="#f00" font-size="20" text-anchor="middle" dominant-baseline="middle">{d}</text>')
		x1, y1 = x2, y2

print(f'</svg>')

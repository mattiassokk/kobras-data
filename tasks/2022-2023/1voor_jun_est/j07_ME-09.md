# 7. Koodikobraste laht

Koodikobraste lahes on tuletornid valgustatud erilisel viisil. Igal majakal on oma unikaalne kood, mille järgi ta vilgub. Nii saavad meremehed lihtsalt tuvastada, millist majakat nad parajasti vaatavad.

![](j07-body.png)

Näiteks kui tuletornil on kood "1011001001", siis esimese kümne sekundi jooksul lülitub ta sisse ja välja nii: 1. sekundil põleb, 2. sekundil ei põle, 3. sekundil põleb, 4. sekundil põleb, 5. sekundil ei põle, 6. sekundil ei põle, 7. sekundil põleb, 8. sekundil ei põle, 9. sekundil ei põle, 10. sekundil põleb.

Iga järgmise 10 sekundi jooksul see muster kordub.

## Küsimus

Kui kõik tuletornid hakkavad tööle samal ajal, siis mitmendal sekundil põlevad kõigi majakate tuled korraga?

[Täisarv]

## Vastus

Õige vastus on: 7.

## Vastuse selgitus

Kui me kirjutame kõigi majakate koodid üksteise alla, on lihtne märgata, et kõigis koodides on seitsmendal kohal number 1 (mis tähistab seda, et tuli majakas põleb).

|1|2|3|4|5|6|  7  |8|9|10|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|0|1|0|0|0|1|**1**|0|1|0|
|0|1|0|1|0|0|**1**|0|0|1|
|1|1|0|0|1|1|**1**|0|0|0|
|1|0|0|0|1|1|**1**|0|0|1|
|1|0|1|1|0|0|**1**|0|0|1|
|1|0|0|1|0|0|**1**|0|0|1|

## See on informaatika!

Informaatikas tuleb meil sageli kodeerida mingeid sündmusi või olekuid arvude või tähtede abil, sageli kasutame selleks ka erinevaid arvusüsteeme. Kui näiteks leiab aset mingi sündmus, võime me selle tähistamiseks omistada mõnele muutujale sobiva väärtuse, et oleks selge: selline sündmus leidis aset või saavutati mingi olek. Hiljem programmi töö käigus saame sellest väärtusest lähtudes käivitada ühe või teise funktsiooni.

## Kategooriad

- Info mõistmine
- Kodeerimine

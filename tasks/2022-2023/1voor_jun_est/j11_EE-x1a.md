# 11. Teksti vormistamine

Järgnevas tekstilõigus on tühikud tähistatud märgiga '`·`' ja lõiguvahetused märgiga '`¶`'. Tähistamata reavahetused on tekstitöötlusprogrammi poolt automaatselt lisatud.

    §·1.·Eesti·on·iseseisev·ja·sõltumatu·demokraatlik·vabariik·,kus·kõrgeima·
    riigivõimu·kandja·on·rahvas.·Eesti·iseseisvus·ja·sõltumatus·on·aegumatu·
    ning·võõrandamatu.¶

    §·2.·Eesti·riigi·maa-ala,territoriaalveed·ja·õhuruum·on·lahutamatu·ja·
    jagamatu·tervik·.·Eesti·on·riiklikult·korralduselt·ühtne·riik,·mille·
    territooriumi·haldusjaotuse·sätestab·seadus.¶

## Küsimus

Mitmes sõnavahes on selle teksti vormistamisel tühikuid valesti kasutatud?

(Kui ühes sõnavahes on mitu viga, lugeda seda ainult üks kord.)

[Täisarv]

## Vastus

Õige vastus on: 3.

## Vastuse selgitus

Esimene viga on '**`vabariik·,kus`**', kus tühik peaks olema koma järel, mitte selle ees.

Teine viga on '**`maa-ala,territoriaalveed`**', kus peaks koma järel olema tühik.

Kolmas viga on '**`tervik·.·Eesti`**', kus punkti ees ei peaks tühikut olema.

## See on informaatika!

Lisaks sellele, et kirjavahemärkide valesti kasutamine on eksimine õigekirjareeglite vastu, võib see tekstide arvutis töötlemisel kaasa tuua ka ebameeldivusi automaatse reapoolitusega. Kui jätta koma või punkti järele tühik panemata, ei saa tekstitöötlusprogramm sellesse kohta vajadusel reavahetust lisada; sellega võib kannatada teksti joondamine. Kui panna tühik koma või punkti ette, võib tekstitöötlusprogramm sinna automaatse reavahetuse lisada ja nii satub kirjavahemärk üksikuna järgmise rea algusse; ka see näeb inetu välja.

Õigekirja seisukohalt võib arutada, kas '**`vabariik·,kus`**' on üks viga (tühik ja koma on omavahel vales järjekorras) või kaks viga (koma ees liigne tühik, koma järel vajalik tühik puudu). Selleks, et oleks üheselt selge, ongi ülesandes küsitud vigade arvu asemel nende sõnavahede arvu, kus on midagi valesti. Samasugust täpsust on vaja paljudes olukordades, kus erinevad osapooled (olgu inimesed või arvutid) peavad mingit reeglit kõik ühtemoodi mõistma — näiteks siis, kui eesti keele eksami etteütluse osas on hindamine määratud vigade arvuga.

## Kategooriad

- Tekstitöötlus

for x in range(2**8):
	s = [int(c) for c in f'{x:08b}']
	if ((s[-1] & s[-2]) & (s[-3] ^ s[-4])) & ((s[-5] & s[-6]) ^ (s[-7] ^ s[-8])) == 1:
		print(''.join([str(i) for i in range(1, 9) if s[-i] == 1]))

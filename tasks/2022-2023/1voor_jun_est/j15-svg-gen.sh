#!/bin/bash

python3 j15-main.py '7, 6, 5, 5, 5, 4, 3, 3, 3, 2, 1, 1, 1, 0, 7, 7' ' ' >j15-question.svg

python3 j15-main.py '1, 1, 1, 0, 7, 7, 7, 6, 5, 5, 5, 4, 3, 3, 3, 2' 'x' >j15-solution-a.svg
python3 j15-main.py '7, 6, 5, 5, 5, 5, 4, 3, 3, 2, 1, 1, 1, 1, 0, 7' 'x' >j15-solution-b.svg
python3 j15-main.py '5, 5, 5, 4, 3, 3, 3, 2, 1, 1, 1, 0, 7, 7, 7, 6' 'x' >j15-solution-c.svg
python3 j15-main.py '3, 3, 2, 1, 1, 1, 0, 7, 7, 7, 6, 5, 5, 5, 4, 3' 'x' >j15-solution-d.svg

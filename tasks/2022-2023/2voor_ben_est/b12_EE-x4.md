# 12. Diagrammid

Kobras Brunol on arvutustabelis andmed ja valemid.

![](b12-body.png)

Nüüd hakkab ta valemitega arvutatud väärtustest (real 3) diagramme joonistama.

## Küsimus

Millist allolevatest diagrammidest ta nende andmete põhjal kindlasti **ei saa**?

[Raadionupud]

A. ![](b12-option-a.png)

B. ![](b12-option-b.png)

C. ![](b12-option-c.png)

D. ![](b12-option-d.png)

## Vastus

Õige vastus on: A.

## Vastuse selgitus

Vastuse leidmiseks pole isegi vaja kõigi valemilahtrite väärtusi välja arvutada. Piisab tähelepanekust, et kolmel diagrammil on üks väärtus kõige suurem ja selle järel suuruselt teisi väärtusi kaks võrdset, aga diagrammil A on just maksimaalse suurusega väärtusi kaks võrdset. Seega ei ole diagramm A kindlasti koostatud samadest andmetest, millest ülejäänud.

## See on informaatika!

Andmete töötlemisel võib kergesti juhtuda, et midagi läheb valesti. Sellepärast on alati kasulik oma töö tulemusele peale vaadata ja mõelda, kas seal on midagi, mis ilmselgelt ei klapi või ei sobi.

## Kategooriad

- Info mõistmine
- Tabelitöötlus

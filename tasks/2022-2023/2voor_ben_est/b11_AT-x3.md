# 11. Telefoniraamat

Madis otsib väga pikalt veebilehelt oma sõbra telefoninumbrit. Ta pole kindel, kuidas sõbra nime täpselt kirjutatakse, seepärast kasutab ta otsimisel erimärke:

| Märk | Tähendus |
| :--: | -------- |
| ? | täpselt üks tundmatu täht |
| & | täpselt kaks järjestikust tundmatut tähte |
| % | mistahes arv tundmatuid tähti kuni nime lõpuni |

Näiteks otsing "Ma%" leiaks nimed Madis, Marta jne.

## Küsimus

Millised järgnevatest nimedest leiab otsing "S?rah B&cht%"?

(Märgi kõik õiged vastused.)

[Märkeruudud]

A. Sarah Birchtree

B. Sara Benchton

C. Sarah Bilchman

D. Sirah Beachtram

## Vastus

Õige vastus on: A, D.

## Vastuse selgitus

Variant A on õige, sest nimi "Sarah Birchtree" vastab otsingule "S?rah B&cht%":

- eesnime esitäht "S" vastab otsingu esitähele "S";
- eesnime teine täht "a" vastab otsingu erimärgile "?";
- eesnime järgmised tähed "rah" vastavad otsingu järgmistele tähtedele "rah";
- nime tühik vastab otsingu tühikule;
- perenime esitäht "B" vastab otsingu tähele "B";
- perenime kaks järgmist tähte "ir" vastavad otsingu erimärgile "&";
- perenime kolm järgmist tähte "cht" vastavad otsingu järgmistele tähtedele "cht";
- perenime lõpp "ree" vastab otsingu erimärgile "%".

Variant B ei ole õige, sest eesnimes "Sara" ei ole "h"-tähte.

Variant C ei ole õige, sest perenimes "Bilchman" on "cht" asemel "chm".

Variant D on õige, sest "S" ja "S", "i" ja "?", "rah" ja "rah", tühik ja tühik, "B" ja "B", "ea" ja "&", "cht" ja "cht" ning "ram" ja "%" kõik vastavad.

## See on informaatika!

Paljud infosüsteemid võimaldavad info otsimiseks kasutada selles ülesandes kirjeldatud erimärkidele sarnaseid nn metamärke (ingl _wildcard_) ehk jokkereid. Näiteks nii Windowsi, MacOSi kui Linuxi paljudes käskudes on võimalik mitme faili korraga nimetamiseks kasutada "?" tähenduses "täpselt üks tundmatu märk" ja "*" tähenduses "mistahes arv tundmatuid märke".

Teise näitena lubavad paljud tekstitöötlusprogrammid teksti otsimisel ja ka asendamisel kasutada mustrite kirjeldamiseks nn regulaaravaldisi (ingl _regular expression_).

Sellised vahendid võimaldavad aega kokku hoida, sest nii saab käske rakendada korraga paljudele failidele või sõnadele. Ilma nendeta tuleks käsk anda kas iga faili või iga võimaliku sõna kohta eraldi.

## Kategooriad

- Info mõistmine
- Tekstitöötlus

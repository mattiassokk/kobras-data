# 15. Piltide teisendamine arvudeks

Vaatame 6x5 ruudust koosnevat pilti, mille osad ruudud on punased, osad valged:

![](b15-body-example.png)

Selle pildi saab teisendada arvujadaks, lugedes iga rea puhul üle, mitu järjestikust ruutu on valged, seejärel mitu järjestikust ruutu on punased, siis mitu järjestikust ruutu on valged jne, kuni jõuame rea lõppu. Pane tähele, et kui rida algab punase ruuduga, siis on selle rea alguses olevate valgete ruutude arv 0.

Lõpuks ühendame kõigi pildiridade kõik arvud ühte jadasse. Eeltoodud näites oleks tulemuseks selline jada: 1, 3, 1, 0, 1, 3, 1, 0, 1, 4, 0, 1, 2, 2, 0, 1, 3, 1, 1, 3, 1.

## Küsimus

Milline on allolevat pilti kirjeldav arvujada?

![](b15-body-question.png)

[Raadionupud]

A. 0, 1, 3, 4, 1, 1, 3, 1, 0, 2, 2, 1, 0, 1, 3, 1, 2, 2, 1

B. 1, 3, 1, 4, 1, 1, 4, 0, 1, 3, 1, 0, 1, 3, 1, 1, 3, 1

C. 1, 3, 1, 0, 1, 4, 1, 4, 0, 1, 3, 1, 0, 1, 3, 1, 1, 3, 1

D. 1, 3, 1, 4, 1, 1, 4, 1, 3, 1, 1, 3, 1, 1, 3, 1

## Vastus

Õige vastus on: B.

## Vastuse selgitus

Vastuse leidmiseks on vaja vähemalt neli pildirida arvudeks teisendada ja seejärel ühendada:

![](b15-solution.png)

Tulemuseks on jada 1, 3, 1, 4, 1, 1, 4, 0, 1, 3, 1, 0, 1, 3, 1, 1, 3, 1, mis vastab variandile B.

Ülejäänud kolm vastust esindavad teisi pilte, kuna iga pildi esitus on üheselt määratud. Teisisõnu: erinevad esitused vastavad alati erinevatele piltidele.

## See on informaatika!

Piltide salvestamiseks või edastamiseks elektroonikaseadmete vahel on vaja need arvudeks teisendada. Selleks on palju viise ja siin on toodud neist üks. Kasutades ühte arvu sama värvi ruutude jada esitamiseks, tihendab see meetod andmeid ehk vähendab andmete hulka ja see on informaatikas oluline. Seda meetodit saab täiustada mitmel viisil, näiteks lubades arvul viidata rohkem kui ühte rida hõlmavale ruutude jadale.

Andmete esitamise viiside leidmine on olnud väljakutse alates juba esimestest elektroonilistest seadmetest, sest sellega seotud valikud võivad mõjutada teabe töötlemiseks või saatmiseks ja vastuvõtmiseks kuluvat aega. Praegu on see probleem endiselt aktuaalne, eriti internetis: uute viiside loomine piltide, videote ja muude meediumifailide kodeerimiseks võib meie veebikasutamist kiirendada.

Selles ülesandes kirjeldatud meetodit kasutasid vanad faksiaparaadid dokumentide sisu edastamiseks.

## Kategooriad

- Info mõistmine
- Kodeerimine

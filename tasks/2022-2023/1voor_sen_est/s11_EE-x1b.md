# 11. Teksti vormistamine

Järgnevas tekstilõigus on tühikud tähistatud märgiga '`·`' ja lõiguvahetused märgiga '`¶`'. Tähistamata reavahetused on tekstitöötlusprogrammi poolt automaatselt lisatud.

    Loogika·on·teadus·mõtlemise·reeglitest·,struktuuridest·ja·vormidest.¶

    Sõna·"loogika"·pärineb·algselt·vanakreeka·omadussõnast·λογική·(·logikē),·mis·on·
    sõna·λογικός·(logikos;·'kõnega·seonduv·;·mõtlemisega·seonduv')·naissoovorm.Seda·
    sõna·kasutati·kas·eraldi·nimisõnana·või·fraasis·λογικὴ·τέχνη(logikē·technē)·
    'mõtlemiskunsti'·tähenduses.¶

## Küsimus

Mitmes sõnavahes on selle teksti vormistamisel tühikuid valesti kasutatud?

(Kui ühes sõnavahes on mitu viga, lugeda seda ainult üks kord.)

[Täisarv]

## Vastus

Õige vastus on: 5.

## Vastuse selgitus

Esimene viga on '**`reeglitest·,struktuuridest`**', kus tühik peaks olema koma järel, mitte selle ees.

Teine viga on '**`λογική·(·logikē)`**', kus sulu järel ei peaks tühikut olema.

Kolmas viga on '**`kõnega·seonduv·;·mõtlemisega·seonduv`**', kus semikooloni ees ei peaks tühikut olema.

Neljas viga on '**`naissoovorm.Seda`**', kus punkti järel peaks tühik olema.

Viies viga on '**`τέχνη(logikē`**', kus sulu ees peaks tühik olema.

## See on informaatika!

Lisaks sellele, et kirjavahemärkide valesti kasutamine on eksimine õigekirjareeglite vastu, võib see tekstide arvutis töötlemisel kaasa tuua ka ebameeldivusi automaatse reapoolitusega. Kui jätta koma või punkti järele tühik panemata, ei saa tekstitöötlusprogramm sellesse kohta vajadusel reavahetust lisada; sellega võib kannatada teksti joondamine. Kui panna tühik koma või punkti ette, võib tekstitöötlusprogramm sinna automaatse reavahetuse lisada ja nii satub kirjavahemärk üksikuna järgmise rea algusse; ka see näeb inetu välja.

Samadel põhjustel käivad tühikud algava sulu ja algavate jutumärkide ette ning lõpetava sulu ja lõpetavate jutumärkide järele, aga ei käi algava sulu ja algavate jutumärkide järele ning lõpetava sulu ja lõpetavate jutumärkide ette.

Õigekirja seisukohalt võib arutada, kas '**`reeglitest·,struktuuridest`**' on üks viga (tühik ja koma on omavahel vales järjekorras) või kaks viga (koma ees liigne tühik, koma järel vajalik tühik puudu). Selleks, et oleks üheselt selge, ongi ülesandes küsitud vigade arvu asemel nende sõnavahede arvu, kus on midagi valesti. Samasugust täpsust on vaja paljudes olukordades, kus erinevad osapooled (olgu inimesed või arvutid) peavad mingit reeglit kõik ühtemoodi mõistma — näiteks siis, kui eesti keele eksami etteütluse osas on hindamine määratud vigade arvuga.

## Kategooriad

- Tekstitöötlus

# 12. Arvutustabel

Vaatame järgmist tabelarvutuse töölehte, mille mõnes lahtris on arvud ja mõnes valemid:

![](s12-body.png)

Töölehe funktsioon `SUM` arvutab antud piirkonna lahtrites olevate väärtuste summa, funktsioon `MAX` aga leiab antud piirkonna lahtrites olevatest väärtustest maksimaalse. Lahtrite piirkonnad on antud oma vasakpoolseima ülemise ja parempoolseima alumise lahtriga.

## Küsimus

Milline väärtus tuleb tabeli lahtrisse C3?

[Täisarv]

## Vastus

Õige vastus on: 21.

## Vastuse selgitus

Lihtne viis vastuse leidmiseks on kõigi lahtrite väärtused välja arvutada. Nii saame

    B2 = SUM(A1:A3) = A1+A2+A3 = 1+6+7 = 14
    B3 = SUM(A2:A4) = A2+A3+A4 = 6+7+8 = 21
    B4 = SUM(A3:A5) = A3+A4+A5 = 7+8+4 = 19
    C3 = MAX(A1:A3) = MAX(14;21;19) = 21

Teine võimalus oleks panna tähele, et B3 väärtus erineb B2 väärtusest selle poolest, et summast jääb välja 1, aga lisandub 8; seega peab B3 väärtus olema suurem. Analoogiliselt saame, et B3 väärtus on suurem ka B4 omast. Seega on just B3 väärtus see maksimum, mille lahtris C3 olev valem valib ja võime piirduda ainult selle ühe summa väljaarvutamisega.

## See on informaatika!

Tabelarvutuse töölehtede valemid võimaldavad korduvaid arvutusi automatiseerida ning sellega kasutajate aega kokku hoida ja käsitsi arvutamisest tulenevate vigade arvu vähendada.

Ka tabelarvutussüsteemid püüavad tehtava töö mahtu optimeerida. Kui me mingi lahtri väärtust muudame, siis arvutab süsteem üle ainult nende valemite väärtused, mis sellest lahtrist sõltuvad (ja siis need valemid, mis muutunud väärtustest sõltuvad jne). Meetod on küll erinev sellest, mida me eespool summade väljaarvutamise vältimiseks kasutasime, aga eesmärk on sama: teha vähem tühja tööd. Arvutid arvutavad küll kiiresti, aga väga suurte paljude valemitega töölehtede puhul on kokkuhoid ikka vajalik.

## Kategooriad

- Tabelitöötlus

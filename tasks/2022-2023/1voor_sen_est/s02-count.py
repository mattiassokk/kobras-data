# Uncomment one of the below:
# state = [4, 3, 5, 2, 6, 1]
# state = [6 - i for i in range(6)]
# state = [60 - i for i in range(60)]
# state = [i + 1 for i in range(60)]
index = 1
count = 0
while index < len(state):
	if state[index - 1] < state[index]:
		index += 1
		count += 1
	else: # state[index - 1] > state[index]
		state[index - 1], state[index] = state[index], state[index - 1]
		if index > 1:
			index -= 1
			count += 1
print(count)

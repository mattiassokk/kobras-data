edges = ['AB', 'AC', 'AD', 'AE', 'AF', 'BC', 'BE', 'CD', 'CI', 'DH', 'DI', 'EF', 'EH', 'EI', 'FH', 'GI']

graph = {}
for e in edges:
	if e[0] not in graph: graph[e[0]] = []
	graph[e[0]].append(e[1])
	if e[1] not in graph: graph[e[1]] = []
	graph[e[1]].append(e[0])
#for v in sorted(list(graph)): print(v, len(graph[v]), graph[v])

def run(a, b):
	global graph
	state = {v: 1 if v == a or v == b else 0 for v in graph}
	while True:
		change = False
		for v in graph:
			if state[v] == 0 and 2 * sum(state[u] for u in graph[v]) > len(graph[v]):
				state[v] = 1
				change = True
		if not change:
			#d = [v for v in graph if state[v] == 1]
			#if len(d) > 2: print(a, b, d)
			return min(state[v] for v in graph)

for a in graph:
	for b in graph:
		if a < b:
			if run(a, b) == 1:
				print(a, b)

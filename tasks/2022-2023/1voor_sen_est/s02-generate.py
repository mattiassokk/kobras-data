# to tune the animated GIF
# 1. edit the script as needed
# 2. execute "python generate.py" to generate the frames as SVG files
# 3. execute "inkscape --export-type=png *.svg" to convert the SVG files to PNG files
# 4. use either GIMP or any online service to merge the PNG files into an animated GIF

scale = 30

def make_svg():
	global state, index, count, frame
	w, h = 2 * len(state) + 1, max(state) + 2
	with open(f'frame_{frame:03d}.svg', 'wt') as f:
		f.write(f'<svg width="{w * scale}" height="{h * scale}">\n')
		f.write(f'  <rect x="{0}" y="{0}" width="{w * scale}" height="{h * scale}" fill="aqua" />\n')
		f.write(f'  <rect x="{0}" y="{(h - 0.5) * scale}" width="{w * scale}" height="{0.5 * scale}" fill="green" />\n')
		f.write(f'\n')
		for i, s in enumerate(state):
			f.write(f'  <ellipse cx="{(2 * i + 1.5) * scale}" cy="{(h - 1 - s) * scale}" rx="{0.5 * scale}" ry="{0.25 * scale}" fill="brown" stroke="brown" stroke-width="1" />\n')
			f.write(f'  <rect x="{(2 * i + 1.0) * scale}" y="{(h - 1 - s) * scale}" width="{scale}" height="{s * scale}" fill="brown" stroke="brown" stroke-width="1" />\n')
			f.write(f'  <ellipse cx="{(2 * i + 1.5) * scale}" cy="{(h - 1) * scale}" rx="{0.5 * scale}" ry="{0.25 * scale}" fill="yellow" stroke="brown" stroke-width="1" />\n')
			f.write(f'  <text x="{(2 * i + 1.5) * scale}" y="{(h - 1.3) * scale}" fill="#000" font-size="16" text-anchor="middle">{s}</text>\n')
			f.write(f'\n')
		f.write(f'  <path d="M {(2 * index + 0.5) * scale} {(h - 1) * scale} l {0.4 * scale} {0.4 * scale} h {-0.2 * scale} v {0.4 * scale} h {-0.4 * scale} v {-0.4 * scale} h {-0.2 * scale} z" fill="blue" stroke="black" stroke-width="1" />\n')
		f.write(f'  <text x="{0.5 * scale}" y="{0.5 * scale}" fill="#000" font-size="16" text-anchor="left" dominant-baseline="hanging">Samme: {count}</text>\n')
		f.write(f'</svg>\n')
	frame += 1

state = [4, 3, 5, 2, 6, 1]
index = 1
count = 0
frame = 0
make_svg()
make_svg()
make_svg()
while index < len(state):
	if state[index - 1] < state[index]:
		index += 1
		count += 1
		make_svg()
	else: # state[index - 1] > state[index]
		state[index - 1], state[index] = state[index], state[index - 1]
		make_svg()
		if index > 1:
			index -= 1
			count += 1
			make_svg()
make_svg()
make_svg()
print(count)

# 13. Paroolide äraarvamine

Kobraste Detektiivibüroo peab viie kurjategija kontodele ligi pääsemiseks ära arvama nende paroolid. Nad teavad juba järgmist:

- Anastassia parool on tema nimi, kuhu on tähtede vahele pandud üks kahekohaline arv, näiteks `Ana99stassia`, `A40nastassia` või `Anastassi17a`.
- Bruno parool on ühe Kobrastoonia küla nimi, millele järgneb üks kaheksast erimärgist (`%` `&` `*` `#` `!` `@` `+` `$`), näiteks `Kopralinn@`, `Kobrasburg*` või `Puumetropol!`.
- Clara parool koosneb neljast inglise tähestiku suurtähest, näiteks `BRZT`, `AAAA` või `EZEZ`.
- Damieni parool koosneb tema nimetähtedest suvalises järjekorras, näiteks `amiDen`, `neimaD` või `Dnmiea`.
- Ellena kasutab samuti paroolina oma nime, kuid iga täht selles võib olla kas suur- või väiketähena, näiteks `ElLeNa`, `ELLena` või `Ellena`.

Detektiivid püüavad paroole ära arvata, proovides iga kurjategija puhul ükshaaval kõiki võimalikke tema mustrile vastavaid paroole. Selleks on neil olemas ka nimekiri kõigist 312 Kobrastoonia külast.

## Küsimus

Kelle parooli nad tõenäoliselt kõige väiksema arvu katsetega ära arvavad?

[Raadionupud]

A. Anastassia

B. Bruno

C. Clara

D. Damieni

E. Ellena

## Vastus

Õige vastus on: E.

## Vastuse selgitus

Tõenäoliselt arvavad detektiivid kõige kiiremini ära parooli, mille jaoks on kõige vähem võimalusi. Seetõttu arvutame, kui palju võimalusi on iga variandi puhul.

- Anastassia nimi koosneb 10 tähest, seega on nende vahel arvu lisamiseks 9 kohta. Kahekohalisi arve on 90. Seega on võimalusi kokku 9·90=810.
- Teame, et külade nimesid, mille hulgast Bruno ühe valis, on 312. Kui ta lisas paroolile ühe erisümboli kaheksast võimalikust, on variantide arv kokku 312·8=2496.
- Kuigi Clara parool on kõige lühem, on tema parooli neljale kohale igaühele 26 variant, kokku on võimaluste arv seega 26·26·26·26=456976!
- Damieni paroolivariantide arvu leidmine nõuab natuke rohkem süvenemist. Esimese sümboli jaoks on 6 kandidaati (`D`, `a`, `m`, `i`, `e` või `n`), teise sümboli jaoks jääb järele 5 varianti (kuna kuuest võimalikust tähest valiti üks välja esimesele kohale), kolmanda jaoks 4 jne. Kokku seega 6·5·4·3·2·1=720 varianti.
- Ellena puhul on iga tähe jaoks kaks varianti: esimese jaoks `e` või `E`, teise jaoks `l` või `L` jne. Kokku on võimalusi ainult 2·2·2·2·2·2=64.

Seega on kõige väiksem variantide arv Ellenal.

Me arvutasime võimaluste arvu täpselt välja kõigi paroolide puhul, kuid piisaks ka sellest, kui teeksime seda ainult Ellena jaoks ja võrdleksime seda tulemust väga üldiselt teiste variantidega (näiteks 90 kahekohalist arvu, 312 küla jne).

## See on informaatika!

Sellel ülesandel on vähemalt kaks seost informaatika ja arvutusliku mõtlemisega.

Esiteks kasutavad paljud inimesed mingit mustrit, kui nad peavad uue parooli välja mõtlema. Nagu sellest ülesandest näha: mida lihtsam on muster, seda lihtsam on kellelgi teisel kõiki võimalusi proovides parooli ära arvata. Ärge unustage, et arvutitega saab paroole proovida automaatselt, seega palju kiiremini kui inimesed, ilma et neil igav hakkaks.

Hea parool koosneb paljudest väiketähtedest, suurtähtedest, numbritest, erimärkidest, näiteks `2kb-C6xV!/<)^-U`. Kui arvestame näiteks 26 väike- ja 26 suurtähe, 10 numbri ja 10 erimärgiga, siis on 15-märgilisi paroole kokku 72<sup>15</sup>=7244150201408990671659859968! On olemas rakendusi, mis genereerivad selliseid paroole ja aitavad neid kasutajal ka meeles pidada (või täpsemalt peavad neid kasutaja eest meeles).

Teine hea võimalus, kui kasutatav süsteem seda lubab, on võtta parooliks ühe lühikese sõna asemel mitmest sõnast koosnev fraas, näiteks `correct horse battery staple`. Kasutades 50000 sõnaga sõnaraamatut, saab selliseid neljasõnalisi fraase moodustada 50000<sup>4</sup>=6250000000000000000. Seda on küll vähem kui eelmises näites, aga siiski päris palju. Samas on selliseid fraase oluliselt lihtsam meeles pidada ja seega sobivad nad ka siis, kui paroolitasku rakendust millegipärast kasutada ei saa.

Teiseks peab programmeerija oskama hinnata, kui kaua mingi arvutus tõenäoliselt aega võtab. Arvutiteadlased nimetavad seda "algoritmide arvutusliku keerukuse määramiseks". Kasutasime sõna "tõenäoline", sest teoreetiliselt on võimalik, et esimene parool, mida proovime, on õige, isegi neil juhtudel, kui võimalusi on miljardeid. Aga võib ka juhtuda, et meil ei vea üldse ja leiame õige parooli alles viimase võimaliku variandina. Keskmiselt leiame õige parooli poolel teel kõigi võimaluste kontrollimisel.

## Kategooriad

- Varia
- Turvalisus
- Kombinatoorika

# 14. Mustrid

Programmeerimises kasutatakse `for`-kordust mingite tegevuste etteantud arv kordi kordamiseks.

Allolevas näites pannakse `i` väärtuseks järjest arvud 1, 2 ja 3 ning iga kord täidetakse korduse kehas (taandega osas) olev käsk; paneme tähele, et selles käsus saab `i` väärtust kasutada:

    for i from 1 to 3
        print(i)

väljastab tulemuse

    1
    2
    3

Operaator `*` (tärn) tähistab arvude puhul korrutamist, teksti puhul selle kordamist:

    for i from 1 to 3
        print(i * "x")

väljastab tulemuse

    x
    xx
    xxx

## Küsimus

Millise mustri saame, kui kasutame allolevat koodilõiku?

    for i from 1 to 5
        print(((6 - i) * 2) * "x")
    for i from 1 to 5
        print((i * 2) * "x")

[Raadionupud]

A. ![](s14-a.png)

B. ![](s14-b.png)

C. ![](s14-c.png)

D. ![](s14-d.png)

## Vastus

Õige vastus on: A.

## Vastuse selgitus

Kõik teised variandid saame kiiresti välistada, pannes tähele, et programm ei trüki tühikuid (`" "`) ning vastusevariant A on ainus, mis neid ei sisalda.

## See on informaatika!

Siin tutvustatud `for`-kordus esineb peaaegu kõigis programmeerimiskeeltes. Korduseid kasutatakse, nagu nimigi ütleb, programmiosade korduvaks täitmiseks; `for`-kordus sobib siis, kui korduste arv on ette teada.

Operaatorite kasutamist erinevatel eesmärkidel olenevalt andmete tüübist nimetatakse operaatorite ülelaadimiseks või polümorfismiks. Polümorfismi kasutatakse programmeerimiskeeltes enamasti selle tõttu, et operaatorite arv on piiratud ja võimalike operatsioonide arv ületab tunduvalt saadaolevate operaatorite arvu.

Hea tava on operaatorite ülelaadimisel anda neile "tavalisega" võimalikult sarnane tähenduse. Nii on loogiline, et `*` tähistab arvude puhul korrutamist ja teksti puhul selle kordamist, samas kui `+` tähistab arvude puhul liitmist ja tekstide puhul nende kokkukleepimist:

    print("a" + "b")

väljastab

    ab

## Kategooriad

- Algoritmid

# 12. Arvutustabel

Vaatame järgmist tabelarvutuse töölehte, mille mõnes lahtris on arvud ja mõnes valemid:

![](b12-body.png)

## Küsimus

Milline väärtus tuleb tabeli lahtrisse C3?

[Täisarv]

## Vastus

Õige vastus on: 5.

## Vastuse selgitus

Lahtri C3 arvutamiseks on vaja lahtrite C1 ja C2 väärtusi. C1 arvutamiseks on kõik andmed olemas, sinna tuleb 2+2=4. C2 arvutamiseks on vaja B3 väärtust. B3 väärtuseks saame 2*2=4, seejärel C2 väärtuseks 2+4=6 ja lõpuks C3 väärtuseks (4+6)/2=5.

Lahtri B2 väärtust meil vaja ei läinud ja selle võisime arvutamata jätta.

## See on informaatika!

Tabelarvutuse töölehtede valemid võimaldavad korduvaid arvutusi automatiseerida ning sellega kasutajate aega kokku hoida ja käsitsi arvutamisest tulenevate vigade arvu vähendada.

## Kategooriad

- Tabelitöötlus

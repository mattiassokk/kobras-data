# 14. Ahelkood

Ahelkood on kokkuhoidlik viis must-valgel pildil olevate kontuuride (piirjoonte) esitamiseks. Põhimõte on valida üks must piksel ja salvestada igal sammul suund liikumiseks järgmise musta pikslini kontuuri päripäeva (kellaosuti liikumise suunas) läbimisel.

Suunad on tähistatud numbritega selliselt:

![](b14-directions.png)

Näiteks kui valime alloleval pildil esimeseks sinise raamiga tähistatud piksli, saame edasi liikuda punaste nooltega näidatud suundades ja tulemuseks on ahelkood 1, 0, 0, 7, 7, 5, 5, 4, 3, 3, 2.

![](b14-example.png)

Vaatame nüüd sellist pilti:

![](b14-main-1.png)

## Küsimus

Milline järgmistest on selle pildi ahelkood?

[Raadionupud]

A. 1, 1, 1, 0, 7, 7, 7, 6, 5, 5, 5, 4, 3, 3, 3, 2

B. 1, 1, 0, 0, 7, 7, 6, 6, 5, 5, 4, 4, 3, 3, 2, 2

C. 6, 7, 7, 7, 0, 1, 1, 1, 2, 3, 3, 3, 4, 5, 5, 5

D. 0, 1, 1, 0, 7, 7, 7, 6, 5, 5, 5, 4, 3, 3, 3, 0

## Vastus

Õige vastus on: A.

## Vastuse selgitus

![](b14-main-2.png)

## See on informaatika!

Ahelkood on üks meetod objektide esitamiseks nende kontuuride abil. Ahelkood on määratud stardipiksli määramisega ja suunavektorite jadaga vastavalt sellele, kas igal sammul suundutakse kontuuri järgmise musta pikslini vasakul, paremal, üleval, all või diagonaalis. Ahelkoodi eeliseks on vajaliku info säilitamine ja samas andmemahu väga suur kokkuhoid. Esimesena esitles ahelkoodi ideed Freeman 1961. aastal ning seepärast on see tuntud ka kui Freemani ahelkood (ingl _Freeman Chain Code_, FCC).

Ahelkoodi üks kasutusala on numbrituvastus. Esiteks võimaldab see kood objekte kompaktselt esitada, teiseks on ahelkoodide võrdlemine lihtsam kui piltide võrdlemine. Lisaks sellele on ahelkoodist võimalik välja lugeda ka kujutatava objekti üksikuid omadusi. Ahelkoodid esindavad kadudeta tihendamist ja säilitavad kogu info, mis on vajalik kiireks ja efektiivseks mustrite analüüsiks.

## Kategooriad

- Info mõistmine
- Geomeetria
- Kodeerimine
- Varia

#!/bin/bash

python3 b14-directions.py >b14-directions.svg

#python3 b14-example.py 1 >b14-example-1.svg
#python3 b14-example.py 2 >b14-example-2.svg
python3 b14-example.py >b14-example.svg

python3 b14-main.py 1 >b14-main-1.svg
python3 b14-main.py 2 >b14-main-2.svg

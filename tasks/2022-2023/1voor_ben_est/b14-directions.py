scale = 100
margin = 0.2 * scale

dirs = [(+1, 0), (+1, -1), (0, -1), (-1, -1), (-1, 0), (-1, +1), (0, +1), (+1, +1)]

print(f'<svg width="{3 * scale + 2 * margin}" height="{3 * scale + 2 * margin}">')
print(f'  <defs>')
print(f'    <marker id="arrow" markerWidth="10" markerHeight="10" refX="9" refY="3" orient="auto" markerUnits="strokeWidth">')
print(f'      <path d="M0,0 L0,6 L9,3 z" fill="#00f" />')
print(f'    </marker>')
print(f'  </defs>')

print(f'')
for i, (dx, dy) in enumerate(dirs):
	print(f'  <rect x="{(1.0 + dx) * scale + margin}" y="{(1.0 + dy) * scale + margin}" width="{scale}" height="{scale}" stroke="#000" stroke-width="1" fill="none" />')

print(f'')
for i, (dx, dy) in enumerate(dirs):
	factor = 1.0 / (dx**2.0 + dy**2.0)**0.3
	print(f'  <line x1="{1.5 * scale + margin}" y1="{1.5 * scale + margin}" x2="{(1.5 + factor * dx) * scale + margin}" y2="{(1.5 + factor * dy) * scale + margin}" stroke="#00f" stroke-width="3" marker-end="url(#arrow)" />')

print(f'')
for i, (dx, dy) in enumerate(dirs):
	factor = 1.3 / (dx**2.0 + dy**2.0)**0.3
	print(f'  <text x="{(1.5 + factor * dx) * scale + margin}" y="{(1.5 + factor * dy) * scale + margin}" font-size="30" text-anchor="middle" dominant-baseline="middle">{i}</text>')

print(f'</svg>')

# 15. Metsapildid

Tanel seisis kaheksast puust ümbritsetud lagendikul sees ja tegi puudest 360-kraadise foto.

![](b15-body.png)

Mõne päeva pärast tuli Tanel samasse kohta tagasi ja nägi, et kaks puud oli vahepeal maha saetud. Ta tegi sarnase foto, aga alustas oma 360-kraadist ringi esimese korraga võrreldes teisest suunast.

## Küsimus

Millise foto allolevatest tegi Tanel teisel korral?

[Raadionupud]

A. ![](b15-option-a.png)

B. ![](b15-option-b.png)

C. ![](b15-option-c.png)

D. ![](b15-option-d.png)

## Vastus

Õige vastus on: C.

## Vastuse selgitus

Kui me nummerdame esimesel fotol olevad puud, siis saame sellise pildi:

![](b15-explanation-1.png)

Kui me muudaksime natuke vaatenurka ja teeksime uue foto, saaksime pildi, kus puud oleksid küll samas järjekorras, aga nihkes. Ka nüüd nummerdame fotol olevad puud, aga nii, et mõlemal fotol vastavad samad numbrid samadele puudele.

![](b15-explanation-2.png)

Nüüd kontrollime, kas kõik puud allolevatel fotodel on nummerdatud samal viisil ja on õiges järjekorras.

Variandi A puhul on maha lõigatud puud numbritega 1 ja 5, kuid puu numbriga 4 on teistsuguse kujuga kui esimesel pildil.

![](b15-option-a-error.png)

Pildil B on maha saetud puud numbritega 6 ja 3, kuid puu numbriga 1 on erineva kujuga võrreldes esimese pildiga.

![](b15-option-b-error.png)

Kui me võrdleme varianti C, näeme, et puud 4 ja 3 on maha saetud ja ülejäänud on õiges järjekorras.

![](b15-option-c-ok.png)

Variandi D puhul on langetatud puud numbritega 1 ja 6, kuid siin on nii 4. kui 5. puu erineva kujuga.

![](b15-option-d-error.png)

## See on informaatika!

Ülesande lahendamiseks on vaja leida puude järjekorda iseloomustav muster ja proovida seda vastusevariantide peal. Oskus tuvastada mustreid lubab meil leida vigu jadas. Momendil on oluline leida puude järjekord. Kui katsetame saadud mustrit erinevate variantide peal ja leiame selles erinevuse, nimetame me seda veaks. Seda protsessi nimetatakse vigade tuvastamiseks, millel on tähtis osa programmide testimisel. Kui osa infost puudub (nagu antud näites oli osa puid maha saetud), on vigade tuvastamine keerulisem.

## Kategooriad

- Info mõistmine
- Varia

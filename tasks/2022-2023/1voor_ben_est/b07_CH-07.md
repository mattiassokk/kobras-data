# 7. Kevadlilled

Jaanika istutas peenrale vasakult paremale seitse lille. Ta valis lilli vastavalt järgmistele reeglitele:

* Kõige vasakpoolsemasse auku võib ta istutada suvalise lille.

* Igasse järgmisse auku võib istutada ainult sellise lille, mille puhul on alloleval skeemil nool eelmisena istutatud lille juurest järgmisena istutatava lille juurde.

![](b07-body.png)

Näiteks võib Jaanika istutada tulbi ja selle kõrvale nartsissi, kuna skeemil on nool tulbi juurest nartsissi juurde. Vastupidine variant pole lubatud, kuna nartsissist tulbini viivat noolt ei ole.

## Küsimus

Milline järgnevatest peenardest **pole loodud** vastavalt Jaanika reeglitele?

[Raadionupud]

A. ![](b07-answer-a.png)

B. ![](b07-answer-b.png)

C. ![](b07-answer-c.png)

D. ![](b07-answer-d.png)

## Vastus

Õige vastus on: D.

## Vastuse selgitus

Nummerdame nooled Jaanika skeemil selliselt:

![](b07-arrows.png)

Peenra A saab istutada, alustades tulbist ja liikudes mööda nooli 1, 3, 4, 2, 7 ja 7.

Peenra B saab istutada, alustades nartsissist ja liikudes mööda nooli 3, 4, 3, 5, 1 ja 3.

Peenra C saab istutada, alustades tulbist ja liikudes mööda nooli 1, 3, 5, 1, 2 ja 7.

Peenra D puhul võime alustada lumikellukesest ja liikuda edasi mööda nooli 4 ja 2. Aga kannikesest tulbini noolt pole ja seepärast seda peenart reeglipäraselt lõpetada ei saa.

![](b07-explanation.png)

Veelgi enam: kui Jaanika istutab kasvõi ühe kannikese, peab ta kõik järgmised istutusaugud kannikestega täitma, sest skeemil ei lähe noolt kannikese juurest ühegi teise lille juurde.

## See on informaatika!

Jaanika plaan meenutab arvutiteadusest tuntud olekumasinaid (ingl _state machine_). Selline masin on alati mingis olekus ja masina kirjeldus määrab, millistesse teistesse olekutesse igast olekust edasi liikuda võib. Jaanika reeglite kohaselt on viimasena istutatud lill peenra olek. Kuna tema skeem kirjeldab, milliseid lilli tohib iga lille järele istutada, määrabki see lubatud üleminekud olekute vahel.

Selliseid olekumasinaid (mõned lihtsamate, mõned keerulisemate reeglitega) kasutatakse arvutiteaduses sageli. Üks levinud rakendus on sisestatud andmete korrektsuse kontrollimine. Näiteks võib tabelarvutusprogramm kasutada olekumasinat, et kontrollida, kas kasutaja sisestatud tekst on arv või korrektne valem.

Teine näide on e-posti aadressi vormi kontroll: kas selles on olemas @-märk, ega kasutajanimes või domeeninimes pole lubamatuid märke, jne. Samas tasub tähele panna, et olekumasinaga saab kontrollida ainult aadressi vormilist korrektsust. Seda, kas aadress ka päriselt olemas on, tuleb küsida selle domeeni meiliserverilt.

## Kategooriad

- Info mõistmine

# 13. Soovitussüsteem

Interneti riidepood kasutab soovituste süsteemi, mis põhineb klientide isikuandmetel. Näiteks kui klient soovib jalatseid osta, järgib programm järgmisel joonisel toodud reeglit ja soovitab saapaid:

![](j13-body1.png)

![](j13-body2.png)

Ühel päeval kustutati osa rõivasoovituse reeglitest kogemata ära. Allolev joonis näitab reeglite allesjäänud osa.

![](j13-body3.png)

Õnneks on alles ka mõned varem antud soovitused:

| Klient | Soovitus |    |
| ------ | -------- | -- |
| 18-aastane<br/>140 cm | Ühevärviline seelik | ![](j13-body4.png) |
| 32-aastane<br/>145 cm | Kirju seelik | ![](j13-body5.png) |
| 28-aastane<br/>155 cm | Kirjud püksid | ![](j13-body6.png) |
| 15-aastane<br/>160 cm | Ühevärvilised püksid | ![](j13-body7.png) |
| 10-aastane<br/>152 cm | Ühevärvilised püksid | ![](j13-body7.png) |

## Küsimus

Milline võis ülaltoodud andmete põhjal olla joonise puuduv osa?

[Raadionupud]

A. ![](j13-option-a.png)

B. ![](j13-option-b.png)

C. ![](j13-option-c.png)

D. ![](j13-option-d.png)

## Vastus

Õige vastus on: B.

## Vastuse selgitus

Esimene tingimus kontrollib kliendi pikkust, seega peame arvestama ainult vähemalt 150 cm pikkuste klientidega:

| Klient | Soovitus |    |
| ------ | -------- | -- |
| 28-aastane<br/>155 cm | Kirjud püksid | ![](j13-body6.png) |
| 15-aastane<br/>160 cm | Ühevärvilised püksid | ![](j13-body7.png) |
| 10-aastane<br/>152 cm | Ühevärvilised püksid | ![](j13-body7.png) |

Tabelist näeme, et kõigil neil klientidel soovitati osta pükse, seetõttu on variant D vale.

Kuna kõigile alla 20-aastastele klientidele soovitati ühevärvilisi pükse, on variant A vale.

Kuna kõigile 20-aastastele ja vanematele klientidele soovitati kirjusid pükse, on variant C vale.

Näeme, et pikemad kliendid said vastavalt vanusele erinevaid püksisoovitusi, seetõttu peaks täielik joonis välja nägema selline:

![](j13-sol.png)

## See on informaatika!

Joonist, mida programm selles ülesandes järgib, nimetatakse otsustuspuuks (ingl _decision tree_). See on vooskeemi (ingl _flowchart_) lihtsam erijuht.

Informaatikas saab vooskeemi kasutada algoritmi, töövoo või protsessi esitamiseks. Vooskeem koosneb erinevatest nooltega ühendatud kujunditest. Kujundid tähistavad erinevat tüüpi toiminguid ja nooled tähistavad nende toimingute sooritamise järjekorda.

Vooskeeme ja otsustuspuid kasutatakse sageli ka mitmesuguste tegevusjuhiste esitamiseks inimestele, näiteks hädaolukorra lahendamiseks vastavalt olukorra asjaoludele või selleks, et selgitada, kuidas mingeid plaane tuleks ellu viia.

## Kategooriad

- Loogika
- Algoritmid

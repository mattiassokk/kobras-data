# 6. Sahver

Kopra-Mati leidis oma toidu jaoks kaks head peidukohta. Meelespidamiseks soovib ta need kaardil ristidega märkida. Aga kui tema rivaal Kati kaardi leiaks, teaks ta, kust otsida!

![](j06-body1.png)

Kati segadusse ajamiseks lisab Mati kaardi teistesse ruutudesse mõned juhuslikud ristid, jälgides, et ristide koguarv igas reas ja veerus oleks paarisarv (tuletame meelde, et ka 0 on paarisarv). Seejärel kustutab ta need kaks risti, mis näitasid tema peidukohti. Valminud kaart on selline:

![](j06-body2.png)

## Küsimus

Millistes ruutudes asuvad Mati toiduvarud?

(Kirjuta kahe ruudu tähised, näiteks A1 B2.)

[Tekstikast]

## Vastus

Õige vastus on: C3 E6.

## Vastuse selgitus

Siin on kahe peidukoha asukohad:

![](j06-solution1.png)

Nende leidmiseks vaatame lõplikku kaarti ja leiame, et seal on kaks rida ja kaks veergu, kus ristide arv pole paaris, nendeks on read 3 ja 6 ning veerud C ja E. Järgneval joonisel on need read ja veerud tähistatud kollase värviga:

![](j06-solution2.png)

Kaks kustutatud risti peavad olema niisiis nendes ridades ja veergudes, me peame leidma viisi, kuidas need uuesti lisada, et taastada paarisarvulisuse omadus.

Kollaste ridade ja veergude ristumiskohti on neli. Neist ülemine parempoolne (E3) on juba tähistatud ristiga, seega ei saa see olla peidukohaks: teame, et Mati on tegelikke peidukohti tähistavad ristid kustutanud. Nii peame selleks, et taastada paarisarv riste real 3, lisama risti ülemisse vasakpoolsesse ristumisruutu ehk ruutu C3. Nüüd teame ühte peidukohta.

Teise peidukoha rist ei saa olla alumises vasakpoolses ristumisruudus ehk ruudus C6, kuna tulemuseks oleks 3 risti veerus C ja 3 pole paarisarv. Ainus valik on alumine parempoolne ristumisruut ehk E6. Nii saamegi ülaltoodud vastuse.

Siin on kaart, nagu ta oli enne ristide kustutamist, ja tõesti on paarisarv riste igas reas ja igas veerus:

![](j06-solution3.png)

## See on informaatika!

Mati kasutab siin võtet, mis on seotud arvutiteaduses sageli tarvitatava paarsusbiti mõistega. See on osa suuremast hulgast võtetest, mis on tuntud vigade tuvastamise ja parandamise meetoditena. Idee seisneb selles, et kui me peame salvestama või edastama andmeid bittide (ühtede ja nullide) jadana, lisame jadale täiendavaid bitte, mis aitavad meil tuvastada, kas edastusel või salvestamisel on tekkinud vigu, näiteks selliseid, kus saadeti 1, aga vastu võeti 0.

Kui kasutame lihtsat veatuvastuskoodi, võime lisada paarsusbiti, nii et ühtede arv oleks paaris. Nii lisame jadale "0110101" nulli, et saada jada "01101010" ja ühtede arv jadas jääks paarisarvuks. Kui näiteks teine bitt võeti valesti vastu ning saadi hoopis jada "00101010", siis vastuvõetud sõnum ei vasta meie paarsusnõudele (jadas on paaritu arv ühtesid). Paneme tähele, et kui vigaseid bitte on rohkem kui üks, ei pruugi selline lihtne meetod probleemi tuvastada.

## Kategooriad

- Info mõistmine
- Diskreetne matemaatika
- Kodeerimine

import peg from 'pegjs';
import {readFileSync} from "fs";


const questionGrammar = readFileSync('question-grammar.peg', 'utf8');
const questionParser = peg.generate(questionGrammar)

export function parseQuestion(text) {
    try {
        return questionParser.parse(text);
    } catch (e) {
        return {error: errorToText(text, e)};
    }
}

export function parseAnswer(questionType, answerText) {
    const correct = [];
    const answerPrefix = "(?:Õige(?: vastus)?(?: on)?:?|(?:Верный|Верные|Правильные|Правильный) (?:ответы|ответ))?:?.*?";
    if (questionType === 'integer' || questionType === 'integerRange') {
        correct.push(Number(answerText.match(/-?\d+/)[0]));
    } else if (questionType === 'radio') {
        const regex = new RegExp(`${answerPrefix}(?:(?: või|или )?([A-Z]))+`, 'gm')
        let matches;
        while ((matches = regex.exec(answerText)) != null) {
            if (matches.length >= 2) {
                correct.push(matches[1]);
            }
        }
    } else if (questionType === 'text') {
        const regex = new RegExp(`${answerPrefix}(?:(?: või|или )?\`(?:(.*?))\`)`, 'g');
        let matches;
        while ((matches = regex.exec(answerText)) != null) {
            if (matches.length >= 2) {
                correct.push(matches[1].trim());
            }
        }
    } else if (questionType === 'checkbox') {
        const regex = new RegExp(answerPrefix + "((?:[A-Z]?\\s?(?:,|ja|и)?)*)");
        const matches = regex.exec(answerText);
        if (matches && matches.length >= 2) {
            correct.push(matches[1].split(/[,(ja|и)\s]+/).filter(s => s));
        }
    } else if (questionType === 'match') {
        let answer = {};
        const regex = new RegExp(`${answerPrefix}(.*)`, "gms");
        const matches = regex.exec(answerText);
        if (matches && matches.length >= 2) {
            const pairs = matches[1].trim().split(/[\n\t\r]/).filter(v => v && v.trim());
            for (const pair of pairs) {
                const pairValues = pair.split('<->');
                answer[pairValues[0].trim()] = pairValues[1].trim();
            }
        }
        correct.push(answer);
    }
    return correct;
}

export function parseCategories(categoriesText) {
    return categoriesText.split(/[,\-\n]/g).filter(v => v).map(v => v.trim());
}


function errorToText(questionText, error) {
    const location = error.location;
    const questionLines = questionText.replaceAll("\r\n", "\n").split("\n");
    let errorMsg = `Viga küsimuse ${location.start.line-1}${location.end.line !== location.start.line ? " - " + location.end.line-1 : ""} real. ${error.message} \r\n`;
    for (let line = 0; line < questionLines.length; line++) {
        let lineText = `${line}.`;
        errorMsg += lineText + "\t\t" + questionLines[line] + "\r\n";
        let errorPointer = " ".repeat(lineText.length) + "\t\t";
        if (line === location.start.line-1 && line === location.end.line-1) {
            errorPointer += " ".repeat(location.start.column-1) + "^".repeat(location.end.column - location.start.column) + " ".repeat(questionLines[line].length - location.end.column);
        } else if (line === location.start.line-1) {
            errorPointer += " ".repeat(location.start.column-1) + "^".repeat(questionLines[line].length - (location.start.column-1));
        } else if (line === location.end.line-1) {
            errorPointer += "^".repeat(location.start.column-1) + " ".repeat(questionLines[line].length - (location.start-1));
        } else if (line < location.end.line-1 && line > location.start.line-1) {
            errorPointer += "^".repeat(questionLines[line].length)
        }
        if (line <= location.end.line-1 && line >= location.start.line-1) {
            errorMsg += errorPointer + "\r\n";
        }
    }
    return errorMsg;
}
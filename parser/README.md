# Andmete parsimine

Selles kataloogis olevat koodi kasutatakse lähteandmete parsimiseks masinloetavale kujule, et seda viktoriini veebirakenduses kuvada.
Andmete parsimiseks on kasutatud JavaScripti ja Gulp.js teeki. Gulp.js võimaldab valida failid, neid töödelda ning lisada seejärel teise kausta.
Andmete töötlemine on jaotatud praegu viite suuremasse plokki:

1. Esilehe andmete avalikustamine (kõik `web` kaustas leiduvad failid)
2. Tulemuste parsimine, indekseerimine ja avalikustamine
    - Tulemusefaili nimest andmete kogumine (+ valitud väljade indekseerimine)
    - Tulemuste parsimine (kasutades neat-csv teeki)
    - Tulemuste avalikustamine
3. Ülesannete parsimine, indekseerimine ja avalikustamine
    - Ülesandefaili nimest andmete kogumine (+ valitud väljade indekseerimine)
    - Ülesande parsimine (failis `parsing.js`, ülesande küsimuse parsimiseks kasutatud PEG.js grammatikat ja muude andmete jaoks tekstitöötlust ja regulaaravaldisi) MD-failist struktueeritud JSON-formaati ja valitud väljade indekseerimine
    - Ülesannete avalikustamine
4. Ülesannetega seotud piltide avalikustamine
5. Indekseeritud tulemuste ja ülesannete avalikustamine


# Lokaalses arvutis käivitamine

**Nõuded:** Node.js (v16.14.0) ja NPM (v8.9.0) olemasolu arvutis (sulgudes testitud versioonid).

* Lae repositoorium alla
* Ava kataloog `parser`
* Käivita kataloogis olles käsurealt käsk `npm install` ja oota, kuni kõik teegid on laetud.
* Käivita samas kataloogis käsurealt käsk `npm run`.
* Repositooriumi juurkataloogi tekib uus kataloog `public`, mis sisaldab töödeldud andmeid.

Järgnevad punktid on vajalikud, kui soovid käivitada lokaalselt [viktoriini veebirakendust](https://gitlab.com/mattiassokk/kobras-data) ja testida lokaalsete andmete vastu.

* Installeeri enda arvutisse http-server teek käsuga `npm install --global http-server`
* Mine kataloogi `public` ja käivita käsurealt `http-server -p 8080`
* Lokaalselt veebirakenduse käivitades peaks see kuvama lokaalselt paritud faile.


## GitLabi lehele andmete paigutamine

Repositooriumi muudatuste korral käivitatakse automaatselt CI/CD töö käigus siia kausta paigutatud Gulp.js skript. CI/CD töö nimeks on `pages`, mis avalikustab töö käigus kõik `public` kausta lisatud failid GitLabi staatilise veebilehena. Varasemaid CI/CD käigus käivitatud töid saab vaadata [siin](https://gitlab.com/mattiassokk/kobras-data/-/jobs). Seal saab uurida kindla töö logi (vajutades loendis nime `pages`) ja ühtlasi laadida töö käigus lehele paigutatud sisu (loendi lõpus allalaadimise ikoon).

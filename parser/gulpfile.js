import {writeFile} from "fs";
import gulp from 'gulp';
import rename from 'gulp-rename';
import change from 'gulp-change';
import neatCsv from 'neat-csv';
import {parseQuestion, parseAnswer, parseCategories} from "./parsing.js";


let taskCounter = 1;
const index = {'est': {}, 'rus': {}};
const matchingTasksIndex = {'est': {}, 'rus': {}};
const taskIds = {};

let currentTask;
let currentTaskLang;

let resultCounter = 1;
const resultIndex = {};

function addResultPositions(file) {
    let results = JSON.parse(file);
    results.forEach((result) => {
        delete Object.assign(result, {name: result.Nimi}).Nimi;
        delete Object.assign(result, {school: result.Kool}).Kool;
        delete Object.assign(result, {grade: parseInt(result.Klass, 10)}).Klass;
        delete Object.assign(result, {points: parseInt(result.Punkte, 10)}).Punkte;
        delete Object.assign(result, {notes: result.Lisainfo}).Lisainfo;
    });
    results.sort((a, b) => a - b);

    let ix = 1;
    let points = Math.pow(10, 1000);
    let place = 0;
    for (const result of results) {
        if (points > result.points) {
            points = result.points;
            place = ix;
        }
        result.place = place;
        ix++;
    }

    return JSON.stringify(results);
}

function processTaskFile(file) {
    const title = file.match(/#\s*\d*\.?\s*(.*)\r?\n/)[1];
    const allBlocks = file.substring(file.indexOf("\n")).split(/\n\s*##\s+/);
    const jsonTask = {
        title: title,
        description: allBlocks.shift(),
        questions: [],
        answers: [],
        categories: []
    }
    for (const block of allBlocks) {
        if (block.startsWith("Küsimus") || block.startsWith("Вопрос")) {
            const questionText = block.substring(block.indexOf("\n")).trim();
            const parsedQuestion = parseQuestion(questionText);
            if (!parsedQuestion.type) {
                parsedQuestion.type = 'invalid';
                parsedQuestion.question = questionText;
                console.error(`Ülesande "${title}" (üleanne ${currentTask.no} kaustas tasks/${currentTask.year}/${currentTask.round}voor_${currentTask.ageCat}_${currentTaskLang}/) parsimine ebaõnnestus, sest küsimus ei vasta kirjeldatud grammatikale.\r\n${parsedQuestion.error}`)
            }
            const currentQuestions = jsonTask["questions"];
            currentQuestions.push(parsedQuestion);

        } else if (block.startsWith("Vastuse selgitus") || block.startsWith("Объяснение ответа")) {
            jsonTask["explanation"] = block.substring(block.indexOf("\n")).trim();
            if (jsonTask["explanation"].toLowerCase().includes("todo")) {
                delete jsonTask["explanation"];
            }
        } else if (block.startsWith("Vastus") || block.startsWith("Ответ")) {
            const answerText = block.substring(block.indexOf("\n")).trim();
            const currentQuestions = jsonTask["questions"];
            const answerType = currentQuestions[jsonTask.answers.length].type;
            let correct = parseAnswer(answerType, answerText);
            currentQuestions[jsonTask.answers.length]["answer"] = correct;
            jsonTask.answers.push(correct);
        } else if (block.startsWith("See on informaatika") || block.startsWith("Это информатика")) {
            jsonTask["informatics"] = block.substring(block.indexOf("\n")).trim();
            if (jsonTask["informatics"].toLowerCase().includes("todo")) {
                delete jsonTask["informatics"];
            }
        } else if (block.startsWith("Kategooriad") || block.startsWith("Категории")) {
            const categoriesText = block.substring(block.indexOf("\n")).trim();
            jsonTask["categories"] = parseCategories(categoriesText);
        }
    }

    currentTask.title = jsonTask.title;
    currentTask.categories = jsonTask.categories;
    if (currentTask.sourceId != null && `${currentTask.year}-${currentTask.round}-${currentTask.sourceId}` in matchingTasksIndex[currentTaskLang]) {
        const ob = matchingTasksIndex[currentTaskLang][`${currentTask.year}-${currentTask.round}-${currentTask.sourceId}`]
        let ageCats = ob["ageCat"];
        ageCats.push(currentTask.ageCat[0]);
        let taskNos = ob["no"];
        taskNos.push(currentTask.no[0]);
    } else if (currentTask.sourceId != null) {
        matchingTasksIndex[currentTaskLang][`${currentTask.year}-${currentTask.round}-${currentTask.sourceId}`] = currentTask;
    }

    delete jsonTask.answers;

    let taskId = taskIds[[currentTask.year, currentTask.round, currentTask.ageCat, currentTask.no]];
    if (!taskId) {
        taskCounter++;
        taskIds[[currentTask.year, currentTask.round, currentTask.ageCat, currentTask.no]] = taskCounter;
        taskId = taskCounter;
    }

    let imagePath = index[currentTaskLang][taskId].path.split("/");
    imagePath.pop();
    imagePath = imagePath.join("/");

    return JSON.stringify(jsonTask)
        .replaceAll(`![](`, `![](${imagePath}/`)
        .replaceAll(`img src=\\\"`, `img src=\\\"${imagePath}/`);
}

function newTasksPath(path) {
    const dirs = path.dirname.split(/[\/\\]/);
    const leafDirInfo = dirs[dirs.length - 1].split("_");
    const year = dirs[dirs.length - 2];
    const round = Number(leafDirInfo[0].match(/\d+/g)[0]);
    const ageCat = leafDirInfo[1];
    const lang = leafDirInfo[2];

    path.dirname = `tasks/${year}/${round}/${ageCat}/${lang}`;

    if (path.extname === '.md') {
        const fileNameInfo = path.basename.split("_");
        const taskNo = Number(fileNameInfo[0].match(/\d+/g)[0]);

        let sourceCountry = null;
        let sourceId = null;
        if (fileNameInfo.length > 1) {
            sourceId = fileNameInfo[1];
            sourceCountry = fileNameInfo[1].split(/[-]/)[0].toLowerCase();
        }

        path.basename = taskNo.toString(10);
        path.extname = `.json`

        let taskId = taskIds[[year, round, ageCat, taskNo]];
        if (!taskId) {
            taskCounter++;
            taskIds[[year, round, ageCat, taskNo]] = taskCounter;
            taskId = taskCounter;
        }

        index[lang][taskId] = {
            id: taskId,
            no: [taskNo],
            round: round,
            year: year,
            ageCat: [ageCat],
            country: sourceCountry,
            path: `${path.dirname}/${path.basename}${path.extname}`,
            sourceId: sourceId
        }

        currentTask = index[lang][taskId];
        currentTaskLang = lang;
    }
}

async function parseCsv(content, done) {
    content = JSON.stringify(await neatCsv(content));
    done(null, content);
}

function processResultFile(path) {
    const dirs = path.dirname.split(/[\/\\]/);
    const year = dirs[dirs.length - 1];
    const fileName = path.basename;
    const fileNameTokens = fileName.split("_");

    const round = Number(fileNameTokens[0].match(/\d+/g)[0]);
    const ageCat = fileNameTokens[1];

    path.basename = ageCat;
    path.dirname = `results/${year}/${round}`;
    path.extname = '.json';
    resultIndex[resultCounter] = {
        id: resultCounter,
        round: round,
        year: year,
        ageCat: ageCat,
        path: `${path.dirname}/${path.basename}${path.extname}`
    }
    resultCounter++;
}

gulp.task("publish_indexes", function () {
    return new Promise(function (resolve) {
        for (const lang of Object.keys(matchingTasksIndex)) {
            writeFile(`../public/tasks/index-${lang}.json`, JSON.stringify(Object.values(matchingTasksIndex[lang])), 'utf8',
                (err) => {
                    new Error(`Creating tasks index file failed! ${err}`)
                }
            );
        }
        writeFile(`../public/results/index.json`, JSON.stringify(Object.values(resultIndex)), 'utf8',
            (err) => {
                new Error(`Creating results index file failed! ${err}`)
            }
        );
        resolve();
    });
});

gulp.task("publish_images", function () {
    return gulp.src(['../tasks/**/*.{png,gif,jpg,svg}',
        '!../tasks/**/.*.{png,gif,jpg,svg}',
        '!../tasks/**/.*/*.{png,gif,jpg,svg}',
        '!../tasks/**/.*/*/*.{png,gif,jpg,svg}'])
        .pipe(rename(path => {
            newTasksPath(path)
        }))
        .pipe(gulp.dest("../public/"));
});

gulp.task("publish_results", function () {
    return gulp.src("../results/**/*.csv")
        .pipe(rename(path => {
            processResultFile(path)
        }))
        .pipe(change(parseCsv))
        .pipe(change(addResultPositions))
        .pipe(gulp.dest("../public/"));
});

gulp.task("publish_tasks", function () {
    return gulp.src(['../tasks/*/*/*.md',
        '!../tasks/.*/*/*.md',
        '!../tasks/*/.*/*.md',
        '!../tasks/*/*/.*.md']
    )
        .pipe(rename(path => {
            newTasksPath(path)
        }))
        .pipe(change(processTaskFile))
        .pipe(gulp.dest("../public/"));
});

gulp.task("publish_pages", function () {
    return gulp.src("../web/**").pipe(gulp.dest("../public/"));
});

gulp.task('default',
    gulp.series(
        'publish_pages',
        'publish_results',
        'publish_tasks',
        'publish_images',
        'publish_indexes'
    )
);

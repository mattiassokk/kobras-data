# Informaatikaviktoriini Kobras andmed (koopia)

See repositoorium on loodud bakalaureusetöö "Veebirakenduse loomine informaatikaviktoriinile Kobras" raames, mille autor on Mattias Sokk.
Repositoorium sisaldab endas kõiki varasemaid viktoriiniülesandeid õppeaastast 2017/18, mis on vormistatud kokkulepitud struktuuriga Markdown ([sissejuhatus](https://daringfireball.net/projects/markdown/), [süntaks](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet/)) vormingut kasutades. Lisaks ülesannetele, hoiame repositooriumis varasemate viktoriinitulemuste ja Kobrase veebirakenduse esilehe sisu.

Repositoorium on kopeeritud versioon olemasolevast repositooriumist, kuid siia on lisatud tulemuste ja Kobrase rakenduse esilehe andmete talletamine. Bakalaureusetöö raames lisati repositooriumisse ka andmete parsimise ja nende automaatse avalikustamise loogika. Parsimisest saab lähemalt lugeda kataloogis "parsimine". Parsimise loogika kirjutamise käigus muudeti algseid ülesandefaile.


## Esileht

Valminud veebirakenduse esilehe muutmiseks on loodud kataloog `web`. Selles kataloogis saab esilehel kuvatavat infot muuta failis `home-ee.md` (eesti keeles) ja `home-ru.md` (vene keeles). Markdown faile võib vormistada vastavalt Markdowni reeglitele ja kõik esilehel kuvatavad pildifailid tuleks lisada samuti `web` kataloogi.

## Ülesanded

Ülesanded on paigutatud kataloogi `tasks` alamkataloogidesse õppeaastate (`2017-2018`, `2018-2019`, ...), rühmade ja keelte kaupa (`1voor_ben_est`, `1voor_ben_rus`, `1voor_jun_est`, ...). Igas alamkataloogis nimetame failid ülesannete kaupa (`s01_CA-04.md`, `s02_EE-01.md`, ...). Failinime esitäht viitab vanuserühmale, sellele järgnev arv ülesandenumbrile, tähekood lähteriigile ning viimane arv Bebras rahvusvahelise repositooriumi vastava riigi ülesandele. Iga ülesande juurde kuuluvad abifailid (näiteks joonised) nimetame sama prefiksiga (`s01-a.png`, `s01-b.png`, ...). Kuna eesti- ja venekeelsed materjalid on eraldi kataloogides, siis failinimedes keelt kajastama ei pea. Ülesanded on kirja pandud kindla formaadiga ja kasutusel on viis erinevat vastusetüüpi.

Ülesandes võib esineda ka mitu küsimust, kuid siis peab enne uue küsimuse plokki olema kirjutama eelneva küsimuse vastuse ploki (näide: [eesti keeles](/tasks/2017-2018/1voor_sen_est/s11.md), [vene keeles](/tasks/2017-2018/1voor_sen_rus/s11.md)). Järgnevalt on loetletud erinevad vastusetüübid ning nende kokkulepitud tähistamise viis. Kindlast tähistusest kinni pidades on võimalik lahendatavaid ülesandeid kuvada korrektselt uuel viktoriini veebilehel.


### Täisarv / täisarvuvahemik

Täisarvu vastusetüüp tähistatakse lisades peale küsimust `[Täisarv]`. Soovi korral võib täpsustada ka arvu alam- ja ülempiiri, nt arv vahemikus 0-10: `[Täisarv [0, 10]]`.
Esitatud vastuses loetakse vastuseks esimene arv. (Näide: [eesti keeles](/tasks/2018-2019/1voor_sen_est/s09_CZ-08c.md), [vene keeles](/tasks/2018-2019/1voor_sen_rus/s09_CZ-08c.md)).


### Tekstikast

Tekstikasti vastusetüüp tähistatakse lisades peale küsimust `[Tekstikast]`. Esitatud vastuses peab olema õige vastus märkide ` vahel. (Näide: [eesti keeles](/tasks/2017-2018/1voor_ben_est/b06.md), [vene keeles](/tasks/2017-2018/1voor_ben_rus/b06.md))


### Raadionupud

Raadionuppude vastusetüüp tähistatakse lisades peale küsimust `[Raadionupud]`. Kõik võimalikud raadionupud peab järjestama peale märget kujul:

```
A. Vastusevariant A
B. Vastusevariant B
C. Vastusevariant C
```

Õige vastusevariandina märgitakse esimene vastuse täht. (Näide: [eesti keeles](/tasks/2017-2018/1voor_ben_est/b11.md), [vene keeles](/tasks/2017-2018/1voor_ben_rus/b11.md))


### Märkeruudud
Raadionuppude vastusetüüp tähistatakse lisades peale küsimust `[Märkeruudud]`. Kõik võimalikud märkeruudud peab järjestama peale märget kujul:

```
A. Vastusevariant A
B. Vastusevariant B
C. Vastusevariant C
```

Vastuses tuleb esitada loeteluna õigete märkeruutude tähed. (Näide: [eesti keeles](/tasks/2019-2020/1voor_sen_est/s11_US-02.md), [vene keeles](/tasks/2019-2020/1voor_sen_rus/s11_US-02.md))


### Vastavus
Vastavusse seadmise vastusetüüpi tähistatakse lisades peale küsimust `[Vastavus]`. Kõik väärtused ja neile vastavad vastusevariandid tuleb seejärel järgnevalt kirja panna:
```
A. Esimene valik <-> valik 1 | valik 2 | valik 3
B. Teine valik <-> valik 1 | valik 2 | valik 9
```
Iga valiku ette tuleb lisada täht, selle järele valiku nimetus ning peale `<->` erinevad valikuvariandid märgiga `|` eraldatult. Nagu näitest näha, ei pea iga valiku vastusevariandid kattuma. Vastus peab olema formaadis:

```
A <-> valik 2
B <-> valik 9
```

Näide: [eesti keeles](/tasks/2018-2019/2voor_sen_est/s08_HU-08.md), [vene keeles](/tasks/2018-2019/2voor_sen_rus/s08_HU-08.md)


## Tulemused

Tulemused on paigutatud kataloogi `results` alamkataloogidesse õppeaastate (`2017-2018`, `2018-2019`, ...) järgi. Igas sellises alamkataloogis on tulemused CSV-failidesse paigutatud (`1voor_ben.csv`, `1voor_jun.csv`, `1voor_sen.csv`, `2voor_ben.csv`, ...). Nagu failinimest oletada võib, on ühte faili paigutatud ainult ühe vooru ja vanuserühma andmed. Selliselt kataloogidesse paigutatud CSV-failid peavad olema kindlas formaadis, kus on päised `Nimi`, `Kool`, `Klass`, `Punkte`, `Õpetaja` ja `Lisainfo` ja neile vastavad read viktoriinil osalenud õpilaste andmetega. CSV-fail eraldusmärgiks on `,` ja fail peaks olema sarnase struktuuriga:

```
Nimi,Kool,Klass,Punkte,Õpetaja,Lisainfo
Heli Kopter,Tallinna Reaalkool,10.a klass,58,Tiit Sei,kutsutud lõppvooru
Elis Tama,Tallinna Reaalkool,10.a klass,58,Tiit Sei,
Nina Sarvik,Kärdla Põhikool,9.a klass,56,Mann A. Vaht,osales mitu korda
```

Korrektne [näitefail](/results/2021-2022/2voor_sen.csv).

## Andmete parsimine ja avalikustamine

Uues repositooriumis ülesandeid muutes käivitub automaatselt pideva integratsiooni/tarne (CI/CD) töö (konfigureeritud failis `.gitlab-ci.yml`), mis teisendab lähtefailid struktuueritud kujule (JSON) ja teeb need GitLabi staatilisel veebilehel avalikuks. Lehekülje saab seada avalikuks ka privaatse repositooriumi korral. Neid avalikke faile on seadistatud kasutama viktoriini veebirakendus ([link](https://mattiassokk.gitlab.io/kobras-demo/), [repositoorium](https://gitlab.com/mattiassokk/kobras-demo)). Kuna repositooriumisse luuakse ülesandeid ka tulevaste viktoriinivoorude jaoks, ei tohi kõiki andmeid avalikustada. Selleks, et tulevasi ülesandeid või privaatseid faile indekseerimisest ja avalikustamisest kõrvale jätta, saab failide ja kataloogide ette lisada punkti (`.`). Niimoodi märgistatud failid ja kataloogid jäävad parsimise ja avalikustamise loogikast kõrvale. Avalikustamise vältimiseks on võimalik uusi ülesandeid luua eraldi Giti harus ja peale viktoriini toimumist need repositooriumi põhiharusse lisada, sest CI/CD töö on seadistatud ainult põhiharul töötama. Rohkem infot andmete parsimise kohta [parsimise kataloogis](/parser).

Avalikustatud andmete indeksfailid:
Ülesanded: [eesti keeles](https://mattiassokk.gitlab.io/kobras-data/tasks/index-est.json), [vene keeles](https://mattiassokk.gitlab.io/kobras-data/tasks/index-rus.json)
[Tulemused](https://mattiassokk.gitlab.io/kobras-data/results/index.json)

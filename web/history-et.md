<div style="float: left; padding: 10px">
  <img src="logo.png" alt="Viktoriini Kobras logo" /><br />
</div>

#### Ajalugu

Esimene kord üldhariduskoolide õpilastele mõeldud informaatikaviktoriin toimus 2004. a. Leedus. Eesti liitus selle initsiatiiviga 2006. aastal. Tänapäeval tegemist on juba [rahvusvahelise viktoriiniga](https://bebras.org), mida korraldatakse juba enam kui 50 riigis.

*Klikkides huvipakkuval hooajal, saate selle kohta lisainfot.*

---

<details><summary>&raquo; Hooaeg 2023/2024</summary>

**I voor**

* kuupäev: 6.-17. november 2023
* koht: [ViLLE keskkond](https://ville.utu.fi/)
* osalejate arv:
  * benjaminid: 2317
  * kadetid: 2154
  * juuniorid: 1267
  * seeniorid: 614

**II voor**

* kuupäev: 4. veebruar 2024
* koht: Tartu Ülikooli Delta õppehoone
* osalejate arv:
  * benjaminid: xx
  * kadetid: xx
  * juuniorid: xx
  * seeniorid: xx
* IT-huvitegevused: ??
* osalejate geograafia:

<center><img src="maakonnad-veebr2023.png" width="100%"></center>
<center><img src="koolid-veebr2023.png" width="90%"></center>

*Toetajad sel hooajal*

![](logo-ati.png)
![](logo-teaduskool.png)
![](logo-gt.png)
![](logo-seb.png)
![](logo-bondora.png)
![](logo-nortal.png)

---
</details>

<details><summary>&raquo; Hooaeg 2022/2023</summary>

**I voor**

* kuupäev: 7.-18. november 2022
* koht: [Tartu Ülikooli Teaduskooli viktoriinikeskkond](https://viktoriinid.ee)
* osalejate arv:
  * benjaminid: 3033
  * juuniorid: 1116
  * seeniorid: 644
* osalejate geograafia:

<center><img src="maakonnad-nov2022.png" width="100%"></center>
<center><img src="koolid-nov2022.png" width="90%"></center>

**II voor**

* kuupäev: 5. veebruar 2023
* koht: Tartu Ülikooli Delta õppehoone
* osalejate arv:
  * benjaminid: 42
  * juuniorid: 23
  * seeniorid: 23
* IT-huvitegevused: infoturbe töötuba, huviloeng isejuhtivatest autodest
* osalejate geograafia:

<center><img src="maakonnad-veebr2023.png" width="100%"></center>
<center><img src="koolid-veebr2023.png" width="90%"></center>

*Toetajad sel hooajal*

![](logo-ati.png)
![](logo-teaduskool.png)
![](logo-gt.png)
![](logo-seb.png)
![](logo-bondora.png)
![](logo-nortal.png)

---
</details>

<details><summary>&raquo; Hooaeg 2021/2022</summary>

**I voor**

* kuupäev: 8.-19. november 2021
* koht: [Tartu Ülikooli Teaduskooli viktoriinikeskkond](https://viktoriinid.ee)
* osalejate arv:
  * benjaminid: 2453
  * juuniorid: 883
  * seeniorid: 553
* osalejate geograafia:

<center><img src="maakonnad-nov2021.png" width="100%"></center>
<center><img src="koolid-nov2021.png" width="90%"></center>

**II voor**

* kuupäev: 14.-25. märts 2022
* koht: [Tartu Ülikooli Teaduskooli viktoriinikeskkond](https://viktoriinid.ee)
* osalejate arv:
  * benjaminid: 746
  * juuniorid: 410
  * seeniorid: 241
* osalejate geograafia:

<center><img src="maakonnad-veebr2022.png" width="100%"></center>
<center><img src="koolid-veebr2022.png" width="90%"></center>

*Toetajad sel hooajal*

![](logo-ati.png)
![](logo-teaduskool.png)
![](logo-gt.png)
![](logo-seb.png)

---
</details>

<details><summary>&raquo; Hooaeg 2020/2021</summary>

**I voor**

* kuupäev: 9. november-4. detsember 2020
* koht: [Tartu Ülikooli Teaduskooli viktoriinikeskkond](https://viktoriinid.ee)
* osalejate arv:
  * benjaminid: 2165
  * juuniorid: 737
  * seeniorid: 452

**II voor**

* kuupäev: 1-12 марта 2021
* koht: [Tartu Ülikooli Teaduskooli viktoriinikeskkond](https://viktoriinid.ee)
* osalejate arv:
  * benjaminid: 1011
  * juuniorid: 345
  * seeniorid: 185

*Toetajad sel hooajal*

![](logo-ati.png)
![](logo-teaduskool.png)
![](logo-gt.png)
![](logo-seb.png)

---
</details>

<details><summary>&raquo; Hooaeg 2019/2020</summary>

**I voor**

* kuupäev: 4.-15. november 2019
* koht: *Miksike* keskkond
* osalejate arv:
  * benjaminid: 2053
  * juuniorid: 805
  * seeniorid: 617

**II voor**

* kuupäev: 15.-16. veebruar 2020
* koht: Tartu Ülikooli Delta õppehoone
* osalejate arv:
  * benjaminid: 32
  * juuniorid: 21
  * seeniorid: 20
* IT-huvitegevused: IT-firmade külastus (Nortal, Perforce, Bolt), tutvumine Delta õppehoonega, Bolti ja Starship esindajate ettekanded, Micro:Bit programmeerimise töötuba, huviloeng keeletehnoloogiast

*Toetajad sel hooajal*

![](logo-ati.png)
![](logo-teaduskool.png)
![](logo-gt.png)
![](logo-seb.png)
![](logo-miksike.png)
![](logo-nortal.png)

---
</details>

<details><summary>&raquo; Hooaeg 2018/2019</summary>

**I voor**

* kuupäev: 5.-16. november 2018
* koht: *Miksike* keskkond
* osalejate arv:
  * benjaminid: 2180
  * juuniorid: 840
  * seeniorid: 438

**II voor**

* kuupäev: 16.-17. veebruar 2019
* koht: Tartu Ülikooli matemaatika-informaatikateaduskond
* osalejate arv:
  * benjaminid: 36
  * juuniorid: 26
  * seeniorid: 23
* IT-huvitegevused: IT-firmade külastus (ZeroTurnaround, Glia, Cybernetica), osalemine mängus "Süsteemianalüütik", mobiiliäppide töötuba, huviloengud küberspionaažist, bioinformaatikast ja infotehnoloogiast põllumajanduses

*Toetajad sel hooajal*

![](logo-ati.png)
![](logo-teaduskool.png)
![](logo-gt.png)
![](logo-miksike.png)
![](logo-nortal.png)

---
</details>

<details><summary>&raquo; Hooaeg 2017/2018</summary>

**I voor**

* kuupäev: 6.-17. november 2017
* koht: *Miksike* keskkond
* osalejate arv:
  * benjaminid: 2422
  * juuniorid: 1015
  * seeniorid: 482

**II voor**

* kuupäev: 17.-18. veebruar 2018
* koht: Tartu Ülikooli matemaatika-informaatikateaduskond
* osalejate arv:
  * benjaminid: 33
  * juuniorid: 23
  * seeniorid: 17
* IT-huvitegevused: IT-firmade külastus (Nortal, Reach-U), programmeerimise ja veebilehe loomise töötuba, huviloengud salakirjateadusest, oma ettevõtte asutamisest ja informaatika õppimisest ülikoolis

*Toetajad sel hooajal*

![](logo-ati.png)
![](logo-teaduskool.png)
![](logo-gt.png)
![](logo-miksike.png)
![](logo-nortal.png)

---
</details>

<br />
<br />
Varasemate aastate andmed ei ole kättesaadavad.
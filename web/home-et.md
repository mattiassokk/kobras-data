<div style="float: left; padding: 10px">
  <img src="logo.png" alt="Viktoriini Kobras logo" /><br />
</div>

#### Informaatikaviktoriin Kobras

Hooaeg 2023/2024:
- I voor toimus **06.-17. novembril 2023. a.** veebipõhise kontrolltööna [ViLLE keskkonnas](https://ville.utu.fi/). Võistlusel osalemiseks pidi õpetaja looma osalejate grupi ja jagama õpilastele paroole.
<!---
I voor toimub **06.-17. novembril 2023. a.** veebipõhise kontrolltööna [Teaduskooli viktoriinikeskkonnas](https://viktoriinid.ee). Võistlusel osalemiseks peab iga õpilane looma endale sinna keskkonda konto. Olete oodatud osalema! -->
- II voor toimub **04. veebruaril 2024. a.** Tartus. Täpsema info II vooru kohta saadame jaanuaris otse kutsututele.

---

Kobras on üldhariduskoolide õpilastele mõeldud informaatikaviktoriin, mille teemaderingi mahuvad küsimused arvutite riist- ja tarkvarast, turvalisusest, arvutieetikast, arvutus- ja sidetehnika ajaloost, arvutitega seotud matemaatikast, loogikast ning informatsiooni mõistmisest ja tõlgendamisest üldisemalt.

Kobras on osa [rahvusvahelisest perest](https://bebras.org), mis sai alguse 2004. aastal Leedust ja jõudis Eestisse kaks aastat hiljem. Praeguseks korraldatakse viktoriini juba enam kui 50 riigis, kus esimeses voorus osaleb kokku miljoneid õpilasi.

Eestis võisteldi pikka aega kolmes vanuserühmas: benjaminid (6.–8. klass), juuniorid (9.–10. klass) ja seeniorid (11.–12. klass). Alates hooajast 2023/2024 võisteldakse juba neljas vanuserühmas: benjaminid (5.–6. klass), kadetid (7.-8. klass), juuniorid (9.–10. klass) ja seeniorid (11.–12. klass).

Veebikeskkonnas peetava I vooru tulemuste põhjal kutsutakse iga rühma parimad Tartusse viktoriini II vooru (ehk finaalvõistlusele).

Ülesannete lahendamiseks on mõlemas voorus aega üks koolitund (45 minutit).

Informaatikavõistluste jooksvat teavet avaldame ka [Facebookis](https://facebook.com/estinfol).

---

Kontaktid: Ahto Truu (<ahto.truu@ut.ee>) ja Lidia Feklistova (<lidia.feklistova@ut.ee>)
